####
# bge_game3_core: Full python game structure for the Blender Game Engine
# Copyright (C) 2019  DaedalusMDW @github.com (Daedalus_MDW @blenderartists.org)
# https://github.com/DaedalusMDW/bge_game3_core
#
# This file is part of bge_game3_core.
#
#    bge_game3_core is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    bge_game3_core is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with bge_game3_core.  If not, see <http://www.gnu.org/licenses/>.
#
####

## INPUT PROCESSING ##


from bge import logic, events, render

from mathutils import Vector
import json

from . import config


## Mouse ##
events.MOUSEMOVE = {"Old":(0,0), "Move":(0,0), "Position":(0,0)}
MS_CENTER = False
WIN_DIM = (render.getWindowWidth(), render.getWindowHeight())

## Find Available Gamepads ##
events.JOYBUTTONS = {}
events.AXISCALIBRATION = {}
JOY_CALIBRATE = None
JOY_HATSDIR = {0:"", 1:"U", 2:"R", 3:"UR", 4:"D", 6:"DR", 8:"L", 9:"UL", 12:"DL"}

for JOYID in range(len(logic.joysticks)):
	if logic.joysticks[JOYID] != None:
		events.JOYBUTTONS[JOYID] = {"Buttons":{}, "Axis":{}, "Hats":{}}
		print("Gamepad Found:", logic.joysticks[JOYID], "ID:", JOYID)

		for BUTID in range(logic.joysticks[JOYID].numButtons):
			events.JOYBUTTONS[JOYID]["Buttons"][BUTID] = 0
		events.JOYBUTTONS[JOYID]["Buttons"]["U"] = 0
		events.JOYBUTTONS[JOYID]["Buttons"]["D"] = 0
		events.JOYBUTTONS[JOYID]["Buttons"]["L"] = 0
		events.JOYBUTTONS[JOYID]["Buttons"]["R"] = 0

		for AXIS in range(len(logic.joysticks[JOYID].axisValues)):
			events.JOYBUTTONS[JOYID]["Axis"][AXIS] = {"NEG":0, "POS":0, "SLIDER":0, "VALUE":0.0, "ACTIVE":False}
			events.AXISCALIBRATION[AXIS] = 0.0

		for HAT in range(len(logic.joysticks[JOYID].hatValues)):
			events.JOYBUTTONS[JOYID]["Hats"][HAT] = {"U":0, "D":0, "L":0, "R":0}


## FEATURES ##
def GetInput(name, group):
	pass

def GamepadCheck():
	if GAMEPADDER not in logic.getSceneList()[0].post_draw:
		logic.getSceneList()[0].post_draw.insert(0, GAMEPADDER)
		print("NOTICE: GAMEPADDER() Scene Fix -", logic.getSceneList()[0].name)
		GAMEPADDER()
		return False
	return True

def ClipAxis(value):
	LZ = 0.2
	HZ = 1.0

	if value < LZ:
		value = LZ
	if value > HZ:
		value = HZ

	value = ( (value-LZ)/(HZ-LZ) )

	return value

def JoinAxis(VX=0, VY=0, VZ=0):
	vec = Vector([VX, VY, VZ])

	CLIP = ClipAxis(vec.length)
	NORM = vec.normalized()*CLIP

	return NORM


## Base Class for Inputs ##
class KeyBase:

	GROUP = "Default"

	def __init__(self, ID, KEY, SIMPLE, JOYINDEX=0, JOYBUTTON=None, JOYAXIS=(None, "SLIDER", "A"), SHIFT=None, CTRL=None, ALT=None):
		self.id = ID
		self.simple_name = SIMPLE
		self.gamepad = {}
		self.modifiers = {"S":SHIFT, "C":CTRL, "A":ALT}

		self.updateBind(KEY)
		self.updateGamepad(Index=JOYINDEX, Button=JOYBUTTON, Axis=JOYAXIS[0], Type=JOYAXIS[1], Curve=JOYAXIS[2])

		GamepadCheck()

	def updateBind(self, NEWKEY):
		self.input_name = NEWKEY

		if NEWKEY != "NONE":
			self.input = getattr(events, self.input_name)
		else:
			self.input = None

		self.autoDevice()

	def autoDevice(self):
		modkey = []
		for m in ["SHIFT", "CTRL", "ALT"]:
			modkey.append("LEFT"+m+"KEY")
			modkey.append("RIGHT"+m+"KEY")

		if self.input == None:
			self.device = logic.keyboard
			self.isWheel = False
			self.isModkey = False

		elif self.input_name in modkey:
			self.device = logic.keyboard
			self.isWheel = False
			self.isModkey = True

		elif self.input_name in ["LEFTMOUSE", "MIDDLEMOUSE", "RIGHTMOUSE"]:
			self.device = logic.mouse
			self.isWheel = False
			self.isModkey = False

		elif self.input_name in ["WHEELDOWNMOUSE", "WHEELUPMOUSE", "MOUSEX", "MOUSEY"]:
			self.device = logic.mouse
			self.isWheel = True
			self.isModkey = False

		else:
			self.device = logic.keyboard
			self.isWheel = False
			self.isModkey = False

	def updateGamepad(self, **kwargs):
		for kw in kwargs:
			self.gamepad[kw] = kwargs[kw]

		JOYID = self.gamepad["Index"]
		BUTID = self.gamepad["Button"]
		AXIS = self.gamepad["Axis"]

		self.joy_index = JOYID

		if JOYID >= len(logic.joysticks):
			self.joy_index = None
		elif logic.joysticks[JOYID] == None:
			self.joy_index = None
		else:
			if AXIS != None:
				if AXIS >= len(logic.joysticks[JOYID].axisValues):
					self.joy_index = None
			if BUTID != None:
				if type(BUTID) is int:
					if BUTID >= logic.joysticks[JOYID].numButtons:
						self.joy_index = None
				if type(BUTID) is str:
					if len(logic.joysticks[JOYID].hatValues) <= 0:
						self.joy_index = None

	def getData(self):
		joy = self.gamepad
		mod = self.modifiers

		dict = {
		"Key": self.input_name,
		"Gamepad": joy.copy(),
		"Modifiers": mod.copy(),
		}

		return dict

	def setData(self, dict):
		self.gamepad = dict["Gamepad"].copy()
		self.modifiers = dict["Modifiers"].copy()

		self.updateBind(dict["Key"])
		self.updateGamepad()

	def checkGamepadInput(self, INPUT, JOYID=None):
		if GamepadCheck() == False:
			return False

		if JOYID == None:
			JOYID = self.joy_index
		elif self.joy_index == None or JOYID == "NONE":
			JOYID = None

		BUTID = self.gamepad["Button"]
		AXIS = self.gamepad["Axis"]
		TYPE = self.gamepad["Type"]
		CURVE = self.gamepad["Curve"]

		if JOYID != None:
			if AXIS != None and BUTID != None:
				x = 0
				if events.JOYBUTTONS[JOYID]["Buttons"][BUTID] == INPUT:
					x += 1
				if events.JOYBUTTONS[JOYID]["Axis"][AXIS][TYPE] == INPUT:
					x += 1
				if x == 2:
					return True

			elif BUTID != None:
				if events.JOYBUTTONS[JOYID]["Buttons"][BUTID] == INPUT:
					return True

			elif AXIS != None:
				if events.JOYBUTTONS[JOYID]["Axis"][AXIS][TYPE] == INPUT:
					return True

		elif INPUT == logic.KX_INPUT_NONE:
			return True

		return False

	def checkKeyboardInput(self, INPUT):
		mod = self.checkModifiers()

		if self.input != None:
			if mod == True or INPUT == logic.KX_INPUT_NONE:
				if self.device.events[self.input] == INPUT:
					return True

				if self.isWheel == True:
					if INPUT == logic.KX_INPUT_JUST_ACTIVATED:
						if self.device.events[self.input] == logic.KX_INPUT_ACTIVE:
							return True
					if INPUT == logic.KX_INPUT_ACTIVE:
						if self.device.events[self.input] == logic.KX_INPUT_JUST_ACTIVATED:
							return True
		elif mod == True:
			return True

		return False

	def checkInput(self, INPUT, JOYID=None):
		PAD = self.checkGamepadInput(INPUT, JOYID)
		KEY = self.checkKeyboardInput(INPUT)

		if INPUT == logic.KX_INPUT_NONE:
			if KEY == True and PAD == True:
				return True
		else:
			if KEY == True or PAD == True:
				return True

		return False

	def checkModifiers(self):
		if self.isModkey == True:
			return True

		KEYBOARD = logic.keyboard.events
		ACTIVE = logic.KX_INPUT_ACTIVE

		S = False
		C = False
		A = False

		if self.modifiers["S"] == None:
			S = None
		elif KEYBOARD[events.LEFTSHIFTKEY] == ACTIVE or KEYBOARD[events.RIGHTSHIFTKEY] == ACTIVE:
			S = True

		if self.modifiers["C"] == None:
			C = None
		elif KEYBOARD[events.LEFTCTRLKEY] == ACTIVE or KEYBOARD[events.RIGHTCTRLKEY] == ACTIVE:
			C = True

		if self.modifiers["A"] == None:
			A = None
		elif KEYBOARD[events.LEFTALTKEY] == ACTIVE or KEYBOARD[events.RIGHTALTKEY] == ACTIVE:
			A = True

		if self.input == None:
			if S == None and C == None and A == None:
				return False

		if self.modifiers["S"] == S and self.modifiers["C"] == C and self.modifiers["A"] == A:
			return True

		return False

	## KEY EVENTS ##
	def active(self, exlusive=False, JOYID=None):
		if self.checkInput(logic.KX_INPUT_ACTIVE, JOYID) == True:
			return True
		elif exlusive == True:
			return False
		elif self.checkInput(logic.KX_INPUT_JUST_ACTIVATED, JOYID) == True:
			return True

		return False

	def tap(self, JOYID=None):
		if self.checkInput(logic.KX_INPUT_JUST_ACTIVATED, JOYID) == True:
			return True

		return False

	def released(self, JOYID=None):
		if self.checkInput(logic.KX_INPUT_JUST_RELEASED, JOYID) == True:
			return True

		return False

	def inactive(self, JOYID=None):
		if self.checkInput(logic.KX_INPUT_NONE, JOYID) == True:
			return True

		return False

	def axis(self, **kwargs):
		if GamepadCheck() == False:
			return 0.0

		JOYID = kwargs.get("JOYID", None)
		if JOYID == None:
			JOYID = self.joy_index
		elif self.joy_index == None or JOYID == "NONE":
			JOYID = None

		BUTID = self.gamepad["Button"]
		AXIS = self.gamepad["Axis"]
		TYPE = self.gamepad["Type"]
		CURVE = self.gamepad["Curve"]

		if (CURVE == "B" and AXIS != None) or (BUTID != None and AXIS == None):
			if self.active(False, JOYID) == True:
				return 1.0
		elif AXIS != None and BUTID != None:
			if events.JOYBUTTONS[JOYID]["Buttons"][BUTID] not in {1,2}:
				return 0.0
		elif kwargs.get("exlusive", False) == False:
			if self.active(False, "NONE") == True:
				return 1.0

		if JOYID == None or AXIS == None or CURVE == "B":
			return 0.0

		VALUE = events.JOYBUTTONS[JOYID]["Axis"][AXIS]["VALUE"]
		ABSVAL = abs(VALUE)
		if kwargs.get("clip", True) == True:
			ABSVAL = ClipAxis(ABSVAL)

		if TYPE == "POS":
			if VALUE > 0:
				return ABSVAL

		elif TYPE == "NEG":
			if VALUE < 0:
				return ABSVAL

		elif TYPE == "SLIDER":
			if VALUE > 0:
				return VALUE

		return 0.0


## Mouse Look Base Class ##
class MouseLook:

	def __init__(self, SPEED=25, SMOOTH=10, TURN=0.035):
		self.getScreenRatio()
		self.updateSpeed(SPEED, SMOOTH, TURN)
		self.center()

		GamepadCheck()

	def getScreenRatio(self):
		global WIN_DIM
		WIN_DIM = (render.getWindowWidth(), render.getWindowHeight())
		self.screen = list(WIN_DIM)
		self.ratio = WIN_DIM[1]/WIN_DIM[0]

	def updateSpeed(self, speed=None, smooth=None, turn=None):
		if speed != None:
			self.input = speed
		if smooth != None:
			self.smoothing = int(smooth)
		if turn != None:
			self.ts_rate = turn

	def getData(self):
		dict = {
			"Speed":self.input,
			"Smooth":self.smoothing,
			"TurnRate":self.ts_rate
			}
		return dict

	def setData(self, dict):
		speed = dict.get("Speed", None)
		smooth = dict.get("Smooth", None)
		turn = dict.get("TurnRate", None)
		self.updateSpeed(speed, smooth, turn)

	def bufferReset(self):
		que = [0]*self.smoothing
		self.old_input = [que.copy(), que.copy()]

	def bufferAxis(self, axis):
		AVG = 0

		for a in range(self.smoothing):
			AVG += self.old_input[axis][a]

		return AVG/self.smoothing

	def center(self):
		global MS_CENTER
		MS_CENTER = True

		self.bufferReset()
		self.ts_input = [0, 0]
		self.skip = 10

	def smoothAxis(self, X, Y):
		turn = [0]*2
		look = Vector((X, Y))

		for i in [0,1]:
			turn[i] = look[i]
			if self.smoothing > 1:
				if look.length <= 0 and config.MOUSE_BUFFER == False:
					self.bufferReset()
				else:
					self.old_input[i].insert(0, look[i])
					self.old_input[i].pop()
				turn[i] = self.bufferAxis(i)

		return turn

	def axis(self, look=None, ui=False, center=True):
		global MS_CENTER
		MS_CENTER = center

		if GamepadCheck() == False:
			return (0,0)

		if self.skip > 0:
			self.skip -= 1
			return (0,0)

		RAW_X, RAW_Y = events.MOUSEMOVE["Move"]

		if ui == True:
			return (RAW_X/WIN_DIM[0], RAW_Y/WIN_DIM[1])

		X = RAW_X*(self.input/10)/-1000
		Y = RAW_Y*(self.input/10)/1000

		if look != None:
			X += look[0]*self.ts_rate
			Y += look[1]*self.ts_rate

		X, Y = self.smoothAxis(X, Y)

		return (X, Y)


## Updates Joystick Values ##
def GAMEPADDER():
	global MS_CENTER, WIN_DIM, JOY_CALIBRATE, JOY_HATSDIR

	POS_X, POS_Y = logic.mouse.position
	OLD_X, OLD_Y = events.MOUSEMOVE["Old"]

	POS_X = round( (POS_X*WIN_DIM[0]) )
	POS_Y = round( (POS_Y*WIN_DIM[1]) )
	CNT_X = round( (WIN_DIM[0]/2) )
	CNT_Y = round( (WIN_DIM[1]/2) )
	NEW_X = POS_X-CNT_X
	NEW_Y = CNT_Y-POS_Y

	skip = False
	if MS_CENTER == True:
		xlm = round(WIN_DIM[0]/4)
		ylm = round(WIN_DIM[1]/4)
		if abs(NEW_X) > xlm or abs(NEW_Y) > ylm:
			skip = True

	MS_CENTER = False

	if skip == True:
		logic.mouse.position = (0.5, 0.5)
		events.MOUSEMOVE["Old"] = (0, 0)
	else:

		X, Y = events.MOUSEMOVE["Position"]
		X += (NEW_X-OLD_X)
		if X < 0:
			X = 0
		if X > WIN_DIM[0]:
			X = WIN_DIM[0]
		Y += (NEW_Y-OLD_Y)
		if Y < 0:
			Y = 0
		if Y > WIN_DIM[1]:
			Y = WIN_DIM[1]

		events.MOUSEMOVE["Position"] = (X, Y)
		events.MOUSEMOVE["Move"] = (NEW_X-OLD_X, NEW_Y-OLD_Y)
		events.MOUSEMOVE["Old"] = (NEW_X, NEW_Y)

	for JOYID in events.JOYBUTTONS:

		if logic.joysticks[JOYID] == None:
			print("GAMEPAD ERROR:", JOYID, "Not Found!")
			return

		for B in events.JOYBUTTONS[JOYID]["Buttons"]:
			DICT = events.JOYBUTTONS[JOYID]["Buttons"]

			if B in logic.joysticks[JOYID].activeButtons:
				if DICT[B] == 0 or DICT[B] == 3:
					DICT[B] = 1
				else:
					DICT[B] = 2
			else:
				if DICT[B] == 2 or DICT[B] == 1:
					DICT[B] = 3
				else:
					DICT[B] = 0

		for A in events.JOYBUTTONS[JOYID]["Axis"]:
			DICT = events.JOYBUTTONS[JOYID]["Axis"]
			BIAS = events.AXISCALIBRATION

			RAWINPUT = logic.joysticks[JOYID].axisValues[A]

			if JOY_CALIBRATE in ["ALL", A]:
				print("AXIS CALIBRATED:", JOYID, A, -RAWINPUT)
				BIAS[A] = -RAWINPUT

			if DICT[A]["ACTIVE"] == True:
				RAWINPUT = (RAWINPUT+BIAS[A])
			elif  RAWINPUT != 0:
				DICT[A]["ACTIVE"] = True
				RAWINPUT = 0

			ABSINPUT = abs(RAWINPUT)

			isneg = int(RAWINPUT<0)
			DICT[A]["VALUE"] = ABSINPUT*(1-(2*isneg))

			if RAWINPUT > 0.5:
				if DICT[A]["POS"] in [0, 3]:
					DICT[A]["POS"] = 1
				else:
					DICT[A]["POS"] = 2
			else:
				if DICT[A]["POS"] in [2, 1]:
					DICT[A]["POS"] = 3
				else:
					DICT[A]["POS"] = 0

			if RAWINPUT < -0.5:
				if DICT[A]["NEG"] in [0, 3]:
					DICT[A]["NEG"] = 1
				else:
					DICT[A]["NEG"] = 2
			else:
				if DICT[A]["NEG"] in [2, 1]:
					DICT[A]["NEG"] = 3
				else:
					DICT[A]["NEG"] = 0

			if RAWINPUT >= 0.1:
				if DICT[A]["SLIDER"] in [0, 3]:
					DICT[A]["SLIDER"] = 1
				else:
					DICT[A]["SLIDER"] = 2
			else:
				if DICT[A]["SLIDER"] in [2, 1]:
					DICT[A]["SLIDER"] = 3
				else:
					DICT[A]["SLIDER"] = 0

		for H in events.JOYBUTTONS[JOYID]["Hats"]:
			DICT = events.JOYBUTTONS[JOYID]["Hats"]
			VALUE = JOY_HATSDIR[logic.joysticks[JOYID].hatValues[H]]

			for key in DICT[H]:
				if key in VALUE:
					if DICT[H][key] == 0 or DICT[H][key] == 3:
						DICT[H][key] = 1
					else:
						DICT[H][key] = 2
				else:
					if DICT[H][key] == 2 or DICT[H][key] == 1:
						DICT[H][key] = 3
					else:
						DICT[H][key] = 0

				events.JOYBUTTONS[JOYID]["Buttons"][key] = DICT[H][key]

	JOY_CALIBRATE = None


print("input.py Imported")

