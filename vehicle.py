####
# bge_game3_core: Full python game structure for the Blender Game Engine
# Copyright (C) 2019  DaedalusMDW @github.com (Daedalus_MDW @blenderartists.org)
# https://github.com/DaedalusMDW/bge_game3_core
#
# This file is part of bge_game3_core.
#
#    bge_game3_core is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    bge_game3_core is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with bge_game3_core.  If not, see <http://www.gnu.org/licenses/>.
#
####

## VEHICLE CORE ##


from bge import logic, constraints, events

from . import base, keymap, HUD, viewport, config


class CoreVehicle(base.CoreAdvanced):

	NAME = "Vehicle"
	GHOST = "CAMERA"
	PORTAL = True
	PHYSICS = "RIGID"
	ENTRY_FLIP = False
	EJECT_PLAYER = True

	MOUSE_CONTROL = True
	MOUSE_CENTER = True
	MOUSE_SCALE = 2
	MOUSE_ROLL = False

	INPUT_LIMIT = 0.001
	INPUT_SMOOTH = 0.1

	CAM_HEAD_G = 8

	WH_OBJECT = "Empty"   # wheel contsraint object
	WH_MESH = None        # Visual wheel object
	WH_RADIUS = 0.2       # Wheel radius
	WH_COLOR = None

	WH_FRONT = 1        # Forward axle offset
	WH_REAR = -1        # Backward axle offset
	WH_WIDTH = 1        # Axle width
	WH_HEIGHT = 0.25    # Axle Z offset

	VEH_LENGTH = 0.25   # Suspension length

	VEH_ROLL = 0.1
	VEH_SPRING = 50
	VEH_DAMPING = 10
	VEH_FRICTION = 5
	VEH_COMPRESS = 3

	WHEELS = {
		"Wheel_FR": {"STEER":True},
		"Wheel_FL": {"LEFT":True},
		"Wheel_RR": {"REAR":True},
		"Wheel_RL": {"REAR":True, "LEFT":True} }

	SEATS = {
		"Seat_1": {"NAME":"Driver", "DOOR":"Door_1", "CAMERA":[0,0,0], "ACTION":"SeatLow", "VISIBLE":True, "SPAWN":[-2,0,0]} }

	def __init__(self, *args):
		if len(args) == 0:
			return
		self.owner = args[0]

		self.ANIMOBJ = None

		self.owner["Class"] = self

		#self.owner["RAYCAST"] = self.owner.get("RAYCAST", None)
		#self.owner["RAYNAME"] = self.NAME

		self.objects = {"Root":self.owner}

		self.defaultEvents()
		self.defaultStates()

		self.driving_seat = None
		self.player_seats = {".":None}

		self.vehicle_constraint = None
		self.wheel_id = []
		self.id_range = []
		self.wheelobj = {}
		self.seatobj = {}
		self.doorobj = {}
		self.cid = 999

		self.dict = self.owner["DICT"]
		self.data = self.defaultData()

		self.data["HEALTH"] = 100
		self.data["ENERGY"] = 100
		self.data["HUD"] = {"Text":"", "Color":(0,0,0,0.5), "Target":None}
		self.data["ACTIVE_SEAT"] = self.driving_seat
		self.data["MOUSEMODE"] = (self.MOUSE_CENTER==True)

		self.motion = {
			"Force": self.createVector(),
			"Torque": self.createVector(),
			"Mouse":self.createVector(2)}

		self.setPhysicsGrid()
		self.setPhysicsType()

		self.addCollisionCallBack()

		self.checkGhost(self.owner)
		self.findObjects(self.owner, ground=False)
		self.findSeats()
		self.doLoad()
		self.createVehicle()

		self.ST_Startup()
		self.doPortal()

	def defaultStates(self):
		self.active_pre = [self.PR_ClipZone, self.PR_SuspensionRig]
		self.active_state = self.ST_Idle
		self.active_post = []

	def doPortal(self):
		owner = self.objects["Root"]

		self.driving_seat = self.data["ACTIVE_SEAT"]

		chk = False

		for plrcls in list(self.container_children):
			for key in self.player_seats:
				if plrcls.dict.get("Vehicle", None) == key:

					self.attachToSeat(plrcls, key)

					if key == self.driving_seat:
						self.assignCamera()
						viewport.loadCamera()
						chk = True

		if chk == False and self.driving_seat != None:
			self.data["ACTIVE_SEAT"] = None
			self.player_seats[self.driving_seat] = None
			self.driving_seat = None
			self.stateSwitch("IDLE")

	def doUpdate(self):
		self.data["ACTIVE_SEAT"] = self.driving_seat
		super().doUpdate()

	def getConstraint(self):
		owner = self.getOwner()

		if self.vehicle_constraint != None:
			vehicle = self.vehicle_constraint

		elif hasattr(constraints, "createVehicle") == True:
			vehicle = constraints.createVehicle(owner.getPhysicsId())
			vehicle.rayMask = owner.collisionMask
			self.cid = vehicle.getConstraintId()

		else:
			vehicle = constraints.createConstraint(owner.getPhysicsId(), 0, constraints.VEHICLE_CONSTRAINT)

			self.cid = vehicle.getConstraintId()
			vehicle = constraints.getVehicleConstraint(self.cid)

		return vehicle

	def findSeats(self):
		for key in self.SEATS:
			self.player_seats[key] = None
			dict = self.SEATS[key]
			seat = self.objects.get(key, None)
			door = self.objects.get(dict.get("DOOR", None), None)
			if seat != None:
				self.seatobj[key] = seat
				if door != None:
					name = dict.get("NAME", key)
					door["Class"] = self
					door["RAYNAME"] = self.NAME+": "+name
					door["COLLIDE"] = []
					self.doorobj[key] = door

		owner = self.objects["Root"]
		if len(self.seatobj) == 0 or len(self.doorobj) == 0:
			self.seatobj["."] = owner
			self.doorobj["."] = owner

	def endObject(self, children=True):
		if self.EJECT_PLAYER == True:
			for key in self.player_seats:
				if self.player_seats[key] != None:
					self.removeFromSeat(key, force=True)
					self.driving_seat = None
		super().endObject(children)

	def destroy(self):
		self.removeVehicle()
		super().destroy()

	def createVehicle(self):
		if self.vehicle_constraint != None:
			return

		self.vehicle_constraint = self.getConstraint()

		for w in self.WHEELS:
			self.addWheel(w, self.WHEELS[w])

	def removeVehicle(self):
		if self.vehicle_constraint == None:
			return

		constraints.removeConstraint(self.cid)

		for w in self.wheelobj:
			self.wheelobj[w].endObject()

		self.vehicle_constraint = None
		self.wheel_id = []
		self.id_range = []
		self.wheelobj = {}
		self.cid = 999

	def addWheel(self, w, dict={}):
		id = len(self.wheel_id)

		R = 0
		X = self.WH_WIDTH
		Y = self.WH_FRONT

		obj = base.SC_SCN.addObject(self.WH_OBJECT, self.objects["Root"], 0)

		if dict.get("LEFT", False) == True:
			X = -X
			R = 180
		if dict.get("CENTER", False) == True:
			X = 0
		if dict.get("REAR", False) == True:
			Y = self.WH_REAR
		if dict.get("ROTATE", False) == True:
			R = 180

		if self.WH_MESH != None:
			msh = base.SC_SCN.addObject(self.WH_MESH, self.objects["Root"], 0)
			msh.setParent(obj)
			msh.localPosition = self.createVector()
			msh.localOrientation = self.createMatrix(rot=(0,0,R))
			msh.localScale = (1,1,1)
			if self.WH_COLOR != None:
				msh.color = self.WH_COLOR

		pos = dict.get("POS", [X, Y, -self.WH_HEIGHT])
		length = dict.get("LENGTH", self.VEH_LENGTH)
		radius = dict.get("RADIUS", self.WH_RADIUS)

		self.vehicle_constraint.addWheel(obj, pos, (0,0,-1), (-1,0,0), length, radius, True)

		self.vehicle_constraint.setRollInfluence(0.0,id)#self.VEH_ROLL, id)
		self.vehicle_constraint.setSuspensionStiffness(self.VEH_SPRING, id)
		self.vehicle_constraint.setSuspensionDamping(self.VEH_DAMPING, id)
		self.vehicle_constraint.setTyreFriction(self.VEH_FRICTION, id)
		self.vehicle_constraint.setSuspensionCompression(self.VEH_COMPRESS, id)

		self.wheel_id.append(w)
		self.id_range.append(id)
		self.wheelobj[w] = obj

		return obj

	def getWheelId(self, wheel, key):
		if wheel == key or wheel == None:
			return True
		elif wheel == "FRONT" and "REAR" not in self.WHEELS[key]:
			return True
		elif wheel == "REAR" and "REAR" in self.WHEELS[key]:
			return True
		return False

	def setWheelSteering(self, value, wheel=None):
		if self.vehicle_constraint == None:
			return

		for i in self.id_range:
			key = self.wheel_id[i]
			check = False
			steer = 0
			if self.getWheelId(wheel, key) == True:
				check = True
				steer = value
			if check == True:
				self.vehicle_constraint.setSteeringValue(steer, i)

	def setWheelPower(self, value, wheel=None):
		if self.vehicle_constraint == None:
			return

		for i in self.id_range:
			key = self.wheel_id[i]
			check = False
			power = 0
			if self.getWheelId(wheel, key) == True:
				check = True
				power = value
			if check == True:
				self.vehicle_constraint.applyEngineForce(power, i)

	def setWheelBrake(self, value, wheel=None):
		if self.vehicle_constraint == None:
			return

		for i in self.id_range:
			key = self.wheel_id[i]
			check = False
			brake = 0
			if self.getWheelId(wheel, key) == True:
				check = True
				brake = value
			if check == True:
				self.vehicle_constraint.applyBraking(brake, i)

	def rotateCamera(self):
		if self.data["CAMERA"]["Orbit"] >= 1:
			TURN = keymap.BINDS["PLR_TURNLEFT"].axis(exlusive=True, clip=False) - keymap.BINDS["PLR_TURNRIGHT"].axis(exlusive=True, clip=False)
			LOOK = keymap.BINDS["PLR_LOOKUP"].axis(exlusive=True, clip=False) - keymap.BINDS["PLR_LOOKDOWN"].axis(exlusive=True, clip=False)
		else:
			TURN = 0
			LOOK = 0

		ROTATE = keymap.input.JoinAxis(LOOK, 0, TURN)
		self.motion["Rotate"] = ROTATE

	def assignCamera(self, parent=None, state=None):
		viewport.setCamera(self)
		if parent == None:
			parent = self.getOwner()
		viewport.setParent(parent)
		self.setCameraState(None)
		HUD.SetLayout(self, None)

	def setCameraState(self, state=None):
		if state == None:
			state = self.data["CAMERA"]["State"]

		if state == "THIRD":
			viewport.setState("THIRD")
			viewport.setCameraPosition(self.CAM_OFFSET)
			self.setPlayerVisibility(self.driving_seat)
		else:
			viewport.setState("SEAT")
			pos = self.SEATS.get(self.driving_seat, {}).get("CAMERA", [0,0,0])
			viewport.setCameraPosition(pos)
			self.setPlayerVisibility(self.driving_seat, False)

		viewport.setEyeHeight(0, set=True)
		viewport.setEyePitch(0, set=True)

	def hideObject(self):
		self.objects["Root"].setVisible(False, True)
		for w in self.wheelobj:
			self.wheelobj[w].setVisible(False, True)

	def getObjectAngle(self):
		if self.gravity.length < 0.1:
			return False

		angle = self.owner.getAxisVect((0,0,-1)).angle(self.gravity.normalized())
		angle = self.toDeg(angle)

		return angle

	def alignObject(self, offset=0.5, velocity=True):
		if self.gravity.length < 0.1:
			return

		self.owner.alignAxisToVect(-self.gravity, 2, 1.0)
		self.owner.worldPosition += self.gravity.normalized()*-offset
		if velocity == True:
			self.owner.localLinearVelocity = (0,0,0)
			self.owner.localAngularVelocity = (0,0,0)

	def setPlayerVisibility(self, seat, vis=None):
		plr = self.player_seats[seat]

		if seat == ".":
			vis = False
		elif vis == None:
			vis = self.SEATS[seat].get("VISIBLE", True)

	def getMouseInput(self):
		self.data["HUD"]["Target"] = None

		if self.data["CAMERA"]["Orbit"] <= 0 and self.MOUSE_CONTROL == True:
			X, Y = keymap.MOUSELOOK.axis()

			X *= 400
			Y *= 400
			if self.data["MOUSEMODE"] == True:
				POS_X, POS_Y = [0,0]
				X = X/(10-abs(POS_X*0))
				Y = Y/(10-abs(POS_Y*0))
			else:
				POS_X, POS_Y = self.motion["Mouse"]
				X = X/(400-abs(POS_X*200))
				Y = Y/(400-abs(POS_Y*200))

			POS_X += X
			POS_Y += Y
			if abs(POS_X) > 1:
				POS_X = 1-(2*(POS_X<0))
			if abs(POS_Y) > 1:
				POS_Y = 1-(2*(POS_Y<0))

			if self.data["MOUSEMODE"] == False:
				self.data["HUD"]["Target"] = [-POS_X, POS_Y]

			if keymap.BINDS["VEH_MOUSEMODE"].tap() == True and self.MOUSE_CENTER != None:
				self.data["MOUSEMODE"] ^= True

			self.motion["Mouse"][0] = POS_X
			self.motion["Mouse"][1] = POS_Y
			return POS_X, POS_Y

		else:
			self.motion["Mouse"][0] = 0
			self.motion["Mouse"][1] = 0
			return 0, 0

	def getInputs(self):
		KB = keymap.BINDS
		MX = (KB["VEH_STRAFERIGHT"], KB["VEH_STRAFELEFT"])
		MY = (KB["VEH_THROTTLEUP"], KB["VEH_THROTTLEDOWN"])
		MZ = (KB["VEH_ASCEND"], KB["VEH_DESCEND"])
		RX = (KB["VEH_PITCHUP"], KB["VEH_PITCHDOWN"])
		RY = (KB["VEH_BANKRIGHT"], KB["VEH_BANKLEFT"])
		RZ = (KB["VEH_YAWLEFT"], KB["VEH_YAWRIGHT"])

		VK = (KB["PLR_TURNLEFT"], KB["PLR_TURNRIGHT"], KB["PLR_LOOKUP"], KB["PLR_LOOKDOWN"])

		MOVE = base.mathutils.Vector([0,0,0])
		MOVE[0] = MX[0].axis() - MX[1].axis()
		MOVE[1] = MY[0].axis() - MY[1].axis()
		MOVE[2] = MZ[0].axis() - MZ[1].axis()

		if self.data["CAMERA"]["Orbit"] >= 1:
			M = (MX,MY,MZ)
			for i in (0,1,2):
				lock = False
				for a in (0,1):
					ax = M[i][a].gamepad["Axis"]
					if ax == None:
						continue
					for v in VK:
						if  v.gamepad["Axis"] == ax:
							lock = True
				if lock == True:
					MOVE[i] = M[i][0].axis(JOYID="NONE") - M[i][1].axis(JOYID="NONE")

		for i in [0,1,2]:
			MOVE[i] = MOVE[i]*abs(MOVE[i])
			dif = MOVE[i]-self.motion["Force"][i]
			mx  = 1-(2*(dif<0))

			dif = self.INPUT_SMOOTH*mx
			self.motion["Force"][i] += dif

			if dif > 0 and self.motion["Force"][i] > MOVE[i]:
				self.motion["Force"][i] = MOVE[i]
			if dif < 0 and self.motion["Force"][i] < MOVE[i]:
				self.motion["Force"][i] = MOVE[i]

		TURN = base.mathutils.Vector([0,0,0])
		TURN[0] = RX[0].axis() - RX[1].axis()
		TURN[1] = RY[0].axis() - RY[1].axis()
		TURN[2] = RZ[0].axis() - RZ[1].axis()

		for i in [0,1,2]:
			TURN[i] = TURN[i]*abs(TURN[i])

		if self.data["CAMERA"]["Orbit"] >= 1:
			R = (RX,RY,RZ)
			for i in (0,1,2):
				lock = False
				for a in (0,1):
					ax = R[i][a].gamepad["Axis"]
					if ax == None:
						continue
					for v in VK:
						if  v.gamepad["Axis"] == ax:
							lock = True
				if lock == True:
					TURN[i] = R[i][0].axis(JOYID="NONE") - R[i][1].axis(JOYID="NONE")

		POS_X, POS_Y = self.getMouseInput()

		TURN[0] += POS_Y
		if abs(TURN[0]) > 1:
			TURN[0] = 1-(2*(TURN[0]<0))

		if keymap.BINDS["VEH_SWAPROLL"].active() == True and self.MOUSE_ROLL == True:
			TURN[1] -= POS_X
			if abs(TURN[1]) > 1:
				TURN[1] = 1-(2*(TURN[1]<0))
		else:
			TURN[2] += POS_X
			if abs(TURN[2]) > 1:
				TURN[2] = 1-(2*(TURN[2]<0))

		for i in [0,1,2]:
			dif = TURN[i]-self.motion["Torque"][i]
			mx  = 1-(2*(dif<0))

			dif = self.INPUT_SMOOTH*mx
			self.motion["Torque"][i] += dif

			if dif > 0 and self.motion["Torque"][i] > TURN[i]:
				self.motion["Torque"][i] = TURN[i]
			if dif < 0 and self.motion["Torque"][i] < TURN[i]:
				self.motion["Torque"][i] = TURN[i]

		self.rotateCamera()
		self.doCameraToggle()
		self.checkStability()

	def doCameraToggle(self):
		if keymap.BINDS["TOGGLECAM"].tap() == True:
			if self.data["CAMERA"]["State"] == "THIRD":
				self.setCameraState("SEAT")
			else:
				self.setCameraState("THIRD")

	def attachToSeat(self, cls, key):
		self.player_seats[key] = cls

		seat = self.seatobj[key]
		door = self.doorobj[key]

		cls.enterVehicle(seat)
		cls.setContainerParent(self)
		cls.dict["Vehicle"] = key

		self.motion["Mouse"][0] = 0
		self.motion["Mouse"][1] = 0

	def removeFromSeat(self, key, force=False):
		owner = self.getOwner()

		cls = self.player_seats[key]
		if cls == None:
			if self.driving_seat == key:
				self.driving_seat = None
			return True

		rayvec = self.createVector(vec=(-1,0,0))

		if key != ".":
			rayvec = self.createVector(vec=self.SEATS[key].get("SPAWN", rayvec))

		dist = rayvec.length+0.2
		spawn = self.getWorldSpace(owner, rayvec)
		rayfrom = self.seatobj[key].worldPosition.copy()

		rayOBJ, rayPNT, rayNRM = owner.rayCast(spawn, owner, dist, "GROUND", 1, 1, 0)
		if rayOBJ != None and force == False:
			rayvec[0] *= -1
			spawn = self.getWorldSpace(owner, rayvec)
			rayOBJ, rayPNT, rayNRM = owner.rayCast(spawn, owner, dist, "GROUND", 1, 1, 0)
			if rayOBJ != None:
				return False
		if force == True:
			spawn = cls.getOwner().worldPosition.copy()
		ori = cls.getOwner().worldOrientation.copy()

		cls.exitVehicle(spawn, ori)
		cls.removeContainerParent()
		cls.data["LINVEL"] = list(owner.localLinearVelocity)

		self.player_seats[key] = None

		return True

	def checkClicked(self):
		owner = self.getOwner()

		all = self.getAllEvents("INTERACT", "SEND")
		act = self.getAllEvents("INTERACT", "ACTOR", "TAP")
		sec = self.getAllEvents("INTERACT", "!ACTOR", "!SEND")

		if self.driving_seat != None:
			if self.player_seats[self.driving_seat] == None:
				self.driving_seat = None

		for evt in all:
			dist = 100
			door = None
			seat = None
			cls = None
			ray = evt.sender
			o = evt.getProp("OBJECT", None)
			t = evt.getProp("TYPE", None)
			for key in self.doorobj:
				obj = self.doorobj[key]
				vec = obj.worldPosition-ray.owner.worldPosition
				if t == "RAY" and o == obj:
					dist = -1
					door = obj
					seat = key
					cls = ray
				if vec.length < dist:
					dist = vec.length
					door = obj
					seat = key
					cls = ray

			if cls != None and o == door:
				name = door["RAYNAME"]
				t = evt.getProp("TYPE", None)
				c = "COLLIDE" in door
				l = ""
				if self.player_seats[seat] != None:
					l = "OCCUPIED"
				if ((door != owner and c == False) or door == owner) and t == "RAY":
					self.sendEvent("INTERACT", cls, "RECEIVE", NAME=name, OBJECT=door, LOCK=l, TYPE=t)
				if (door != owner and c == True) and t == "COLLIDE":
					self.sendEvent("INTERACT", cls, "RECEIVE", NAME=name, OBJECT=door, LOCK=l, TYPE=t)

		plrcls = None
		drvkey = None

		for evt in act:
			cls = evt.sender
			key = None
			door = evt.getProp("OBJECT")
			if door == None or door.invalid == True:
				print("DOOR FAIL:", cls.NAME)
				return False

			for i in self.doorobj:
				if door == self.doorobj[i]:
					key = i

			if cls != None and key != None:
				if self.player_seats[key] != None:
					if self.removeFromSeat(key) == False:
						continue
				if self.ENTRY_FLIP == True and self.getObjectAngle() > 60:
					self.alignObject()
					return False
				if cls.dict.get("Vehicle", None) == None:
					self.attachToSeat(cls, key)
					if cls.player_id == "1":
						plrcls = cls
						drvkey = key

			if plrcls != None and drvkey != None:
				self.driving_seat = drvkey

		if self.driving_seat != None:
			return True

		return False

	def clearRayProps(self):
		for key in self.doorobj:
			obj = self.doorobj[key]
			obj["COLLIDE"] = []

	def stateSwitch(self, state=None):
		if state != None:
			pass
		elif self.driving_seat == ".":
			state = "DRIVER"
		else:
			state = self.SEATS[self.driving_seat].get("STATE", "DRIVER")

		if state == "IDLE":
			for seat in self.player_seats:
				if self.removeFromSeat(seat) == True:
					if self.driving_seat == seat:
						self.driving_seat = None
			self.active_state = self.ST_Idle
			return

		self.assignCamera()

		if state == "DRIVER":
			self.active_state = self.ST_Active

		elif state == "PASSIVE":
			self.active_state = self.ST_Passive

	## IDLE STATE ##
	def ST_Idle(self):
		owner = self.objects["Root"]

		if self.checkClicked() == True:
			self.stateSwitch()

	## ACTIVE STATE ##
	def ST_Active(self):
		self.getInputs()

		owner = self.objects["Root"]

		if keymap.BINDS["ENTERVEH"].tap() == True:
			self.stateSwitch("IDLE")

	## PASSIVE STATE ##
	def ST_Passive(self):
		owner = self.objects["Root"]
		self.doCameraToggle()
		if keymap.BINDS["ENTERVEH"].tap() == True:
			self.stateSwitch("IDLE")

	## RUN ##
	def PR_ClipZone(self):
		self.checkStability(override=False)

	def PR_SuspensionRig(self):
		owner = self.objects["Root"]

		rig = self.objects.get("Rig", None)
		if  rig == None:
			print("No Wheel Rig", owner.name)
			return "REMOVE"
		if rig.invalid == True:
			print("Wheel Rig Invalid", owner.name)
			return "REMOVE"

		for key in self.wheelobj:
			obj = self.wheelobj[key]
			if obj.invalid == True:
				print("Wheel Obj Invalid", owner.name)
				return "REMOVE"

			#ch = None
			#for i in rig.channels:
			#	if i.name == key:
			#		ch = i

			ct = None
			for i in rig.constraints:
				s = rig.constraints.get(key+":"+i.name, None)
				if s != None:
					ct = s

			if ct != None:
				ct.active = True
				ct.target = obj

			#elif ch != None:
			#	pnt = obj.worldPosition-owner.worldPosition
			#	lp = owner.worldOrientation.inverted()*pnt
			#	ref = owner.scene.objectsInactive[rig.name].channels[key]
			#	offset = ref.bone.arm_head
			#	ch.location = lp-offset

		rig.update()


class LayoutCar(HUD.HUDLayout):

	GROUP = "Core"
	MODULES = [HUD.Stats, HUD.Speedometer, HUD.MousePos, HUD.Compass]

class CoreCar(CoreVehicle):

	MOUSE_ROLL = False
	ENTRY_FLIP = True
	INPUT_SMOOTH = 0.09

	CAM_HEIGHT = 0.2
	CAM_MIN = 0.8
	CAM_SLOW = 6
	CAM_ORBIT = False

	CAR_POWER = 70
	CAR_SPEED = 100
	CAR_BRAKE = 1
	CAR_HANDBRAKE = 1
	CAR_REVERSE = 0.5
	CAR_STEER = 1
	CAR_DRIVE = "FRONT"
	CAR_AIR = (0,0,0)
	CAR_ALIGN = False
	CAR_BAIL = 360

	HUDLAYOUT = LayoutCar

	def defaultData(self):
		dict = super().defaultData()
		dict["ENGINE"] = {
			"Power":self.CAR_POWER,
			"Speed":self.CAR_SPEED
			}

		return dict

	def ST_Active(self):
		owner = self.objects["Root"]

		self.getInputs()

		KB = keymap.BINDS
		engine = self.data["ENGINE"]
		force = self.motion["Force"]
		torque = self.motion["Torque"]

		vel = owner.localLinearVelocity
		speed = abs(vel[1])

		move = KB["VEH_CARACCEL"].axis() - KB["VEH_CARBRAKE"].axis()
		turn = (torque[2]-force[0])
		if turn > 1:
			turn = 1
		if turn < -1:
			turn = -1

		## Steering Optimizer ##
		if speed >= 8:
			turn *= (2/((speed-4)*0.5))

		POWER = 0
		BRAKE = 0
		HANDBRAKE = 0

		## Gas Pedal ##
		if move > 0:
			if vel[1] < -0.1:
				BRAKE = abs(move)
			else:
				POWER = move

		## Brake Pedal ##
		elif move < 0:
			if vel[1] > 0.1:
				BRAKE = abs(move)
			else:
				POWER = move*self.CAR_REVERSE

		if keymap.BINDS["VEH_HANDBRAKE"].active() == True:
			HANDBRAKE = 1-BRAKE

		DRIVE = self.CAR_DRIVE
		BRAKE *= self.CAR_BRAKE
		HANDBRAKE *= self.CAR_HANDBRAKE
		STEER = self.CAR_STEER*turn

		## Apply Engine Force ##
		mx = 1
		if speed > 0.1:
			mx = 1-(speed/self.CAR_SPEED)

		FORCE_F = (engine["Power"]*mx)*-POWER

		if DRIVE == "FRONT":
			if STEER > 0.05:
				self.setWheelPower(FORCE_F*1.5, "Wheel_FR")
				self.setWheelPower(0.0, "Wheel_FL")
			elif STEER < -0.05:
				self.setWheelPower(0.0, "Wheel_FR")
				self.setWheelPower(FORCE_F*1.5, "Wheel_FL")
			else:
				self.setWheelPower(FORCE_F, "FRONT")
		else:
			if STEER > 0.05:
				self.setWheelPower(FORCE_F*1.5, "Wheel_RR")
				self.setWheelPower(0.0, "Wheel_RL")
			elif STEER < -0.05:
				self.setWheelPower(0.0, "Wheel_RR")
				self.setWheelPower(FORCE_F*1.5, "Wheel_RL")
			else:
				self.setWheelPower(FORCE_F, "REAR")

			if DRIVE == "FOUR":
				self.setWheelPower(FORCE_F, "FRONT")
			else:
				self.setWheelPower(0.0, "FRONT")

		air = self.CAR_AIR
		owner.applyTorque((torque[0]*air[0], torque[1]*air[1], torque[2]*air[2]), True)

		## Brake All Wheels ##
		self.setWheelBrake(BRAKE)

		## Brake Rear Wheels ##
		self.setWheelBrake(HANDBRAKE, "REAR")

		## Steer Front Wheels ##
		self.setWheelSteering(STEER, "FRONT")

		## Reset ##
		angle = self.getObjectAngle()
		if angle > 60 and keymap.BINDS["VEH_ACTION"].tap() == True:
			self.alignObject()

		## Align To Ground ##
		if self.CAR_ALIGN == True and self.gravity.length >= 0.1:
			zref = self.gravity.normalized()
			rayto = owner.worldPosition+zref
			fac = 0

			down, pnt, nrm = owner.rayCast(rayto, None, 3, "GROUND", 1, 1, 0)

			if down != None:
				zref = -nrm
				dist = owner.worldPosition-pnt

				if dist.length < 1:
					fac = 1-dist.length

			owner.alignAxisToVect(-zref, 2, (fac*0.08)+0.02)

		#self.rotateCamera()

		self.data["HUD"]["Text"] = str(int(round(speed, 1)))

		if keymap.BINDS["ENTERVEH"].tap() == True or angle > self.CAR_BAIL:
			self.stateSwitch("IDLE")

	def ST_Idle(self):
		owner = self.objects["Root"]

		self.setWheelPower(0)
		self.setWheelBrake(self.CAR_BRAKE, "REAR")

		if self.checkClicked() == True:
			self.stateSwitch()


class LayoutAircraft(HUD.HUDLayout):

	GROUP = "Core"
	MODULES = [HUD.Stats, HUD.Aircraft, HUD.MousePos]

class CoreAircraft(CoreVehicle):

	MOUSE_ROLL = True

	INPUT_LIMIT = 2.0

	LANDACTION = "AircraftRigLand"
	LANDFRAMES = [0, 100]

	CAM_HEIGHT = 0.1
	CAM_MIN = 1
	CAM_SLOW = 3

	SEATS = {
		"Seat_1": {"NAME":"Driver", "DOOR":"Root", "ACTION":"SeatLow", "SPAWN":[-2,0,0]} }

	AERO = {"POWER":1000, "HOVER":0, "LIFT":0.1, "TAIL":10, "DRAG":(1,1,1)}
	HUDLAYOUT = LayoutAircraft

	def defaultData(self):
		self.lift = 0

		dict = super().defaultData()
		dict["POWER"] = 0
		dict["HOVER"] = [0,0]
		dict["HUD"] = {"Power":0, "Lift":0}

		return dict

	def createVehicle(self):
		if self.data.get("LANDSTATE", "LAND") != "LAND":
			return
		super().createVehicle()

	def airDrag(self):

		dampLin = 0.0
		dampRot = (self.objects["Root"].localLinearVelocity[1]*0.002)+0.4

		if dampRot >= 0.7:
			dampRot = 0.7

		self.objects["Root"].setDamping(dampLin, dampRot)

		self.doDragForce()

	def airLift(self):
		owner = self.objects["Root"]
		linV = owner.localLinearVelocity
		grav = self.gravity.length

		self.lift = (linV[1])*self.AERO["LIFT"]*owner.mass*self.air_drag
		mass = owner.mass*grav

		if self.lift > mass:
			self.lift = mass

		owner.applyForce((0,0,self.lift), True)

		#owner["LIFT"] = str(round(self.lift, 1))
		#owner.addDebugProperty("LIFT", True)

	def getEngineForce(self):
		owner = self.objects["Root"]

		force = self.motion["Force"]
		torque = self.motion["Torque"]
		grav = self.gravity.length
		mass = owner.mass

		self.data["POWER"] += force[1]*(self.AERO["POWER"]/100)
		if self.data["POWER"] > self.AERO["POWER"]:
			self.data["POWER"] = self.AERO["POWER"]

		maxR = self.AERO["POWER"]*self.AERO.get("REVERSE", 0)
		if self.data["POWER"] < -maxR:
			self.data["POWER"] = -maxR
		elif self.data["POWER"] < 0:
			if force[1] > -0.1:
				self.data["POWER"] += (self.AERO["POWER"]/100)
				if self.data["POWER"] > 0:
					self.data["POWER"] = 0

		self.data["HOVER"][0] += force[2]*20
		if self.data["HOVER"][0] > 1000 or self.data["HOVER"][1] > 0:
			self.data["HOVER"][0] = 1000
			if force[2] > 0.1:
				self.data["HOVER"][1] += force[2]*20
				if self.data["HOVER"][1] > self.AERO["HOVER"]:
					self.data["HOVER"][1] = self.AERO["HOVER"]
			else:
				self.data["HOVER"][1] -= 20
				if self.data["HOVER"][1] < 0:
					self.data["HOVER"][1] = 0

		if self.data["HOVER"][0] < 0:
			self.data["HOVER"][0] = 0

		## FORCES ##
		power = self.data["POWER"]
		normal = self.data["HOVER"][0]/1000
		base = normal*grav*mass
		hover = ( (base-(self.lift*normal)) + self.data["HOVER"][1] )

		return power, hover

	def doDragForce(self, drag=None, scale=None):
		linV = self.objects["Root"].localLinearVelocity
		if scale == None:
			scale = abs(linV[1])
		if drag == None:
			drag = self.AERO["DRAG"]
		scale *= self.air_drag
		DRAG_X = linV[0]*drag[0]*scale
		DRAG_Y = linV[1]*drag[1]*scale
		DRAG_Z = linV[2]*drag[2]*scale

		self.objects["Root"].applyForce((-DRAG_X, -DRAG_Y, -DRAG_Z), True)

	def doLandingGear(self, init=False):
		start, end = self.LANDFRAMES

		if init == True:
			if self.doLandingGear not in self.active_post:
				self.active_post.append(self.doLandingGear)
			self.data["LANDFRAME"] = self.data.get("LANDFRAME", 0)
			self.data["LANDSTATE"] = self.data.get("LANDSTATE", "LAND")

		if self.data["LANDSTATE"] == "LAND":
			if init == True:
				self.doAnim(NAME=self.LANDACTION, FRAME=(start,start))
				return

			if self.data["LANDFRAME"] == abs(end-start):
				self.doAnim(NAME=self.LANDACTION, FRAME=(end,start))
			if self.data["LANDFRAME"] == 1:
				self.createVehicle()
			if self.data["LANDFRAME"] > 0:
				self.data["LANDFRAME"] -= 1

		elif self.data["LANDSTATE"] == "FLY":
			if init == True:
				self.doAnim(NAME=self.LANDACTION, FRAME=(end,end))
				return

			if self.data["LANDFRAME"] == 0:
				self.doAnim(NAME=self.LANDACTION, FRAME=(start,end))
				self.removeVehicle()
			if self.data["LANDFRAME"] < abs(end-start):
				self.data["LANDFRAME"] += 1

		elif init == True:
			f = start
			if self.vehicle_constraint == None:
				f = end
			self.doAnim(NAME=self.LANDACTION, FRAME=(f,f))

		if self.driving_seat == None:
			return

		owner = self.getOwner()
		rayto = owner.worldPosition+owner.getAxisVect((0,0,-1))
		dist = self.WH_HEIGHT+self.VEH_LENGTH+self.WH_RADIUS
		ground = owner.rayCastTo(rayto, dist, "GROUND")

		if ground == None and keymap.BINDS["VEH_ACTION"].tap() == True:
			if self.data["LANDSTATE"] == "LAND" and self.data["LANDFRAME"] == 0:
				self.data["LANDSTATE"] = "FLY"
			elif self.data["LANDSTATE"] == "FLY" and self.data["LANDFRAME"] == abs(end-start):
				self.data["LANDSTATE"] = "LAND"



