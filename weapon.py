####
# bge_game3_core: Full python game structure for the Blender Game Engine
# Copyright (C) 2019  DaedalusMDW @github.com (Daedalus_MDW @blenderartists.org)
# https://github.com/DaedalusMDW/bge_game3_core
#
# This file is part of bge_game3_core.
#
#    bge_game3_core is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    bge_game3_core is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with bge_game3_core.  If not, see <http://www.gnu.org/licenses/>.
#
####

## WEAPON CORE ##


from bge import logic

from . import attachment, keymap


class CoreWeapon(attachment.CoreAttachment):

	NAME = "Weapon"
	ENABLE = False
	SLOTS = []
	COLOR = (0,1,0,1)
	TYPE = "RANGED"

	def assignToPlayer(self):
		super().assignToPlayer()

		slot = self.owning_slot
		cls = self.owning_player

		weap = cls.data["WPDATA"]["WHEEL"]

		if self.TYPE not in weap:
			weap[self.TYPE] = {"ID":-1, "LIST":[]}

		if slot not in weap[self.TYPE]["LIST"]:
			weap[self.TYPE]["LIST"].append(slot)

		if self.data["ENABLE"] == True and cls.active_weapon == None:
			cls.active_weapon = self

	def removeFromPlayer(self):
		super().removeFromPlayer()

		slot = self.owning_slot
		cls = self.owning_player

		self.data["ENABLE"] = False

		if cls == None:
			return

		weap = cls.data["WPDATA"]["WHEEL"][self.TYPE]

		if slot in weap["LIST"]:
			weap["LIST"].remove(slot)

		if weap["ID"] >= len(weap["LIST"]):
			weap["ID"] = len(weap["LIST"])-1

		if cls.active_weapon == self:
			cls.active_weapon = None

	def stateSwitch(self, state=None, force=False):
		if state == None:
			if self.data["ENABLE"] == True:
				state = False
			else:
				state = True
		elif state != True:
			state = False

		if force == False:
			if self.data["COOLDOWN"] != 0:
				return False
			if self.data["ENABLE"] == state:
				return True

		if self.box != None:
			return False

		if state == True:
			self.active_state = self.ST_Enable

			try:
				slot = self.owning_slot
				cls = self.owning_player

				wd = cls.data["WPDATA"]
				weap = wd["WHEEL"][self.TYPE]

				wd["CURRENT"] = self.TYPE
				weap["ID"] = weap["LIST"].index(slot)
			except:
				pass

		else:
			self.active_state = self.ST_Stop

		return False

	def ST_Enable(self):
		self.data["ENABLE"] = True
		self.active_state = self.ST_Active

		try:
			cls = self.owning_player
			if cls.active_weapon == None:
				cls.active_weapon = self
		except:
			pass

	def ST_Stop(self):
		self.data["ENABLE"] = False
		self.active_state = self.ST_Idle

		try:
			cls = self.owning_player
			wd = cls.data["WPDATA"]

			if cls.active_weapon == self and wd["ACTIVE"] != "SWITCH":
				cls.active_weapon = None
		except:
			pass


class CorePlayerWeapon(CoreWeapon):

	HAND = "AUTO"
	WAIT = 40

	def defaultStates(self):
		super().defaultStates()
		self.active_post.append(self.PS_HandSocket)

	def defaultData(self):
		self.handobj = None

		dict = super().defaultData()
		dict["HAND"] = None

		return dict

	def assignToPlayer(self):
		super().assignToPlayer()

		try:
			cls = self.owning_player
			hand = cls.HAND.get(self.data["HAND"], None)
			if self.data["ENABLE"] == True and hand != None:
				self.handobj = cls.objects["SKT"][hand]
			else:
				self.handobj = None
				self.data["HAND"] = None
		except:
			self.handobj = None
			self.data["HAND"] = None

	def removeFromPlayer(self):
		super().removeFromPlayer()

		self.handobj = None
		self.data["HAND"] = None

	def attachToSocket(self, obj=None, socket=None):
		if socket == None:
			return
		if obj == None:
			obj = self.owner

		obj.setParent(socket)
		obj.localPosition = self.createVector()
		obj.localOrientation = self.createMatrix()
		obj.localScale = (1,1,1)

	def checkHand(self):
		plr = self.owning_player
		if plr == None:
			return None

		if self.HAND == "AUTO":
			l = ["MAIN"]

			pri = self.getFirstEvent("WP_HAND", "MAIN")
			sec = self.getFirstEvent("WP_HAND", "OFF")
			if pri != None or sec != None:
				l = []
			if pri != None:
				l.append("MAIN")
			if sec != None:
				l.append("OFF")

		else:
			l = [self.HAND]

		for h in l:
			try:
				hand = plr.HAND[h]
				if plr.active_weapon != None and h == "MAIN":
					return h
				if len(plr.objects["SKT"][hand].children) == 0:
					return h
			except:
				continue

		return None

	def doPlayerAnim(self, frame=0):
		plr = self.owning_player

		h = self.data["HAND"]
		hand = plr.HAND.get(h, None)
		if hand == None or plr == None:
			return

		anim = self.TYPE+hand+self.owning_slot
		start = 0
		end = 40
		lyr = 1+(h=="OFF")

		if frame == "LOOP":
			plr.doAnim(NAME=plr.ANIMSET+anim, FRAME=(end,end), LAYER=lyr, PRIORITY=2, MODE="LOOP")
		elif frame == "STOP":
			plr.doAnim(LAYER=lyr, STOP=True)
		elif type(frame) is int:
			plr.doAnim(NAME=plr.ANIMSET+anim, FRAME=(start,end), LAYER=lyr)
			fac = (frame/self.WAIT)
			if frame < 0:
				fac = 1+fac
			plr.doAnim(LAYER=lyr, SET=fac*end)

	def PS_HandSocket(self):
		mesh = self.objects["Mesh"]
		sock = self.objects["Socket"]

		self.objects["Socket"].visible = True
		if self.handobj != None:
			sock = self.handobj
		if self.box != None:
			self.objects["Socket"].visible = False
			sock = self.owner

		if mesh.parent != sock:
			self.attachToSocket(mesh, sock)

	## STATE TRANSITION ##
	def ST_Enable(self):
		slot = self.owning_slot
		cls = self.owning_player
		if cls == None:
			return

		if self.data["HAND"] == None:
			h = self.checkHand()
			if h == None:
				self.data["ENABLE"] = False
				self.data["COOLDOWN"] = 0
				self.data["HAND"] = None
				self.active_state = self.ST_Idle
				return
			else:
				wd = cls.data["WPDATA"]
				weap = wd["WHEEL"][self.TYPE]

				wd["CURRENT"] = self.TYPE
				weap["ID"] = weap["LIST"].index(slot)
				if cls.active_weapon == None:
					cls.active_weapon = self

				self.data["HAND"] = h

		h = self.data["HAND"]

		if h == "MAIN" and cls.active_weapon != self:
			return

		if self.data["COOLDOWN"] >= self.WAIT*0.5:
			try:
				hand = cls.HAND[h]
				self.handobj = cls.objects["SKT"][hand]
			except:
				self.handobj = None

		self.data["COOLDOWN"] += 1
		self.doPlayerAnim(self.data["COOLDOWN"])

		if self.data["COOLDOWN"] >= self.WAIT:
			self.data["COOLDOWN"] = 0
			super().ST_Enable()
			#self.active_state = self.ST_Active

	def ST_Stop(self):
		slot = self.owning_slot
		cls = self.owning_player
		if cls == None:
			return

		self.data["COOLDOWN"] += 1

		if self.data["HAND"] == None or self.dict["Equiped"] == "DROP":
			self.data["COOLDOWN"] = self.WAIT
		else:
			self.doPlayerAnim(self.WAIT-self.data["COOLDOWN"])

		if self.data["COOLDOWN"] >= self.WAIT*0.5:
			self.handobj = None

		if self.data["COOLDOWN"] >= self.WAIT:
			self.doPlayerAnim("STOP")
			self.data["COOLDOWN"] = 0
			self.data["HAND"] = None
			super().ST_Stop()
			#self.active_state = self.ST_Idle


