####
# bge_game3_core: Full python game structure for the Blender Game Engine
# Copyright (C) 2019  DaedalusMDW @github.com (Daedalus_MDW @blenderartists.org)
#
# This file is part of bge_game3_core.
#
#    bge_game3_core is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    bge_game3_core is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with bge_game3_core.  If not, see <http://www.gnu.org/licenses/>.
#
####

from bge import logic, render

from game3 import GAMEPATH, base, keymap, config, settings


class CoreWorldTile(base.CoreObject):

	NAME = "World Tile"
	INTERACT = False
	GHOST = False
	LOD_ACTIVE = 1000
	LOD_FREEZE = 2000
	LOD_PROXY = 3000
	OBJ_HIGH = ["Mesh", "Smooth", "Details", "COL"]
	OBJ_LOW  = ["Mesh"]
	OBJ_PROXY = ["LOW"]

	def defaultData(self):
		self.lod_objects = {}

		dict = super().defaultData()
		dict["GROUP"] = self.owner.get("GROUP", "")

		return dict

	def setLodObjects(self, stuff=[], group=""):
		owner = self.owner
		group = owner.name+group
		scene = owner.scene

		for cat in self.lod_objects:
			obj = self.lod_objects[cat]
			if cat not in stuff and obj != None:
				obj.endObject()
				self.lod_objects[cat] = None
				#print("LOD END", cat)

		for cat in stuff:
			name = ".".join([group, cat])
			if self.lod_objects.get(cat, None) == None and name in scene.objectsInactive:
				obj = scene.addObject(name, owner, 0)
				obj.setParent(owner, False, False)
				for chk in obj.childrenRecursive+[obj]:
					for prop in ["GROUND", "CAMERA"]:
						if chk.get(prop, True) == True:
							chk[prop] = True
						else:
							del chk[prop]
				self.lod_objects[cat] = obj
				#print("LOD ADD", name)

	def setLodState(self, lod):
		if lod == self.lod_state:
			return

		self.lod_state = lod
		self.data["LOD_STATE"] = lod

		obj = self.getOwner()
		grp = self.data["GROUP"]

		if lod == "ACTIVE":
			self.setLodObjects(self.OBJ_HIGH, grp)
			#self.setPhysicsType("RESUME")
			self.loadChildren()
			for cls in self.getChildren():
				cls.setPhysicsType("RESUME")
			#print("LOD ACTIVE", self)

		elif lod == "FREEZE":
			self.setLodObjects(self.OBJ_LOW)
			#self.setPhysicsType("FREEZE")
			self.loadChildren()
			for cls in self.getChildren():
				cls.setPhysicsType("FREEZE")
			#print("LOD FREEZE", self)

		elif lod == "PROXY":
			for cls in self.getChildren():
				cls.packObject()
			self.setLodObjects(self.OBJ_PROXY)
			#print("LOD PROXY", self)

		elif lod == "EMPTY":
			for cls in self.getChildren():
				cls.packObject()
			self.setLodObjects()
			#print("LOD EMPTY", self)

	def getLodLevel(self):
		if base.ORIGIN_SHIFT != None:
			return self.lod_state

		dist = logic.VIEWPORT.getWorldPosition()-self.getOwner().worldPosition
		dist = dist.length

		if dist < self.LOD_ACTIVE:
			return "ACTIVE"
		elif dist < self.LOD_FREEZE:
			return "FREEZE"
		elif dist < self.LOD_PROXY:
			return "PROXY"
		else:
			return "EMPTY"

	def ST_Disabled(self):
		lod = self.getLodLevel()
		if self.lod_state != lod:
			self.setLodState(lod)


class DynamicWorldTile(CoreWorldTile):

	def loadContainer(self):
		self.container_seagulls = []
		super().loadContainer()
		self.buildBorders()

	def setContainerParent(self, cls, slot=None):
		if self.CONTAINER == "WORLD":
			if cls not in self.container_seagulls:
				self.container_seagulls.append(cls)
		super().setContainerParent(cls, slot)

	def applyContainerProps(self, cls):
		cls.gravity = self.owner.getAxisVect([0,0,1]).normalized()*-9.8
		cls.air_drag = 1

	def buildBorders(self):
		self.borders = []
		for obj in self.owner.childrenRecursive:
			if obj.get("BORDERPATROL", False) == True:
				mesh = obj.meshes[0]
				polys = []
				for p in range(mesh.numPolygons):
					p = mesh.getPolygon(p)
					n = p.getNumVertex()
					face = []
					dot = self.createVector()
					for i in range(n):
						m = p.getMaterialIndex()
						v = p.getVertexIndex(i)
						vert = mesh.getVertex(m, v).XYZ
						for i in [0,1,2]:
							vert[i] *= obj.localScale[i]
						if obj.get("DYNAMIC", False) == False:
							vert = self.getWorldSpace(obj, vert)
							vert = self.getLocalSpace(self.owner, vert)
						face.append(vert)
						dot += vert

					dot = dot/n
					nrm = base.mathutils.geometry.normal(face)
					polys.append((nrm, dot))

				if obj.get("DYNAMIC", False) == False:
					obj.endObject()
					obj = None

				self.borders.append((obj, polys))

		self.active_pre.insert(0, self.PR_BorderPatrol)

	def checkCoords(self, cls):
		if cls.CONTAINER == "LOCK" or cls == self:
			return None
		if cls.invalid == True:
			return False

		for obj, polys in self.borders:
			for chkpnt in cls.getOriginBounds():
				if obj == None:
					pnt = self.getLocalSpace(self.owner, chkpnt)
				else:
					pnt = self.getLocalSpace(obj, chkpnt)

				check = True
				for nrm, dot in polys:
					if nrm.length > 0.01:
						dist = base.mathutils.geometry.distance_point_to_plane(pnt, dot, nrm)
						if dist > 0:
							check = False
							break
					else:
						print("NaN", cls.NAME, obj)

				if check == True:
					return True

		return False

	def PR_BorderPatrol(self):
		if self.lod_state != "ACTIVE":
			self.container_seagulls = []
			return
		if base.ORIGIN_SHIFT != None:
			return

		search = logic.UPDATELIST.copy()
		if self.container_parent != None:
			search = search+self.container_parent.getChildren()
		for ch in self.getChildren():
			if hasattr(ch, "container_seagulls") == False:
				continue
			if self not in ch.container_seagulls:
				ch.container_seagulls.append(self)
			for gull in self.container_seagulls:
				if gull not in ch.container_seagulls:
					ch.container_seagulls.append(gull)
		for gull in self.container_seagulls:
			search = search+gull.getChildren()
		self.container_seagulls = []

		addlist = []
		for cls in search:
			if self.checkCoords(cls) == True and cls not in addlist:
				addlist.append(cls)

		for cls in addlist:
			if cls not in self.getChildren() and cls.dict["Parent"] in [None, True]:
				#if cls.CONTAINER != "WORLD":
				#	print(self.owner, "("+self.NAME+")", "ADD", cls.owner)
				cls.setContainerParent(self)

		for cls in self.getChildren():
			if cls.dict["Parent"] == True and self.checkCoords(cls) == False:
				#print(self.owner, "("+self.NAME+")", "REMOVE", cls.owner, cls.owner.worldPosition+base.ORIGIN_OFFSET)
				cls.removeContainerParent()


def sendObjects(map, door, send, zone=None):
	portal = {"Send":[]}

	for cls in send:
		cls.removeContainerParent()
		if cls in base.PLAYER_CLASSES.values():
			portal["Send"].append(cls.dict["Object"])
		else:
			portal["Send"].append(cls.dict)

		if zone != None:
			lp, lr = cls.getTransformDiff(zone, cmp=True)
			cls.dict["Zone"] = [lp, lr]

		cls.packObject(True, True)

	base.PROFILE["Portal"][map+door] = portal


def loadObjects(door, owner):
	if base.GAME_STATE not in ["DONE", "SPAWN"]:
		return

	newmap = str(base.CURRENT["Level"])
	portal = base.PROFILE["Portal"].get(newmap+door, None)

	if portal == None:
		return []

	send = portal.get("Send", [])

	spawns = []
	for drop in send:
		if type(drop) == str:
			name = drop
			drop = base.PROFILE["PLRData"][name]
			pid = None
			for i in base.WORLD["PLAYERS"]:
				if base.WORLD["PLAYERS"][i] == name:
					pid = i
			if name not in base.PLAYER_CLASSES and pid != None:
				obj = base.SC_SCN.addObject(name, owner, 0)
				obj["DICT"] = drop
				obj["PID"] = pid
		else:
			obj = base.SC_SCN.addObject(drop["Object"], owner, 0)
			obj["DICT"] = drop

		if door != None:
			pos = owner.worldPosition.copy()
			ori = owner.worldOrientation.copy()

			if drop.get("Zone", None) != None:
				lp = base.mathutils.Vector(drop["Zone"][0])
				lr = base.mathutils.Matrix(drop["Zone"][1])

				pos = pos+(ori*lp)
				ori = ori*lr
				drop["Zone"] = None

			obj.worldPosition = pos
			obj.worldOrientation = ori

		cls = base.GETCLASS(obj)
		spawns.append(obj)

		print("PORTAL:", obj.name, obj.worldPosition)

	del base.PROFILE["Portal"][newmap+door]

	return spawns


MAPBLEND = None
def openBlend(map, scn=None):
	gd = logic.globalDict

	if map == "RESUME":
		if "NEWLEVEL" not in base.WORLD:
			return
		map = base.WORLD["NEWLEVEL"]
		scn = base.WORLD["NEWSCENE"]
		blend = config.MAPS_PATH+"\\"+map
	elif map == "LAUNCHER":
		blend = config.LAUNCHER_BLEND+".blend"
	elif map == "KEYMAP":
		blend = config.KEYMAP_BLEND+".blend"
	else:
		base.WORLD["NEWLEVEL"] = map
		base.WORLD["NEWSCENE"] = scn
		blend = config.MAPS_PATH+"\\"+map

	gd["TRAVELING"] = True

	for cls in logic.UPDATELIST.copy():
		cls.doUpdate()
	if logic.VIEWPORT != None:
		logic.VIEWPORT.doUpdate()

	logic.UPDATELIST = []

	print("OPEN MAP:\n\t"+blend)

	from . import HUD
	HUD.SetLayout(None, HUD.LayoutLoading)

	if config.UPBGE_FIX == True:
		settings.SaveJSON(GAMEPATH+"gd_dump", gd, "\t")

	global MAPBLEND
	MAPBLEND = blend
	lsr = logic.getSceneList()[-1]
	if MAPLOADER not in lsr.post_draw:
		lsr.post_draw.append(MAPLOADER)
	base.GAME_STATE = "BLEND"

def MAPLOADER():
	for scn in logic.getSceneList():
		if MAPLOADER in scn.post_draw:
			scn.post_draw.remove(MAPLOADER)
	logic.startGame(settings.ospath.normpath(GAMEPATH+MAPBLEND))

