####
# bge_game3_core: Full python game structure for the Blender Game Engine
# Copyright (C) 2019  DaedalusMDW @github.com (Daedalus_MDW @blenderartists.org)
# https://github.com/DaedalusMDW/bge_game3_core
#
# This file is part of bge_game3_core.
#
#    bge_game3_core is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    bge_game3_core is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with bge_game3_core.  If not, see <http://www.gnu.org/licenses/>.
#
####

## GAME CORE ##


import importlib
import math, time
import mathutils

from bge import logic

from . import settings, keymap, config, COREBLENDS, GAMEPATH


SC_SCN = None
SC_HUD = None
SC_CAM = None
SC_RUN = None

CURRENT = logic.globalDict["CURRENT"]
PROFILE = logic.globalDict["PROFILES"][CURRENT["Profile"]]
DATA = logic.globalDict["DATA"]
WORLD = PROFILE["GLBData"]
LEVEL = None

GAME_STATE = "INIT"
ACTIVE_LIBLOAD = None
ORIGIN_SHIFT = None
ORIGIN_OFFSET = mathutils.Vector([0,0,0])
GAME_TIME = time.clock()
PLAYER_CLASSES = {}


def DO_ORIGIN(owner):
	global ORIGIN_SHIFT, ORIGIN_OFFSET, GAME_STATE

	mode = LEVEL.get("ORIGIN_SHIFT", True)
	if mode == False:
		return None
	if GAME_STATE != "DONE" or ORIGIN_SHIFT != None:
		ORIGIN_SHIFT = None
		return None

	ori = None
	pos = logic.VIEWPORT.getWorldPosition()

	axislist = [0,1,2]
	if mode != True and type(mode) is str:
		axislist = []
		if "X" in mode:
			axislist.append(0)
		if "Y" in mode:
			axislist.append(1)
		if "Z" in mode:
			axislist.append(2)

	for i in axislist:
		mx = 1000
		if abs(pos[i]) >= 1000:
			while abs(abs(pos[i])-abs(mx)) >= 1000:
				mx += 1000
			if ori == None:
				ori = mathutils.Vector([0,0,0])
			ori[i] = mx*(1-(2*(pos[i]<0)))

	if ori != None:
		print("ORIGIN", ori)
		ORIGIN_OFFSET += ori
		for obj in owner.scene.objects:
			if obj.parent == None:
				obj.worldPosition -= ori

	ORIGIN_SHIFT = ori

	LEVEL["ORIGIN_OFFSET"] = list(ORIGIN_OFFSET)

	owner["POSITION"] = ", ".join([
		str(round(pos[0], 1)),
		str(round(pos[1], 1)),
		str(round(pos[2], 1)) ])
	owner["ORIGIN"] = ", ".join([
		str(round(ORIGIN_OFFSET[0], 1)),
		str(round(ORIGIN_OFFSET[1], 1)),
		str(round(ORIGIN_OFFSET[2], 1)) ])
	owner.addDebugProperty("POSITION", True)
	owner.addDebugProperty("ORIGIN", True)


def MAIN(cont):
	owner = cont.owner

	if SC_SCN.active_camera == None:
		print("-- CRITICAL ERROR --  CAMERA NONE!")
		SC_SCN.active_camera = SC_CAM

	DO_ORIGIN(owner)

	logic.OLDUPDATELIST = []
	logic.ORPHANUPDATE = []

	for UPDATE in [logic.UPDATELIST, logic.ORPHANUPDATE]:
		for cls in UPDATE.copy():
			try:
				if cls not in logic.OLDUPDATELIST:
					logic.OLDUPDATELIST.append(cls)
					cls.air_drag = SC_SCN.gravity.length/9.8
					cls.gravity = SC_SCN.gravity.copy()
					cls.RUN()
			except Exception as ex:
				if cls in logic.UPDATELIST:
					logic.UPDATELIST.remove(cls)
				print("FATAL RUNTIME ERROR:", cls.__class__, cls.owner)
				print("\t", ex)

	#try:
	logic.VIEWPORT.RUN()
	#except Exception as ex:
	#	logic.VIEWPORT = None
	#	print("FATAL RUNTIME ERROR:", "CoreViewport")
	#	print("\t", ex)

	if keymap.SYSTEM["SCREENSHOT"].tap() == True:
		logic.globalDict["SCREENSHOT"]["Trigger"] = True


def LIBCB(status):
	global ACTIVE_LIBLOAD
	ACTIVE_LIBLOAD = None

def CLOCKTIME():
	global GAME_TIME, GAME_STATE
	if GAME_STATE == "SPAWN":
		return

	if config.FPS_CAP > 1 and logic.globalDict["GRAPHICS"]["Vsync"] == False:
		while (time.clock()-GAME_TIME) < (1/config.FPS_CAP):
			pass

	GAME_TIME = time.clock()

	if config.UPBGE_FIX == True:
		logic.setClockTime(logic.getClockTime()+(1/60))


def GAME(cont):
	global CURRENT, PROFILE, DATA, WORLD, LEVEL, ACTIVE_LIBLOAD, GAME_STATE

	owner = cont.owner
	scene = owner.scene

	spawn = owner.get("SPAWN", True)
	timer = owner.get("TIMER", None)
	black = owner.get("GFXBG", None)

	if spawn == False:
		MAIN(cont)
		GAME_STATE = "DONE"
		return "DONE"

	keymap.MOUSELOOK.center()

	if GAME_STATE in ["INIT"]:
		if config.UPBGE_FIX == True and config.EMBEDDED_FIX == False:
			logic.setRender(False)

	## SET SCENE ##
	if WORLD.get("NEWSCENE", None) != None and config.EMBEDDED_FIX == False:
		if WORLD["NEWSCENE"] != scene.name:
			if scene.replace(str(WORLD["NEWSCENE"])) == True:
				CURRENT["Level"] = None
				print(scene.name, WORLD["NEWSCENE"])
				GAME_STATE = "SCENE"
				return "SCENE"
			else:
				print("NOTICE: Scene Not Found...", WORLD["NEWSCENE"])

	global SC_SCN, SC_RUN, SC_CAM

	if GAME_STATE in ["INIT", "SCENE"]:
		SC_SCN = scene
		SC_RUN = owner
		SC_CAM = scene.active_camera
		settings.applyGraphics()
		cont.useHighPriority = True

	CURRENT["Scene"] = scene.name

	## LEVEL DATA ##
	if WORLD.get("NEWLEVEL", None) != None:
		CURRENT["Level"] = WORLD["NEWLEVEL"]

	if CURRENT["Level"] == None and owner.get("MAP", None) != None:
		CURRENT["Level"] = owner["MAP"]+".blend"

	newmap = str(CURRENT["Level"])+scene.name

	WORLD["NEWLEVEL"] = CURRENT["Level"]
	WORLD["NEWSCENE"] = CURRENT["Scene"]

	if newmap not in PROFILE["LVLData"]:
		print("Initializing Level Data...", newmap)
		PROFILE["LVLData"][newmap] = settings.GenerateLevelData()

	LEVEL = PROFILE["LVLData"][newmap]

	if LEVEL.get("ORIGIN_OFFSET", None) == None:
		LEVEL["ORIGIN_OFFSET"] = [0,0,0]

	## EARLY BAIL ##
	if config.EMBEDDED_FIX == True:
		GAME_STATE = "EMBEDDED"
		return "EMBEDDED"

	if config.FPS_CAP >= 0:
		if CLOCKTIME not in scene.post_draw:
			if config.UPBGE_FIX == True:
				logic.setUseExternalClock(True)
			scene.post_draw.append(CLOCKTIME)

		if config.UPBGE_FIX == True:
			if logic.getRender() == False:
				CLOCKTIME()

	if black == None:
		owner["GFXBG"] = True
		GAME_STATE = "BLACK"
		return "BLACK"

	## LIBLOAD ##
	if owner.get("LIBLIST", None) == None:
		owner["LIBLIST"] = [COREBLENDS["ASSETS"]]
		for libblend in config.LIBRARIES:
			libblend = settings.ospath.normpath(GAMEPATH+config.LIBRARY_PATH+"\\"+libblend+".blend")
			owner["LIBLIST"].append(libblend)
		GAME_STATE = "LIBLOAD"
		return "LIBLOAD"

	if ACTIVE_LIBLOAD != None:
		GAME_STATE = "LIBLOAD"
		return "LIBLOAD"

	if len(owner["LIBLIST"]) > 0:
		libblend = owner["LIBLIST"].pop(0)
		#if config.LIBLOAD_TYPE != "ASYNC":
		logic.LibLoad(libblend, "Scene", load_actions=True, verbose=False, load_scripts=True) #, asynchronous=True)
		print("LIBRARY:", libblend)
		#elif config.LIBLOAD_TYPE == "ASYNC":
		#	ACTIVE_LIBLOAD = logic.LibLoad(libblend, "Scene", load_actions=True, verbose=False, load_scripts=True, async=True)
		#	ACTIVE_LIBLOAD.onFinish = LIBCB
		#	print("ASYNC:", libblend)

		GAME_STATE = "LIBLOAD"
		return "LIBLOAD"

	## HUD SCENE ##
	if timer == None:
		owner["TIMER"] = 0

		owner.worldScale = [1,1,1]
		logic.addScene("HUD", 1)
		GAME_STATE = "HUD"
		return "HUD"

	elif timer <= 30:
		owner["TIMER"] += 1
		GAME_STATE = "TIMER"
		return "TIMER"

	## SPAWN ##
	if "CLIP" in owner:
		LEVEL["CLIP"] = owner["CLIP"]
	if "ORIGIN_SHIFT" in owner:
		LEVEL["ORIGIN_SHIFT"] = owner["ORIGIN_SHIFT"]

	if spawn == True:
		GAME_STATE = "SPAWN"
		try:
			LOAD(owner)
		except Exception as ex:
			print("SPAWN LOOP ERROR:", ex)
			owner.endObject()

		if config.UPBGE_FIX == True:
			logic.setRender(True)

		owner["GAMELOOP"] = owner
		owner["SPAWN"] = False
		MAIN(cont)
		return "SPAWN"

	GAME_STATE = "WAIT"
	return "WAIT"


def GETCLASS(obj):
	path = None
	for cont in obj.controllers:
		path = getattr(cont, "script", path)

	if path == None:
		print("SPAWN ERROR: No Class Pointer Found", obj)
		return None

	try:
		path = path.split(".")
		name = path.pop()
		path = ".".join(path)
		mod = importlib.import_module(path)
		ref = getattr(mod, name)
		cls = ref(obj)
		logic.OLDUPDATELIST.append(cls)
		cls.RUN()
		cls.dict["Portal"] = None
		return cls
	except Exception as ex:
		print("CLASS INIT ERROR:", path, obj)
		print("\t", ex)


def LOAD(owner):
	global CURRENT, PROFILE, DATA, WORLD, LEVEL, PLAYER_CLASSES

	scene = owner.scene

	from . import viewport
	logic.VIEWPORT = viewport.CoreViewport()

	## Ground Detector ##
	for obj in scene.objects:
		gnd = obj.get("GROUND", None)
		cam = obj.get("CAMERA", None)
		if gnd == False:
			del obj["GROUND"]
		elif obj.getPhysicsId() != 0:
			obj["GROUND"] = True
		if cam == False:
			del obj["CAMERA"]
		else:
			obj["CAMERA"] = True

	## Ready Player ##
	if "PLAYERS" not in WORLD:
		WORLD["PLAYERS"] = {"1":None}

	for pid in WORLD["PLAYERS"]:
		player = WORLD["PLAYERS"][pid]

		if player == None:
			player = config.DEFAULT_PLAYER

			if "PLAYER" in owner:
				player = owner["PLAYER"]

		if player not in scene.objectsInactive:
			player = config.DEFAULT_PLAYER

		if player not in PROFILE["PLRData"]:
			PROFILE["PLRData"][player] = {"Object":player, "Data":None, "ID":None}

		WORLD["PLAYERS"][pid] = player

		print("READY PLAYER:", pid, player)

	## Spawn New Objects ##
	cleanup = []
	spawns = []
	objchk = LEVEL["SPAWN"].copy()

	for obj in scene.objects:
		split = obj.name.split(".")
		name = None

		if obj != owner and len(split) > 1:
			if split[0] == "Spawn":
				name = obj.get("OBJECT", split[1])

		if name != None:
			cleanup.append(obj)
			obj.worldScale = [1,1,1]

			if obj.get("SPAWN", True) == True and obj.name not in objchk:
				if name != None and name in scene.objectsInactive:
					dict = {"Object":name, "Data":None}
					spawns.append((obj, dict))

					print("SPAWNED:", name, obj.worldPosition)
				else:
					print("SPAWN ERROR: Object '"+name+"' does not exist")
			else:
				print("NOT SPAWNED:", name)

	## Load Cyberspace Objects ##
	newmap = str(CURRENT["Level"])+str(CURRENT["Scene"])

	for drop in (WORLD.get("DROP",[])+LEVEL["DROP"]):
		name = drop["Object"]
		if name != None and name in scene.objectsInactive:
			spawns.append((None, drop))
			print("CYBERSPACE:", name)
		else:
			print("CYBERSPACE ERROR: Object '"+name+"' does not exist")

	WORLD["DROP"] = []
	LEVEL["DROP"] = []

	## Spawn the Things ##
	for obj, dict in spawns:
		name = dict["Object"]
		if obj == None:
			newobj = scene.addObject(name, owner, 0)
		else:
			newobj = scene.addObject(name, obj, 0)
			for prop in obj.getPropertyNames():
				if prop not in ["SPAWN", "OBJECT", "DICT", "PARENT", "GROUND", "CAMERA"]:
					newobj[prop] = obj[prop]
		newobj["DICT"] = dict
		cls = GETCLASS(newobj)
		if cls.UPDATE == True and obj != None:
			LEVEL["SPAWN"].append(obj.name)

	for obj in cleanup:
		obj.endObject()

	for key in PROFILE["Portal"]:
		if CURRENT["Level"] in key:
			print("... PORTAL SPAWN STOP ...")
			return

	## Add Player if Needed ##
	for pid in WORLD["PLAYERS"]:
		name = WORLD["PLAYERS"][pid]
		dict = PROFILE["PLRData"][name]

		if dict.get("Portal", None) != True and name not in PLAYER_CLASSES:
			newplr = scene.addObject(name, owner, 0)
			newplr["DICT"] = dict
			newplr["PID"] = pid
			GETCLASS(newplr)
			print("PLAYER:", name)


class CoreEvent:

	def __init__(self, id, fr, tg, dt):
		self.event = id
		self.sender = fr

		self.tags = tg
		self.data = dt

	def getEvent(self, id=None):
		if id == None:
			return self.event
		return (self.event==id)

	def getProp(self, key, default=None):
		return self.data.get(key, default)

	def hasTag(self, item):
		return (item in self.tags)

	def hasProp(self, key):
		return (key in self.data)

	def hasAllTags(self, filter):
		for item in filter:
			if "!" in item:
				item = item.strip("!")
				if item in self.tags:
					return False
			else:
				if item not in self.tags:
					return False
		return True

	def hasAllProps(self, filter):
		for key in filter:
			if key not in self.data:
				return False
		return True


class CoreObject:

	NAME = "Object"
	UPDATE = True
	INTERACT = True
	GHOST = False
	PHYSICS = "NONE"
	LOCK = ""
	WORLDDOOR = False
	CONTAINER = "ALLOW"

	def __init__(self, *args):
		if len(args) == 0:
			return
		self.owner = args[0]

		self.owner["Class"] = self

		self.ANIMOBJ = None

		#if self.INTERACT == True:
		#	self.owner["RAYCAST"] = self.owner.get("RAYCAST", None)
		#	self.owner["RAYNAME"] = self.NAME

		self.objects = {"Root":self.owner}

		self.dict = self.owner["DICT"]
		self.data = self.defaultData()

		self.defaultEvents()
		self.defaultStates()

		self.setPhysicsGrid()
		self.setPhysicsType()

		self.checkGhost(self.owner)
		self.findObjects(self.owner)
		self.doLoad()

		self.ST_Startup()

	def defaultStates(self):
		self.active_pre = []
		self.active_state = self.ST_Disabled
		self.active_post = []

	def defaultData(self):
		return {}

	def defaultChildren(self):
		items = []
		return items

	def defaultEvents(self):
		self.active_events = []
		self.queue_events = []

	def findObjects(self, obj, ground=None):
		dict = self.objects
		group = []
		list = []

		for child in obj.childrenRecursive:
			if ground == True:
				child["GROUND"] = True
				child["CAMERA"] = True
			elif ground == None:
				self.checkGhost(child)

			split = child.name.split(".")
			name = None

			if len(split) > 1:
				name = split[1]

			if name != None and name in list:
				if name not in group:
					group.append(name)

			if len(split) > 2:
				dict[split[1]] = {}
				if split[1] not in group:
					group.append(split[1])

			if name != None:
				list.append(name)

		for child in obj.childrenRecursive:
			split = child.name.split(".")
			if len(split) > 1:
				if split[1] in group:
					if len(split) == 2 or split[2] == "":
						dict[split[1]][""] = child
					else:
						dict[split[1]][split[2]] = child
				else:
					dict[split[1]] = child

	def createVector(self, size=3, fill=0, vec=None):
		if vec == None:
			vec = [fill]*int(size)
		return mathutils.Vector(vec)

	def createMatrix(self, rot=None, deg=True, mirror="", mat=None, scale=None):
		if scale != None:
			return mathutils.Matrix.Scale(0.0, 3, mathutils.Vector(scale))
		if mat != None:
			return mathutils.Matrix(mat)
		if rot != None:
			if deg == True:
				rot = [math.radians(rot[0]), math.radians(rot[1]), math.radians(rot[2])]
			euler = mathutils.Euler(rot)
			return euler.to_matrix()

		mat = mathutils.Matrix.Identity(3)

		if "X" in mirror:
			mat[0][0] = -1
		if "Y" in mirror:
			mat[1][1] = -1
		if "Z" in mirror:
			mat[2][2] = -1

		return mat

	def toDeg(self, rad):
		return math.degrees(rad)

	def getSlopeOffset(self, deg, dist=1):
		return math.tan(math.radians(deg))*dist

	def vecTuple(self, POS):
		NEWPOS = [round(POS[0],4), round(POS[1],4), round(POS[2],4)]
		return NEWPOS

	def matTuple(self, ORI):
		NEWORI = [list(ORI[0]), list(ORI[1]), list(ORI[2])]
		for a in [0,1,2]:
			for b in [0,1,2]:
				NEWORI[a][b] = round(NEWORI[a][b],4)
		return NEWORI

	def addCollisionCallBack(self, obj=None):
		if obj == None:
			obj = self.getOwner()

		if obj.getPhysicsId() == 0:
			return

		if self.COLCB not in obj.collisionCallbacks:
			self.clearCollisionList()
			obj.collisionCallbacks.append(self.COLCB)

			if self.clearCollisionList not in self.active_post:
				self.active_post.append(self.clearCollisionList)

	def removeCollisionCallBack(self, obj=None):
		if obj == None:
			obj = self.getOwner()

		if self.clearCollisionList in self.active_post:
			self.active_post.remove(self.clearCollisionList)

		if obj.getPhysicsId() == 0:
			return

		if self.COLCB in obj.collisionCallbacks:
			self.clearCollisionList()
			obj.collisionCallbacks = [] #.remove(self.COLCB)

	def clearCollisionList(self):
		self.collisionList = []

	def COLCB(self, OBJ):
		if "COLLIDE" in OBJ:
			if self not in OBJ["COLLIDE"]:
				OBJ["COLLIDE"].append(self)

		if OBJ not in self.collisionList:
			self.collisionList.append(OBJ)

	def checkGhost(self, obj):
		if obj.getPhysicsId() != 0:
			for prop in ["GROUND", "CAMERA"]:
				if self.GHOST not in [prop, True]:
					obj[prop] = True
				elif prop in obj:
					del obj[prop]

	def quePhysicsRestore(self):
		if self.phys_paused == True:
			return
		if self.PR_PhysManRestore not in self.active_pre:
			self.active_pre.insert(0, self.PR_PhysManRestore)
			if self.phys_paused != "INIT":
				self.phys_paused = "QUE"

	def setPhysicsGrid(self):
		self.gravity = SC_SCN.gravity.copy()

		drag = self.gravity.length/9.8
		self.air_drag = drag
		self.air_linv = self.createVector()
		self.air_angv = self.createVector()
		self.old_air = [self.createVector(), self.createVector()]

		self.phys_paused = "INIT"

		self.data["LINVEL"] = [0,0,0]
		self.data["ANGVEL"] = [0,0,0]

	def setPhysicsType(self, mode=None):
		owner = self.getOwner()
		state = mode

		if state == None:
			state = "NONE"
			if self.PHYSICS in {"DYNAMIC", "RIGID"}:
				state = self.PHYSICS
			state = self.data.get("PHYSICS", state)
			self.data["PHYSICS"] = state
			self.phys_paused = "INIT"

		if owner.getPhysicsId() == 0:
			self.data["PHYSICS"] = ""
			return

		if mode in {"PAUSE", "FREEZE"}:
			state = "NONE"
			if self.phys_paused == False:
				self.data["LINVEL"] = self.vecTuple(owner.localLinearVelocity)
				self.data["ANGVEL"] = self.vecTuple(owner.localAngularVelocity)
			owner.localLinearVelocity = [0,0,0]
			owner.localAngularVelocity = [0,0,0]
		elif mode == "RESUME":
			if self.phys_paused != "RES":
				self.phys_paused = "INIT"
				return
			state = self.data.get("PHYSICS", self.PHYSICS)
		else:
			self.data["PHYSICS"] = state
			if self.phys_paused == "INIT":
				return

		if state == "NONE":
			owner.disableRigidBody()
			owner.suspendDynamics()
		elif state == "DYNAMIC":
			owner.restoreDynamics()
			owner.disableRigidBody()
		elif state == "RIGID":
			owner.restoreDynamics()
			owner.enableRigidBody()

		if mode == "PAUSE":
			self.quePhysicsRestore()
		elif mode == "FREEZE":
			self.phys_paused = True
		elif mode == "RESUME":
			if state in ["DYNAMIC", "RIGID"]:
				owner.localLinearVelocity = self.data["LINVEL"]
				owner.localAngularVelocity = self.data["ANGVEL"]
			self.data["LINVEL"] = [0,0,0]
			self.data["ANGVEL"] = [0,0,0]

			self.phys_paused = False

	def loadContainer(self):
		self.container_parent = None
		self.container_children = []
		self.container_orphans = []
		#self.container_seagulls = []
		self.container_slots = {"Root":[]}
		self.container_objects = self.getSlotObjects()
		self.lod_state = ""
		self.invalid = False

		cls = self.owner.get("PARENT", None)
		if cls != None:
			if self.CONTAINER == "WORLD":
				cls = None
			del self.owner["PARENT"]

		slot = self.dict.get("Parent", None)
		if slot != None and cls != None:
			self.setContainerParent(cls, slot)
		else:
			self.removeContainerParent()

	def setContainerParent(self, cls, slot=None):
		if self.CONTAINER == "WORLD":
			#if cls not in self.container_seagulls:
			#	self.container_seagulls.append(cls)
			#print("PARENTING FAILED: child cant be 'WORLD' type")
			return
		if cls == self:
			print("ERROR: Cant parent to self")
			return
		if self.container_parent != None:
			print("ALREADY PARENTED:", self.container_parent)
			self.removeContainerParent()
		if slot == None:
			slot = True

		self.container_parent = cls
		self.dict["Parent"] = slot

		if self in logic.UPDATELIST:
			logic.UPDATELIST.remove(self)

		global LEVEL
		if self.dict in LEVEL["DROP"]:
			LEVEL["DROP"].remove(self.dict)

		if self.dict not in cls.dict["Children"]:
			cls.dict["Children"].append(self.dict)
		if self not in cls.container_children:
			cls.container_children.append(self)

		cls.applyContainerProps(self)
		if slot != True:
			if slot not in cls.container_slots:
				cls.container_slots[slot] = [self]
			elif self not in cls.container_slots[slot]:
				cls.container_slots[slot].append(self)
			self.setSlotParent(cls, slot)

	def removeContainerParent(self, orphan=True):
		slot = self.dict.get("Parent", None)
		if slot not in [None, True]:
			self.owner.removeParent()

		if self.container_parent != None:
			cls = self.container_parent
			cls.applyContainerProps(self)

			if self.dict in cls.dict["Children"]:
				cls.dict["Children"].remove(self.dict)
			if self in cls.container_children:
				cls.container_children.remove(self)

			if orphan == True and self not in cls.container_orphans:
				cls.container_orphans.append(self)

			if slot in cls.container_slots:
				if self in cls.container_slots[slot]:
					cls.container_slots[slot].remove(self)
				#if len(cls.container_slots[slot]) == 0:
				#	del cls.container_slots[slot]

		if self not in logic.UPDATELIST:
			logic.UPDATELIST.append(self)
		if orphan == True and self not in logic.ORPHANUPDATE:
			logic.ORPHANUPDATE.append(self)

		global LEVEL
		if self.UPDATE == True:
			if self.dict not in LEVEL["DROP"]:
				LEVEL["DROP"].append(self.dict)

		self.container_parent = None
		self.dict["Parent"] = None

	def getSlotObjects(self):
		l = {}
		for child in self.owner.childrenRecursive:
			slot = child.get("SLOT", None)
			if slot != None:
				l[slot] = child
		return l

	def getSlotChild(self, slot, id=0):
		obj = None
		inv = self.container_slots.get(slot, [])
		x = len(inv)
		if id == -1 and x >= 1:
			obj = inv[-1]
		elif x >= (id+1):
			obj = inv[id]
		return obj

	def setSlotParent(self, cls, slot):
		obj = cls.container_objects.get(slot, cls.owner)
		if obj.invalid == True:
			obj = cls.owner

		if self.owner.parent != None:
			print("SLOT ERROR: Already Parented", self.owner, self.owner.parent)
			return

		self.owner.setParent(obj, False, False)
		self.owner.localPosition = self.createVector()
		self.owner.localOrientation = self.createMatrix()

	def loadWorldPos(self):
		obj = self.getOwner()
		rel = self.getParent()

		self.data["POS_LEVEL"] = self.data.get("POS_LEVEL", None)
		if self.data["POS_LEVEL"] == "CONTAINER" and rel == None:
			self.data["POS_LEVEL"] = None

		global CURRENT
		newmap = str(CURRENT["Level"])+str(CURRENT["Scene"])

		if self.data["POS_LEVEL"] not in [newmap, "CONTAINER"] or self.dict.get("Portal", None) == True:
			self.saveWorldPos()

		if rel != None:
			if self.dict["Parent"] not in [None, True] and obj.parent != None:
				obj.localPosition = self.data["POS"]
				obj.localOrientation = self.data["ORI"]
				return
			else:
				self.data["POS"] = self.vecTuple(self.getWorldSpace(rel.owner, self.data["POS"])+ORIGIN_OFFSET)
				self.data["ORI"] = self.matTuple(self.getWorldRotation(rel.owner, self.data["ORI"]))

		obj.worldPosition = mathutils.Vector(self.data["POS"])-ORIGIN_OFFSET
		obj.worldOrientation = self.data["ORI"]

	def saveWorldPos(self):
		obj = self.getOwner()
		rel = self.getParent()

		if rel == None:
			self.data["POS"] = self.vecTuple(obj.worldPosition+ORIGIN_OFFSET)
			self.data["ORI"] = self.matTuple(obj.worldOrientation)
			self.data["POS_LEVEL"] = str(CURRENT["Level"])+str(CURRENT["Scene"])
		elif self.dict["Parent"] not in [None, True] and obj.parent != None:
			self.data["POS"] = self.vecTuple(obj.localPosition)
			self.data["ORI"] = self.matTuple(obj.localOrientation)
			self.data["POS_LEVEL"] = "CONTAINER"
		else:
			pos, ori = self.getTransformDiff(rel.owner, cmp=True)
			self.data["POS"] = pos
			self.data["ORI"] = ori
			self.data["POS_LEVEL"] = "CONTAINER"

	def doLoad(self):
		if self.dict.get("Data", None) == None:
			self.dict["Data"] = self.data
			self.data["ACTIVE_STATE"] = self.active_state.__name__
			self.data["LOD_STATE"] = self.getLodLevel()
		else:
			self.data = self.dict["Data"]
			self.active_state = getattr(self, self.data["ACTIVE_STATE"], self.active_state)

		self.loadContainer()
		self.loadWorldPos()

		self.setLodState(self.data["LOD_STATE"])

	def doUpdate(self):
		self.data["ACTIVE_STATE"] = self.active_state.__name__

		self.saveWorldPos()

		obj = self.getOwner()
		if obj.getPhysicsId() != 0 and obj.isSuspendDynamics == False:
			self.data["LINVEL"] = self.vecTuple(obj.localLinearVelocity)
			self.data["ANGVEL"] = self.vecTuple(obj.localAngularVelocity)

		self.updateChildren()

	def loadChildren(self):
		spawn = []
		clean = []
		for child in self.owner.childrenRecursive:
			split = child.name.split(".")
			name = None
			if len(split) >= 3:
				if split[1] == "Spawn":
					name = child.get("OBJECT", split[2])
					child["OBJECT"] = name
					clean.append(child)

		if "Children" not in self.dict:
			self.dict["Children"] = []
			self.dict["PlayerChildren"] = []
			spawn = self.defaultChildren()
			for child in clean:
				name = child.get("OBJECT", None)
				dict = {"Object":name, "Data":None, "Parent":True}
				self.spawnChild(dict, child)

		else:
			for dict in self.dict["Children"]:
				check = False
				for cls in self.container_children:
					if dict == cls.dict:
						check = True
				if check == False:
					spawn.append(dict)

		for name in self.dict["PlayerChildren"].copy():
			dict = PROFILE["PLRData"][name]

			self.dict["PlayerChildren"].remove(name)

			if dict.get("Portal", None) == self.dict["Object"]:
				pid = None
				for i in WORLD["PLAYERS"]:
					if WORLD["PLAYERS"][i] == name:
						pid = i
				if name not in PLAYER_CLASSES and pid != None:
					newplr = SC_SCN.addObject(name, self.owner, 0)
					#if dict.get("Parent", None) == None:
					#	dict["Parent"] = True
					newplr["PARENT"] = self
					newplr["DICT"] = dict
					newplr["PID"] = pid
					GETCLASS(newplr)
					print("CHILDPLAYER:", name)

		for dict in spawn:
			self.spawnChild(dict)

		for obj in clean:
			obj.endObject()

	def updateChildren(self):
		for cls in self.getChildren():
			cls.dict["Portal"] = self.dict["Object"]
			cls.doUpdate()

	def spawnChild(self, dict, obj=None):
		props = {}
		name = dict["Object"]

		if name == None or name not in SC_SCN.objectsInactive:
			return None

		if obj == None:
			obj = self.owner
		else:
			for prop in obj.getPropertyNames():
				if prop not in ["SPAWN", "OBJECT", "GROUND", "CAMERA"]:
					props[prop] = obj[prop]

		newobj = SC_SCN.addObject(name, obj, 0)
		for prop in props:
			newobj[prop] = props[prop]
		#if dict.get("Parent", None) == None:
		#	dict["Parent"] = True
		newobj["PARENT"] = self
		newobj["DICT"] = dict

		print("SPAWNCHILD:", name)
		GETCLASS(newobj)

		return newobj

	def packObject(self, update=True, portal=None):
		if portal == None:
			portal = self.dict["Object"]

		if update == True:
			self.dict["Portal"] = portal
			self.doUpdate()

		for cls in self.getChildren():
			cls.packObject(False)

		if self.container_parent != None:
			cls = self.container_parent
			slot = self.dict.get("Parent", None)
			if self in cls.container_children:
				cls.container_children.remove(self)
			if slot not in  [None, True] and slot in cls.container_slots:
				if self in cls.container_slots[slot]:
					cls.container_slots[slot].remove(self)
			self.container_parent = None

		self.destroy()

		return self.dict

	def endObject(self, children=True):
		if children == True:
			for cls in self.getChildren():
				cls.endObject(children)
		else:
			for cls in self.getChildren():
				cls.removeContainerParent(orphan=False)

		self.destroy()

	def destroy(self):
		if self.container_parent != None:
			self.removeContainerParent(orphan=False)

		if self in logic.UPDATELIST:
			logic.UPDATELIST.remove(self)
		logic.OLDUPDATELIST.append(self)

		global LEVEL
		if self.dict in LEVEL["DROP"]:
			LEVEL["DROP"].remove(self.dict)

		from . import viewport, HUD
		if viewport.getController() == self:
			viewport.setCamera(None)
			SC_SCN.active_camera = SC_CAM
		if HUD.GetController() == self:
			HUD.SetLayout(None, None)

		self.invalid = True
		self.getOwner().endObject()

	def getOwner(self):
		if self.owner.invalid == True:
			print("ERROR: Owner Invalid -", self.NAME)
		return self.owner

	def getOriginBounds(self):
		return [self.getOwner().worldPosition]

	def getLocalSpace(self, obj, pnt):
		if type(pnt) != mathutils.Vector:
			pnt = mathutils.Vector(pnt)
		lp = pnt - obj.worldPosition
		lp = obj.worldOrientation.inverted()*lp
		return lp

	def getWorldSpace(self, obj, pnt):
		if type(pnt) != mathutils.Vector:
			pnt = mathutils.Vector(pnt)
		wp = obj.worldOrientation*pnt
		wp = wp + obj.worldPosition
		return wp

	def getLocalRotation(self, obj, ori):
		if type(ori) != mathutils.Matrix:
			ori = mathutils.Matrix(ori)

		lr = obj.worldOrientation.inverted()*ori

		return lr

	def getWorldRotation(self, obj, ori):
		if type(ori) != mathutils.Matrix:
			ori = mathutils.Matrix(ori)

		wr = obj.worldOrientation*ori

		return wr

	def getTransformDiff(self, obj, cmp=False):
		root = self.getOwner()

		pnt = root.worldPosition-obj.worldPosition
		lp = obj.worldOrientation.inverted()*pnt

		dr = obj.worldOrientation
		pr = root.worldOrientation
		lr = dr.inverted()*pr

		if cmp == True:
			return self.vecTuple(lp), self.matTuple(lr)

		return lp, lr

	def getParent(self):
		return self.container_parent

	def getChildren(self, orphans=False):
		return self.container_children.copy()

	def applyContainerProps(self, cls):
		cls.gravity = self.gravity.copy()
		cls.air_drag = cls.gravity.length/9.8

	def getLodLevel(self):
		return "ACTIVE"

	def setLodState(self, lod):
		if lod == self.lod_state:
			return

		self.lod_state = lod
		self.data["LOD_STATE"] = lod

		self.loadChildren()

	def sendEvent(self, key, cls=None, *args, **kwargs):
		if cls == None:
			cls = self
		evt = CoreEvent(key, self, args, kwargs)
		if cls == self:
			cls.queue_events.append(evt)
		else:
			cls.active_events.append(evt)
		return cls

	def getLastEvent(self, key, *args):
		last = None
		for evt in self.active_events:
			if evt.getEvent() == key and evt.hasAllTags(args) == True:
				last = evt
		return last

	def getFirstEvent(self, key, *args):
		for evt in self.active_events:
			if evt.getEvent() == key and evt.hasAllTags(args) == True:
				return evt

	def getAllEvents(self, key, *args):
		all = []
		for evt in self.active_events:
			if evt.getEvent() == key and evt.hasAllTags(args) == True:
				all.append(evt)
		return all

	def teleportTo(self, pos=None, ori=None, vel=False, cam=True):
		owner = self.getOwner()

		if cam == True and logic.VIEWPORT != None:
			cam = False
			if logic.VIEWPORT.control == self:
				cam = True

		if pos != None:
			diff = pos-owner.worldPosition
			owner.worldPosition += diff
			if cam == True:
				vertref = logic.VIEWPORT.owner
				vertref.worldPosition += diff

		if ori != None:
			qs = owner.worldOrientation.to_quaternion()
			qt = ori.to_quaternion()
			diff = qs.rotation_difference(qt)
			owner.worldOrientation.rotate(diff)
			if cam == True:
				vertref = logic.VIEWPORT.owner
				vertref.worldOrientation.rotate(diff)

		#if vel == False:
		#	owner.worldLinearVelocity = (0,0,0)

	def hideObject(self, state=True):
		state = (state==False)
		self.getOwner().setVisible(state, True)

	def doAnim(self, OBJECT=None, NAME="NONE", FRAME=(0, 60), LAYER=0, PRIORITY=0, MODE="PLAY", BLEND=0, KEY=False, STOP=False, CHECK=False, SET=None):

		if type(OBJECT) is str:
			if OBJECT not in self.objects:
				OBJECT = None
			else:
				OBJECT = self.objects[OBJECT]

		if OBJECT == None:
			if self.ANIMOBJ != None:
				OBJECT = self.ANIMOBJ
			elif self.objects.get("Root", None) != None:
				OBJECT = self.objects["Root"]
			else:
				return

		if SET != None:
			OBJECT.setActionFrame(SET, LAYER)
			return

		if CHECK == "FRAME":
			ACTCHK = OBJECT.getActionFrame(LAYER)
			return ACTCHK
		if CHECK == True:
			ACTCHK = OBJECT.isPlayingAction(LAYER)
			return ACTCHK

		if STOP == True:
			OBJECT.stopAction(LAYER)
			return

		if NAME == "NONE":
			NAME = OBJECT.name
		if NAME == None:
			return

		SPEED = 1.0
		LYRWGHT = 0.0
		IPO_FLG = 0

		if MODE == "PLAY":
			PLAYTYPE = logic.KX_ACTION_MODE_PLAY

		elif MODE == "LOOP":
			PLAYTYPE = logic.KX_ACTION_MODE_LOOP

		BLENDTYPE = logic.KX_ACTION_BLEND_BLEND

		OBJECT.playAction(NAME, FRAME[0], FRAME[1], LAYER, PRIORITY, BLEND, PLAYTYPE, LYRWGHT, IPO_FLG, SPEED, BLENDTYPE)
		if KEY == True:
			OBJECT.reinstancePhysicsMesh()

	def alignToGravity(self, obj=None, axis=2, neg=False):
		if obj == None:
			obj = self.getOwner()
		if self.gravity.length >= 0.1:
			grav = self.gravity.normalized()
			if neg == False:
				grav = -grav
			fac = self.gravity.length/5
			if fac > 1:
				fac = 1
			obj.alignAxisToVect(grav, axis, fac)

	def checkStability(self, align=False, offset=1.0, override=None):
		if settings.config.DO_STABILITY == False:
			return

		owner = self.getOwner()

		lowclip = False
		if SC_SCN.gravity.length >= 0.1 and (owner.worldPosition[2]+ORIGIN_OFFSET[2]) < LEVEL["CLIP"]:
			lowclip = True

		if lowclip == True or keymap.SYSTEM["UBERRESET"].tap() == True and owner.parent == None:
			owner.localLinearVelocity = (0,0,0)
			owner.localAngularVelocity = (0,0,0)
			owner.worldPosition = mathutils.Vector(self.data["POS"])-ORIGIN_OFFSET
			owner.worldOrientation = self.data["ORI"]

		if override == False or self.gravity.length < 0.1:
			return

		grav = self.gravity.normalized()
		rayto = owner.worldPosition+grav

		if keymap.SYSTEM["STABILITY"].tap() == True or override == True:
			override = True
		#else:
		#	down, pnt, nrm = owner.rayCast(rayto, None, 10000, "GROUND", 1, 1, 0)
		#	if nrm != None:
		#		if nrm.dot(grav) > 0:
		#			override = True

		if override == True:
			up, pnt, nrm = owner.rayCast(rayto, None, -10000, "GROUND", 1, 1, 0)
			if up != None:
				owner.localLinearVelocity = (0,0,0)
				owner.localAngularVelocity = (0,0,0)
				owner.worldPosition = pnt+(nrm*offset)
				if align == True:
					owner.alignAxisToVect(nrm, 2, 1.0)

	#def checkClicked(self, obj=None):
	#	if self.INTERACT == False:
	#		return False

	#	if obj == None:
	#		obj = self.owner

	#	if obj["RAYCAST"] != None:
	#		if keymap.BINDS["ACTIVATE"].tap() == True:
	#			return True

	#	return False

	def checkClicked(self, obj=None):
		if self.INTERACT == False:
			return False

		all = self.getAllEvents("INTERACT", "SEND")
		act = self.getAllEvents("INTERACT", "ACTOR", "TAP")
		sec = self.getAllEvents("INTERACT", "!ACTOR", "!SEND")

		for evt in all:
			self.sendEvent("INTERACT", evt.sender, "RECEIVE", NAME=self.NAME)

		for evt in act:
			return True

		return False

	def clearRayProps(self):
		if self.INTERACT == True and "RAYCAST" in self.owner:
			self.owner["RAYCAST"] = None

	def stateSwitch(self, state=None):
		if state == None:
			self.active_state = getattr(self, self.data["ACTIVE_STATE"], self.active_state)
		elif state == "DISABLED":
			self.active_state = self.ST_Disabled
		elif state == "ACTIVE":
			self.active_state = self.ST_Active

	## PHYSMAN ##
	def runPhysMan(self):
		owner = self.getOwner()
		if owner.getPhysicsId() != 0:
			#owner.worldLinearVelocity += self.air_linv-self.old_air[0]
			if self.phys_paused == "INIT":
				owner.localLinearVelocity = (0,0,0)
				owner.localAngularVelocity = (0,0,0)
				self.setPhysicsType("PAUSE")
			elif ORIGIN_SHIFT != None and self.phys_paused == False and self.data["PHYSICS"] in ["DYNAMIC", "RIGID"]:
				owner.worldPosition += owner.worldLinearVelocity*(1/60)
				if self.data["PHYSICS"] == "RIGID":
					angV = owner.localAngularVelocity
					owner.worldOrientation = owner.worldOrientation*self.createMatrix(rot=(angV[0]/60,angV[1]/60,angV[2]/60), deg=False)
				self.setPhysicsType("PAUSE")
			else:
				owner.applyForce((self.gravity-SC_SCN.gravity)*owner.mass, False)
				if self.data["PHYSICS"] == "NONE":
					owner.localLinearVelocity = (0,0,0)
					owner.localAngularVelocity = (0,0,0)
				elif self.data["PHYSICS"] == "DYNAMIC":
					owner.localAngularVelocity = (0,0,0)
		#else:
		#	owner.worldPosition += self.air_linv*(1/60)
		self.old_air = [self.air_linv, self.air_angv]
		self.air_linv = self.createVector()
		self.air_angv = self.createVector()

	def PR_PhysManRestore(self):
		if self.phys_paused == True:
			#print("PHYS_RESTORE_FREEZE", self.owner)
			return "REMOVE"
		if self.phys_paused == "INIT":
			self.phys_paused = "QUE"
			#print("PHYS_RESTORE_INIT", self.owner)
		elif ORIGIN_SHIFT == None:
			self.phys_paused = "RES"
			self.setPhysicsType("RESUME")
			#print("PHYS_RESTORE", self.owner)
			return "REMOVE"
		#else:
		#	print("PHYS_RESTORE_WAIT", self.owner)

	## INIT STATE ##
	def ST_Startup(self):
		pass

	## STATE DISABLED ##
	def ST_Disabled(self):
		if self.checkClicked() == True:
			self.stateSwitch("ACTIVE")

	## STATE ACTIVE ##
	def ST_Active(self):
		if self.checkClicked() == True:
			self.stateSwitch("DISABLED")

	## RUN ##
	def runPre(self):
		for run in self.active_pre:
			st = run()
			if st == "REMOVE":
				self.active_pre.remove(run)

	def runStates(self):
		if self.active_state != None:
			self.active_state()
			self.data["ACTIVE_STATE"] = self.active_state.__name__

	def runPost(self):
		for run in self.active_post:
			st = run()
			if st == "REMOVE":
				self.active_post.remove(run)

	def clearEvents(self):
		self.active_events = []
		for cls in self.queue_events:
			self.active_events.append(cls)
		self.queue_events = []

	def runChildren(self):
		if self.lod_state != "ACTIVE":
			self.container_orphans = []
			return

		for update in [self.container_children, self.container_orphans]:
			for cls in update.copy():
				if cls.invalid == True:
					cls.removeContainerParent(orphan=False)
				elif cls not in logic.OLDUPDATELIST:
					logic.OLDUPDATELIST.append(cls)
					self.applyContainerProps(cls)
					cls.RUN()

		self.container_orphans = []

	def RUN(self):
		self.runPre()
		self.runStates()
		self.runPost()
		self.runPhysMan()
		self.clearEvents()
		self.clearRayProps()
		self.runChildren()


class CoreAdvanced(CoreObject):

	WP_TYPE = "RANGED"
	WP_SOCKETS = []
	STAT_ATTR = {"HEALTH":(0,100,200), "ENERGY":(0,100,200)} #"VAL":(min, max, super)
	INVENTORY = {}
	SLOTS = {}

	CAM_TYPE = "THIRD"
	CAM_ORBIT = False
	CAM_RANGE = (4,16)
	CAM_HEIGHT = 0.1
	CAM_STEPS = 5
	CAM_ZOOM = 2
	CAM_MIN = 0.5
	CAM_SLOW = 10
	CAM_FOV = 90
	CAM_HEAD_G = 0

	CAM_OFFSET = (0,0,0)
	CAM_AUTOZOOM = False

	CAM_SHDIST = 0.3
	CAM_SHSIDE = 0.3

	CONTAINER = "ALLOW"
	PHYSICS = "NONE"
	VIEWPORT = None
	HUDLAYOUT = None

	def defaultChildren(self):
		items = []
		defaults = list(self.INVENTORY.keys())
		defaults.sort()

		for slot in defaults:
			dict = {"Object":self.INVENTORY[slot], "Data":None, "Parent":slot}
			items.append(dict)

		return items

	def doLoad(self):
		self.loadInventory()
		super().doLoad()

	def loadInventory(self):
		self.active_weapon = None
		self.attachments = []

		self.data["WPDATA"] = {"ACTIVE":"NONE", "CURRENT":"NONE", "TIMER":0, "WHEEL":{}}
		self.data["WPDATA"]["WHEEL"][self.WP_TYPE] = {"ID":-1, "LIST":[]}

		self.data["SLOTS"] = {}

		for key in self.SLOTS:
			slot = self.SLOTS[key]
			new = "SLOT_"+key
			self.data["SLOTS"][slot] = new

		for key in self.STAT_ATTR:
			self.data[key] = self.STAT_ATTR[key][1]

		self.data["MODIFIERS"] = []

	def addModifier(self, **kwargs):
		dict = {"__TIMER__":0}
		for key in kwargs.keys():
			dict[key] = kwargs[key]
		self.data["MODIFIERS"].append(dict)

	def getModifierValues(self, key):
		all = []
		for dict in self.data["MODIFIERS"]:
			if key in dict.keys():
				all.append(dict[key])
		return all

	def applyModifier(self, dict):
		for key in dict.keys():
			if key in self.STAT_ATTR:
				cur = self.data[key]
				min, max, sup = self.STAT_ATTR[key]
				val = dict[key]
				cap = max
				if dict.get("SUPER", False) == True or val <= 0:
					cap = sup
				if (cur+val) > cap:
					val = cap-cur
				if cur <= cap or val < 0:
					self.data[key] += val

	def runModifiers(self):
		all = self.getAllEvents("MODIFIERS")
		for evt in all:
			dict = evt.data.copy()
			#dict["__TIMER__"] = dict.get("TIMER", 0)
			self.data["MODIFIERS"].append(dict)

		modprops = self.getOwner().get("MODIFIERS", [])
		for dict in modprops:
			self.data["MODIFIERS"].append(dict.copy())
		self.getOwner()["MODIFIERS"] = []

		for dict in self.data["MODIFIERS"].copy():
			dict["__TIMER__"] = dict.get("__TIMER__", 0)
			if dict["__TIMER__"] >= 1:
				dict["__DURATION__"] = dict.get("__DURATION__", dict["__TIMER__"])
			if dict["__TIMER__"] < 0:
				self.data["MODIFIERS"].remove(dict)
			else:
				self.applyModifier(dict)
				dict["__TIMER__"] -= dict.get("__RATE__", 1)

	def manageStatAttr(self):
		if self.data["HEALTH"] < 0:
			self.data["HEALTH"] = -1
		if self.data["ENERGY"] < 0:
			self.data["ENERGY"] = 0

		if self.data["HEALTH"] > 200:
			self.data["HEALTH"] = 200
		if self.data["ENERGY"] > 200:
			self.data["ENERGY"] = 200

	def weaponLoop(self):
		pass

	def weaponManager(self):
		weap = self.data["WPDATA"]

		## MODE SWITCH ##
		modes = []
		for mode in weap["WHEEL"]:
			if len(weap["WHEEL"][mode]["LIST"]) >= 1:
				modes.append(mode)
			else:
				weap["WHEEL"][mode]["ID"] = -1

		if len(modes) >= 1:
			if weap["CURRENT"] == "NONE":
				if self.WP_TYPE in modes:
					weap["CURRENT"] = self.WP_TYPE
				else:
					weap["CURRENT"] = modes[0]

			elif keymap.BINDS["WP_MODE"].tap() == True and len(modes) == 2 and self.active_weapon != None:
				if weap["CURRENT"] == "MELEE":
					weap["CURRENT"] = "RANGED"
				elif weap["CURRENT"] == "RANGED":
					weap["CURRENT"] = "MELEE"

		else:
			self.active_weapon = None
			weap["CURRENT"] = "NONE"
			weap["ACTIVE"] = "NONE"
			return

		dict = weap["WHEEL"][weap["CURRENT"]]

		## WEAPON SELECT ##
		if dict["ID"] >= len(dict["LIST"]):
			dict["ID"] = len(dict["LIST"])-1

		if dict["ID"] == -1:
			if len(dict["LIST"]) >= 1:
				dict["ID"] = 0
			else:
				self.active_weapon = None
				weap["CURRENT"] = "NONE"
				weap["ACTIVE"] = "NONE"
				weap["TIMER"] = 0
				return

		elif keymap.BINDS["WP_UP"].tap() == True:
			dict["ID"] += 1
			weap["TIMER"] = 0
			if dict["ID"] == len(dict["LIST"]):
				dict["ID"] = 0

		elif keymap.BINDS["WP_DOWN"].tap() == True:
			dict["ID"] -= 1
			weap["TIMER"] = 0
			if dict["ID"] == -1:
				dict["ID"] = len(dict["LIST"])-1

		slot = dict["LIST"][dict["ID"]]
		cls = self.getSlotChild(slot)
		if cls == None:
			weap["ACTIVE"] = "NONE"
			weap["TIMER"] = 0

		## STATE MANAGER ##
		if weap["ACTIVE"] == "ACTIVE":
			if self.active_weapon == None:
				weap["TIMER"] = 0
				weap["ACTIVE"] = "NONE"

			elif self.active_weapon != cls:
				if weap["TIMER"] == 10:
					weap["TIMER"] = 0
					weap["ACTIVE"] = "SWITCH"
				else:
					weap["TIMER"] += 1

			## SET PASSIVE ##
			elif keymap.BINDS["SHEATH"].tap() == True:
				weap["TIMER"] = 0
				weap["ACTIVE"] = "DISABLE"

			## RUN WEAPON ##
			else:
				self.weaponLoop()

		elif weap["ACTIVE"] == "DISABLE":
			if self.active_weapon == None:
				weap["TIMER"] = 0
				weap["ACTIVE"] = "NONE"

			else:
				check = self.active_weapon.stateSwitch(False)
				if check == True:
					self.active_weapon = None


		elif weap["ACTIVE"] == "SWITCH":
			if self.active_weapon == None:
				weap["TIMER"] = 0
				weap["ACTIVE"] = "NONE"

			elif self.active_weapon != cls:
				check = self.active_weapon.stateSwitch(False)
				if check == True:
					self.active_weapon = cls

			else:
				check = self.active_weapon.stateSwitch(True)
				if check == True:
					weap["TIMER"] = 0
					weap["ACTIVE"] = "ACTIVE"

		elif weap["ACTIVE"] == "NONE":
			if self.active_weapon != None:
				weap["TIMER"] = 0
				weap["ACTIVE"] = "SWITCH"

			## SET ACTIVE ##
			elif keymap.BINDS["SHEATH"].tap() == True:
				self.active_weapon = cls
				weap["TIMER"] = 0
				weap["ACTIVE"] = "SWITCH"

	def RUN(self):
		self.runModifiers()
		self.manageStatAttr()
		self.runPre()
		self.runStates()
		self.runPost()
		self.runPhysMan()
		self.clearEvents()
		self.clearRayProps()
		self.runChildren()

