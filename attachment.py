####
# bge_game3_core: Full python game structure for the Blender Game Engine
# Copyright (C) 2019  DaedalusMDW @github.com (Daedalus_MDW @blenderartists.org)
# https://github.com/DaedalusMDW/bge_game3_core
#
# This file is part of bge_game3_core.
#
#    bge_game3_core is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    bge_game3_core is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with bge_game3_core.  If not, see <http://www.gnu.org/licenses/>.
#
####

## INVENTORY ITEMS ##


from bge import logic

from . import base, keymap


class CoreAttachment(base.CoreObject):

	NAME = "Attachment"
	GHOST = True
	CONTAINER = "ALLOW"
	ENABLE = False
	SLOTS = []
	SCALE = 1
	OFFSET = (0,0,0)
	COLOR = (1,1,1,1)
	COLLIDE = False

	BOX_MESH = "BOX_Cube"
	BOX_NORMALIZE = True
	BOX_ALIGN = False
	BOX_GLOW = False

	def __init__(self, *args):
		if len(args) == 0:
			return
		owner = args[0]

		owner["Class"] = self
		owner["DICT"] = owner.get("DICT", {"Object":owner.name, "Data":None})

		self.ANIMOBJ = None

		self.owner = owner
		self.objects = {"Root":owner}
		self.box = None
		self.halo = None
		self.campers = []

		self.owning_slot = None
		self.owning_player = None

		self.defaultEvents()
		self.defaultStates()

		self.dict = owner["DICT"]
		self.data = self.defaultData()

		self.data["HUD"] = {"Color":(1,1,1,1), "Stat":100, "Text":""}
		self.data["ENABLE"] = None
		self.data["COOLDOWN"] = 0

		self.dict["Equiped"] = self.dict.get("Equiped", None)

		self.setPhysicsGrid()

		self.checkGhost(owner)
		self.findObjects(owner)
		self.doLoad()
		self.ST_Startup()

		if self.dict["Parent"] in [None, True]:
			self.buildBox()
		else:
			if self.active_state == self.ST_Box:
				self.active_state = self.ST_Idle
			self.owning_slot = self.dict["Parent"]
			self.owning_player = self.container_parent
			self.dict["Equiped"] = self.owning_slot
			self.assignToPlayer()
			if self.data["ENABLE"] == None:
				if self.ENABLE == True:
					self.stateSwitch(True, force=True)
				else:
					self.data["ENABLE"] = False

	def defaultStates(self):
		self.active_pre = []
		self.active_state = self.ST_Box
		self.active_post = []

	def defaultData(self):
		dict = super().defaultData()

		dict["BOX_MESH"] = self.owner.get("BOX", self.BOX_MESH)
		dict["BOX_SCALE"] = self.owner.get("SCALE", self.SCALE)
		dict["BOX_NORM"] = self.owner.get("NORMALIZE", self.BOX_NORMALIZE)
		dict["BOX_ALIGN"] = self.owner.get("ALIGN", self.BOX_ALIGN)
		dict["BOX_HALO"] = self.owner.get("GLOW", self.BOX_GLOW)

		return dict

	def getOwner(self):
		obj = self.box
		if self.box == None:
			obj = self.owner
		return obj

	def hideObject(self, state=True):
		state = (state==False)
		self.owner.setVisible(state, True)
		#if self.halo != None:
		#	self.halo.setVisible(state, True)

	def buildBox(self, pos=None):
		owner = self.owner
		scene = owner.scene

		gfx_msh = self.data["BOX_MESH"]
		gfx_scl = self.data["BOX_SCALE"]
		gfx_nrm = self.data["BOX_NORM"]
		gfx_zup = self.data["BOX_ALIGN"]
		gfx_glo = self.data["BOX_HALO"]
		gfx_col = self.COLOR

		if self.dict["Equiped"] != None:
			self.dict["Equiped"] = False
			gfx_msh = "BOX_Drop"
			gfx_scl = self.SCALE
			gfx_nrm = True
			gfx_zup = True
			gfx_glo = True
			gfx_col = (1,1,1,1)

		scale = gfx_scl
		if gfx_nrm == True:
			scale = 1.0

		box = scene.addObject(gfx_msh, owner, 0)
		box.worldScale = self.createVector(fill=scale)
		box.color = gfx_col

		if pos != None:
			box.worldPosition = pos

		if gfx_zup == True:
			self.alignToGravity(box)

		self.setPhysicsType()
		self.box = box

		owner.setParent(box, False, False)
		owner.localPosition = self.createVector(vec=self.OFFSET)*(1/scale)
		owner.localOrientation = self.createMatrix()
		owner.localScale = self.createVector(fill=1.0)*(1/gfx_scl)

		if gfx_glo == True:
			halo = scene.addObject("GFX_Halo", box, 0)
			halo.setParent(box)
			halo.localPosition = self.createVector()
			halo.localOrientation = self.createMatrix()
			halo.localScale = (1,1,1)
			halo.color = gfx_col
			halo["LOCAL"] = True
			halo["AXIS"] = None
			self.halo = halo

		box["RAYCAST"] = None
		box["RAYNAME"] = self.NAME

		box["COLLIDE"] = []

		self.addCollisionCallBack(box)

		return box

	def destroyBox(self):
		owner = self.owner

		if self.box != None:
			self.removeCollisionCallBack(self.box)
			if owner.parent == self.box:
				owner.removeParent()
			self.box.endObject()
		else:
			self.removeCollisionCallBack()

		owner.worldScale = (1,1,1)

		self.box = None
		self.halo = None

	def assignToPlayer(self):
		slot = self.owning_slot
		cls = self.owning_player

		if cls!= None:
			if self not in cls.attachments:
				cls.attachments.append(self)

	def removeFromPlayer(self):
		slot = self.owning_slot
		cls = self.owning_player

		if cls != None:
			if self in cls.attachments:
				cls.attachments.remove(self)

	def equipItem(self, cls, slot=None):
		owner = self.owner

		for key in self.SLOTS:
			obj = cls.getSlotChild(key)
			if obj == None and slot in [None, False, "DROP"] and key in cls.container_objects:
				slot = key
				break

		if slot in [None, False]:
			return

		self.active_state = self.ST_Idle

		self.owning_slot = slot
		self.owning_player = cls

		self.destroyBox()
		self.setContainerParent(cls, slot)
		self.assignToPlayer()

		self.dict["Equiped"] = slot

		if self.ENABLE == True:
			self.stateSwitch(True, force=True)
		else:
			self.data["ENABLE"] = False

	def dropItem(self, pos=None):
		owner = self.owner

		self.dict["Equiped"] = "DROP"
		self.ST_Stop()

		self.removeFromPlayer()
		self.removeContainerParent()
		self.hideObject(False)

		self.buildBox(pos)

		self.active_state = self.ST_Box
		self.owning_slot = None
		self.owning_player = None

	def checkClicked(self, obj=None):
		if self.box == None:
			return False

		if self.box["RAYCAST"] != None:
			if self.COLLIDE == True:
				if self.box["RAYCAST"] in self.box["COLLIDE"]:
					return True

			elif keymap.BINDS["ACTIVATE"].tap() == True:
				return True
		return False

	def checkStability(self, align=False, offset=None):
		if self.gravity.length < 0.1:
			return

		box = self.getOwner()

		if offset == None:
			offset = box.worldScale[2]

		rayto = box.worldPosition.copy()
		if self.gravity.length >= 0.1:
			rayto += self.gravity.normalized()
		else:
			rayto += box.getAxisVect((0,0,-1))

		obj, pnt, nrm = box.rayCast(rayto, None, 10000, "GROUND", 1, 1, 0)

		if obj != None:
			box.worldPosition = pnt+box.getAxisVect((0,0,offset*0.5))
		else:
			obj, pnt, nrm = box.rayCast(rayto, None, -10000, "GROUND", 1, 1, 0)
			if obj != None:
				box.worldPosition = pnt+box.getAxisVect((0,0,offset*0.5))

	def stateSwitch(self, state=None, force=False):
		if state == None:
			if self.data["ENABLE"] == True:
				state = False
			else:
				state = True
		elif state != True:
			state = False

		if force == False:
			if self.data["COOLDOWN"] != 0:
				return False
			if self.data["ENABLE"] == state:
				return True

		if self.box != None:
			return False

		if state == True:
			self.active_state = self.ST_Enable
		else:
			self.active_state = self.ST_Stop

		return False

	## STATE BOX ##
	def ST_Box(self):
		if self.checkClicked(self.box) == True:
			self.equipItem(self.box["RAYCAST"])

	## STATE TRIGGER ##
	def ST_Enable(self):
		self.data["ENABLE"] = True
		self.active_state = self.ST_Active

	def ST_Stop(self):
		self.data["ENABLE"] = False
		self.active_state = self.ST_Idle

	## STATES ##
	def ST_Idle(self):
		pass

	def ST_Active(self):
		pass

	def clearRayProps(self):
		if self.box != None:
			self.box["RAYCAST"] = None
			self.box["COLLIDE"] = []



