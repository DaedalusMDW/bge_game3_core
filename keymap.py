
from bge import logic, events

from . import input, config


## LOAD KEYBINDS ##

MOUSELOOK = input.MouseLook(25, SMOOTH=5)

BINDS = {

"A": "Global",
"ACTIVATE":          input.KeyBase("000.A",  "FKEY",           "Activate",            JOYBUTTON=0),
"ALTACT":            input.KeyBase("001.A",  "EKEY",           "AltAct",              JOYBUTTON=2),
"ENTERVEH":          input.KeyBase("002.A",  "ENTERKEY",       "Enter/Exit Vehicle",  JOYBUTTON=3),
"TOGGLEMODE":        input.KeyBase("003.A",  "QKEY",           "Mode Switch",         JOYBUTTON=9),
"TOGGLECAM":         input.KeyBase("004.A",  "VKEY",           "Camera Switch",       JOYBUTTON=4),
"CAM_ORBIT":         input.KeyBase("005.A",  "ZKEY",           "Rotate Camera",       JOYBUTTON=8),
"TOGGLEHUD":         input.KeyBase("006.A",  "HKEY",           "HUD Action Key"),
"TOGGLESTRAFE":      input.KeyBase("007.A",  "LKEY",           "Toggle Strafe",       JOYBUTTON=8),
"ZOOM_IN":           input.KeyBase("008.A",  "WHEELUPMOUSE",   "Camera In",           JOYBUTTON=11, SHIFT=True),
"ZOOM_OUT":          input.KeyBase("009.A",  "WHEELDOWNMOUSE", "Camera Out",          JOYBUTTON=12, SHIFT=True),

"P": "Player Movement",
"PLR_FORWARD":       input.KeyBase("100.P",  "WKEY",           "Move Forward",  JOYAXIS=(1, "NEG", "A")),
"PLR_BACKWARD":      input.KeyBase("101.P",  "SKEY",           "Move Backward", JOYAXIS=(1, "POS", "A")),
"PLR_STRAFELEFT":    input.KeyBase("102.P",  "AKEY",           "Strafe Left",   JOYAXIS=(0, "NEG", "A")),
"PLR_STRAFERIGHT":   input.KeyBase("103.P",  "DKEY",           "Strafe Right",  JOYAXIS=(0, "POS", "A")),
"PLR_LOOKUP":        input.KeyBase("104.P",  "UPARROWKEY",     "Look Up",       JOYAXIS=(3, "NEG", "A")),
"PLR_LOOKDOWN":      input.KeyBase("105.P",  "DOWNARROWKEY",   "Look Down",     JOYAXIS=(3, "POS", "A")),
"PLR_TURNLEFT":      input.KeyBase("106.P",  "LEFTARROWKEY",   "Turn Left",     JOYAXIS=(2, "NEG", "A")),
"PLR_TURNRIGHT":     input.KeyBase("107.P",  "RIGHTARROWKEY",  "Turn Right",    JOYAXIS=(2, "POS", "A")),
"PLR_JUMP":          input.KeyBase("108.P",  "SPACEKEY",       "Jump",          JOYAXIS=(5, "SLIDER", "B")),
"PLR_DUCK":          input.KeyBase("109.P",  "CKEY",           "Duck",          JOYAXIS=(4, "SLIDER", "B")),
"PLR_RUN":           input.KeyBase("110.P",  "LEFTCTRLKEY",    "Toggle Run",    JOYBUTTON=7),
"PLR_EDIT":          input.KeyBase("111.P",  "BACKSLASHKEY",   "Toggle Edit"),

"V": "Vehicle Movement",
"VEH_THROTTLEUP":    input.KeyBase("200.V",  "WKEY",           "Vehicle Throttle Up",   JOYAXIS=(5, "SLIDER", "A")),
"VEH_THROTTLEDOWN":  input.KeyBase("201.V",  "SKEY",           "Vehicle Throttle Down", JOYAXIS=(4, "SLIDER", "A")),
"VEH_YAWLEFT":       input.KeyBase("202.V",  "AKEY",           "Vehicle Yaw Left",      JOYAXIS=(0, "NEG", "A"), SHIFT=False),
"VEH_YAWRIGHT":      input.KeyBase("203.V",  "DKEY",           "Vehicle Yaw Right",     JOYAXIS=(0, "POS", "A"), SHIFT=False),
"VEH_PITCHUP":       input.KeyBase("204.V",  "DOWNARROWKEY",   "Vehicle Pitch Up",      JOYAXIS=(3, "POS", "A")),
"VEH_PITCHDOWN":     input.KeyBase("205.V",  "UPARROWKEY",     "Vehicle Pitch Down",    JOYAXIS=(3, "NEG", "A")),
"VEH_BANKLEFT":      input.KeyBase("206.V",  "LEFTARROWKEY",   "Vehicle Bank Left",     JOYAXIS=(2, "NEG", "A")),
"VEH_BANKRIGHT":     input.KeyBase("207.V",  "RIGHTARROWKEY",  "Vehicle Bank Right",    JOYAXIS=(2, "POS", "A")),
"VEH_ASCEND":        input.KeyBase("208.V",  "SPACEKEY",       "Vehicle Ascend",        JOYAXIS=(1, "NEG", "A")),
"VEH_DESCEND":       input.KeyBase("209.V",  "CKEY",           "Vehicle Descend",       JOYAXIS=(1, "POS", "A")),
"VEH_STRAFELEFT":    input.KeyBase("210.V",  "AKEY",           "Vehicle Strafe Left",   JOYBUTTON=13, SHIFT=True),
"VEH_STRAFERIGHT":   input.KeyBase("211.V",  "DKEY",           "Vehicle Strafe Right",  JOYBUTTON=14, SHIFT=True),
"VEH_ACTION":        input.KeyBase("212.V",  "LEFTCTRLKEY",    "Vehicle Action Key",    JOYBUTTON=7),
"VEH_SWAPROLL":      input.KeyBase("213.V",  "LEFTALTKEY",     "Mouse Yaw as Roll"),
"VEH_MOUSEMODE":     input.KeyBase("214.V",  "RIGHTSHIFTKEY",  "Mouse Centering"),
"VEH_CARACCEL":      input.KeyBase("215.V",  "WKEY",           "Car Accelerate",        JOYAXIS=(5, "SLIDER", "A")),
"VEH_CARBRAKE":      input.KeyBase("216.V",  "SKEY",           "Car Brake/Reverse",     JOYAXIS=(4, "SLIDER", "A")),
"VEH_HANDBRAKE":     input.KeyBase("217.V",  "SPACEKEY",       "Car Handbrake",         JOYBUTTON=0),

"W": "Weapons",
"WP_UP":             input.KeyBase("300.W",  "WHEELUPMOUSE",   "Weapon Up",        JOYBUTTON=14,  SHIFT=False),
"WP_DOWN":           input.KeyBase("301.W",  "WHEELDOWNMOUSE", "Weapon Down",      JOYBUTTON=13,  SHIFT=False),
"WP_MODE":           input.KeyBase("302.W",  "QKEY",           "Weapon Mode",      JOYBUTTON=9),
"ATTACK_ONE":        input.KeyBase("303.W",  "LEFTMOUSE",      "Primary Attack",   JOYBUTTON=10),
"ATTACK_TWO":        input.KeyBase("304.W",  "RIGHTMOUSE",     "Secondary Attack", JOYBUTTON=2),
"SHEATH":            input.KeyBase("305.W",  "XKEY",           "Sheath Weapon",    JOYBUTTON=1),

"S": "Slot Keys",
"SLOT_ZERO":         input.KeyBase("800.S",  "ZEROKEY",        "Slot 0"),
"SLOT_ONE":          input.KeyBase("801.S",  "ONEKEY",         "Slot 1"),
"SLOT_TWO":          input.KeyBase("802.S",  "TWOKEY",         "Slot 2"),
"SLOT_THREE":        input.KeyBase("803.S",  "THREEKEY",       "Slot 3"),
"SLOT_FOUR":         input.KeyBase("804.S",  "FOURKEY",        "Slot 4"),
"SLOT_FIVE":         input.KeyBase("805.S",  "FIVEKEY",        "Slot 5"),
"SLOT_SIX":          input.KeyBase("806.S",  "SIXKEY",         "Slot 6"),
"SLOT_SEVEN":        input.KeyBase("807.S",  "SEVENKEY",       "Slot 7"),
"SLOT_EIGHT":        input.KeyBase("808.S",  "EIGHTKEY",       "Slot 8"),
"SLOT_NINE":         input.KeyBase("809.S",  "NINEKEY",        "Slot 9"),

"Z": "System/Extras",
"SHUFFLE":           input.KeyBase("900.Z",  "ACCENTGRAVEKEY", "Arrange Slots",   ALT=False),
"SUPER_DROP":        input.KeyBase("901.Z",  "ACCENTGRAVEKEY", "Drop All",        ALT=True),
"KILL":              input.KeyBase("902.Z",  "TKEY",           "Terminate/Kill")
}

joy_msb = [10,12,13]
joy_scr = [2,3]
joy_bck = 11
joy_esc = 4
if config.UPBGE_FIX == True:
	joy_msb = [0,3,2]
	joy_scr = [12,11]
	joy_bck = 1
	joy_esc = 6

SYSTEM = {

"SCREENSHOT":        input.KeyBase("999.Z",  "F12KEY",         "PrintScreen"),
"STABILITY":         input.KeyBase("999.Z",  "PAGEUPKEY",      "Snap to Floor",  CTRL=True),
"UBERRESET":         input.KeyBase("999.Z",  "BACKSPACEKEY",   "Uber Reset",     CTRL=True),
"CALIBRATE":         input.KeyBase("999.Z",  "HOMEKEY",        "Center Axis",    CTRL=True),
"ESCAPE":            input.KeyBase("999.Z",  "ESCKEY",         "Escape",       JOYBUTTON=joy_esc),

"SHIFT":             input.KeyBase("999.Z",  "NONE",           "Shift",    JOYBUTTON=10, SHIFT=True),
"CTRL":              input.KeyBase("999.Z",  "NONE",           "Control",  CTRL=True),
"ALT":               input.KeyBase("999.Z",  "NONE",           "Alt",      ALT=True),

"LEFTCLICK":         input.KeyBase("999.Z",  "LEFTMOUSE",      "Primary",      JOYBUTTON=joy_msb[0]),
"MIDDLECLICK":       input.KeyBase("999.Z",  "MIDDLEMOUSE",    "Middle",       JOYBUTTON=joy_msb[1]),
"RIGHTCLICK":        input.KeyBase("999.Z",  "RIGHTMOUSE",     "Secondary",    JOYBUTTON=joy_msb[2]),
"WHEEL_UP":          input.KeyBase("999.Z",  "WHEELUPMOUSE",   "Scroll Up",    JOYBUTTON=joy_scr[1]),
"WHEEL_DOWN":        input.KeyBase("999.Z",  "WHEELDOWNMOUSE", "Scroll Down",  JOYBUTTON=joy_scr[0]),

"UI_ENTER":          input.KeyBase("999.Z",  "ENTERKEY",       "UI Select",    JOYBUTTON=joy_msb[0]),
"UI_BACK":           input.KeyBase("999.Z",  "BACKSPACEKEY",   "UI Back",      JOYBUTTON=joy_bck),
"UI_UP":             input.KeyBase("999.Z",  "UPARROWKEY",     "UI Up",     JOYAXIS=(1, "NEG", "B")),
"UI_DN":             input.KeyBase("999.Z",  "DOWNARROWKEY",   "UI Down",   JOYAXIS=(1, "POS", "B")),
"UI_LE":             input.KeyBase("999.Z",  "LEFTARROWKEY",   "UI Left",   JOYAXIS=(0, "NEG", "B")),
"UI_RT":             input.KeyBase("999.Z",  "RIGHTARROWKEY",  "UI Right",  JOYAXIS=(0, "POS", "B"))
}

print("keymap.py Imported")

