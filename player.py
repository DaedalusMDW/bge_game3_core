####
# bge_game3_core: Full python game structure for the Blender Game Engine
# Copyright (C) 2019  DaedalusMDW @github.com (Daedalus_MDW @blenderartists.org)
# https://github.com/DaedalusMDW/bge_game3_core
#
# This file is part of bge_game3_core.
#
#    bge_game3_core is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    bge_game3_core is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with bge_game3_core.  If not, see <http://www.gnu.org/licenses/>.
#
####

## PLAYER CLASS ##


from bge import logic, render

from . import keymap, base, HUD, config, viewport


class ActorLayout(HUD.HUDLayout):

	GROUP = "Core"
	MODULES = [
		HUD.Stats,	# Health Bars
		HUD.Interact,	# Target and Text
		HUD.Inventory,	# Number Slots and Side Icons
		HUD.Weapons,	# Top Center Indicator
		HUD.Target,
		HUD.Compass
		]


class CorePlayer(base.CoreAdvanced):

	NAME = "Player"
	HITBOX = "Player"
	PORTAL = True
	GHOST = True
	CLASS = "Standard"
	WP_TYPE = "RANGED"

	HAND = {"MAIN":"Hand_R", "OFF":"Hand_L"}
	SLOTS = {"THREE":"Hip_L", "FOUR":"Shoulder_L", "FIVE":"Back", "SIX":"Shoulder_R", "SEVEN":"Hip_R"}

	SPEED = 0.1
	JUMP = 6
	ACCEL = 20
	ACCEL_FAST = 10
	ACCEL_STOP = 10
	SLOPE = 60
	MOVERUN = True
	SIDESTEP = False
	AIR_DRAG = (0.5, 0.5, 0.25)
	GRAV_DAMAGE = 10
	PHYSICS = "DYNAMIC"

	INTERACT = 2
	EYE_H = 1.6
	GND_H = 1.0
	EDGE_H = 2.0
	WALL_DIST = 0.4
	WALL_ALIGN = False
	OFFSET = (0, 0.0, 0.15)
	SLOPE_SPEED = 1.0
	SLOPE_BIAS = 0.0
	CROUCH_H = 0.4
	CROUCH_SCALE = 0.2 #Capsule is 50% effected by scale
	ANIMSET = ""

	CAM_TYPE = "FIRST"
	CAM_FOV = 90
	CAM_ORBIT = 2
	CAM_SLOW = 10
	CAM_RANGE = (0.75, 6) #0.75 6
	CAM_HEIGHT = 0.15
	CAM_STEPS = 7
	CAM_ZOOM = 4
	CAM_MIN = 0.2

	CAM_AUTOZOOM = True

	HUDLAYOUT = ActorLayout

	def __init__(self, *args):
		if len(args) == 0:
			return
		self.owner = args[0]

		scene = base.SC_SCN
		char = self.owner

		char["Class"] = self

		self.objects = {"Root":None, "Character":char}

		if len(char.children) == 0:
			gimbal = scene.addObject("Gimbal", char, 0)
			gimbal.setParent(char)
			gimbal.localPosition = self.createVector()
			gimbal.localOrientation = self.createMatrix()
			gimbal.worldScale = self.createVector(fill=1)*0.5

		self.findObjects(char)

		self.ANIMOBJ = self.objects.get("Rig", None)

		self.defaultEvents()
		self.defaultStates()

		self.rayhit = None
		self.rayvec = None
		self.raycls = None

		self.player_id = None
		self.player_npc = True
		if "PID" in char:
			ID = char["PID"]
			if ID != None:
				if ID != -1:
					self.player_id = ID
				self.player_npc = False
			del char["PID"]

		self.jump_state = "NONE"
		self.jump_timer = 0
		self.lastaction = [None,0]
		self.vis_override = 2

		self.resetGroundRay()
		self.resetAcceleration()

		self.motion = {
			"Move": self.createVector(2),
			"Rotate": self.createVector(3),
			"Climb": 0,
			"Local": self.createVector(3),
			"World": self.createVector(3)
			}

		self.dict = char["DICT"]

		self.data = {"HEALTH":100, "ENERGY":100, "RECHARGE":0.05, "SPEED":self.SPEED,
			"JUMP":self.JUMP, "RUN":self.MOVERUN, "STRAFE":self.SIDESTEP}

		self.data["HUD"] = {"Text":"", "Color":(0,0,0,0.5), "Target":None, "Locked":None}

		self.data["POS_LEVEL"] = None
		self.data["PHYSICS"] = self.PHYSICS
		self.data["CROUCH"] = 0
		self.data["DAMPING"] = [0, 0]
		self.data["LINVEL"] = [0,0,0]
		self.data["ANGVEL"] = [0,0,0]

		self.data["JP_STATE"] = self.jump_state
		self.data["JP_TIMER"] = self.jump_timer

		dict = self.defaultData()
		for key in dict:
			if key not in self.data:
				self.data[key] = dict[key]

		char["DEBUG1"] = ""
		char["DEBUG2"] = ""
		char["RAYTEXT"] = ""
		char.addDebugProperty("DEBUG1", True)
		char.addDebugProperty("DEBUG2", True)
		char.addDebugProperty("RAYTEXT", True)

		self.setPhysicsGrid()

		self.doLoad()

		self.hideObject(False)
		self.doPlayerAnim("RESET")

		self.ST_Startup()

		if self.getParent() == None and self.dict.get("Vehicle", None) != None:
			print("VEH REMOVE", self.NAME)
			self.dict["Vehicle"] = None

		if self.dict.get("Vehicle", None) == None:
			if self.player_id != None:
				self.switchPlayerActive(self.player_id)
				viewport.loadCamera()
				if self.jump_state == "FLYING":
					self.ST_Advanced_Set()
			elif self.player_npc == True:
				self.switchPlayerNPC()
			else:
				self.switchPlayerPassive()

	def defaultStates(self):
		self.active_pre = [self.PR_LastVelocity]
		self.active_state = self.ST_Walking
		self.active_post = [self.PS_SetVisible, self.PS_GroundTrack]

	def resetGroundRay(self):
		self.groundhit = None
		self.groundold = None
		self.groundobj = None
		self.groundchk = False
		self.groundfrz = 0
		self.groundvel = self.createVector()
		self.groundpos = [self.createVector(), self.createVector()]
		self.groundori = [self.createMatrix(), self.createMatrix()]

		self.gposoffset = self.createVector(vec=[0,0,self.GND_H])
		self.gndraybias = self.GND_H

	def resetAcceleration(self, speed=(0,0,0)):
		self.accel_start = self.createVector(3)
		self.accel_move = self.createVector(vec=speed)
		self.accel_end = self.createVector(vec=speed)
		self.accel_timer = 0
		self.accel_stand = -1

	def doLoad(self):
		self.loadInventory()

		if self.player_npc == False:
			newdict = base.PROFILE["PLRData"].get(self.owner.name, self.dict)
			newdict = base.settings.LaunderDict(newdict)
			self.owner["DICT"] = newdict
			self.dict = newdict

		if self.dict["Data"] == None:
			self.dict["Data"] =  self.data
			self.data["ACTIVE_STATE"] = self.active_state.__name__
			self.data["LOD_STATE"] = "ACTIVE"
		else:
			self.data = self.dict["Data"]
			self.active_state = getattr(self, self.data["ACTIVE_STATE"], self.active_state)

		self.jump_state = self.data["JP_STATE"]
		self.jump_timer = self.data["JP_TIMER"]

		self.loadContainer()
		self.loadWorldPos()

		self.setLodState(self.data["LOD_STATE"])

	def doUpdate(self):
		self.data["JP_STATE"] = self.jump_state
		self.data["JP_TIMER"] = self.jump_timer
		self.data["ACTIVE_STATE"] = self.active_state.__name__

		self.saveWorldPos()

		owner = self.objects["Root"]
		if owner != None and owner.isSuspendDynamics == False:
			self.data["LINVEL"] = self.vecTuple(owner.localLinearVelocity)
			self.data["ANGVEL"] = [0,0,0]
			self.data["DAMPING"] = [owner.linearDamping, owner.angularDamping]

		self.updateChildren()

		if viewport.getController() == self:
			viewport.VIEWCLASS.doUpdate()

		if self.player_npc == False:
			newdict = base.settings.LaunderDict(self.dict)
			base.PROFILE["PLRData"][self.owner.name] = newdict

		if self.player_npc == False:
			if self.container_parent != None:
				cls = self.container_parent
				if self.dict in cls.dict["Children"]:
					cls.dict["Children"].remove(self.dict)
				if self.dict["Object"] not in cls.dict["PlayerChildren"]:
					cls.dict["PlayerChildren"].append(self.dict["Object"])
			if self.dict in base.LEVEL["DROP"]:
				base.LEVEL["DROP"].remove(self.dict)

	def destroy(self):
		name = self.dict["Object"]
		if name in base.PLAYER_CLASSES:
			del base.PLAYER_CLASSES[name]
		super().destroy()

	def getOwner(self):
		if self.objects["Root"] == None:
			return self.objects["Character"]
		else:
			return self.objects["Root"]

	def addPhysicsBox(self, obj=None):
		char = self.objects["Character"]

		if self.objects["Root"] != None:
			self.removePhysicsBox()
		if obj == None:
			obj = self.HITBOX

		owner = char.scene.addObject(obj, char, 0)
		owner.setDamping(self.data["DAMPING"][0], self.data["DAMPING"][1])
		owner["Class"] = self

		self.objects["Root"] = owner

		self.addCollisionCallBack()
		self.setPhysicsType()
		self.parentArmature(owner)

		return owner

	def removePhysicsBox(self):
		self.jump_state = "NONE"
		self.jump_timer = 0
		self.data["CROUCH"] = 0
		self.data["LINVEL"] = [0,0,0]

		if self.objects["Root"] != None:
			self.objects["Character"].removeParent()
			self.objects["Root"].endObject()
			self.objects["Root"] = None

	def replacePhysicsBox(self, obj, vel=True):
		char = self.objects["Character"]

		if self.objects["Root"] != None:
			if self.objects["Root"].name == obj:
				return self.objects["Root"]
			self.setPhysicsType("FREEZE")
			self.objects["Character"].removeParent()
			self.objects["Root"].endObject()
			self.objects["Root"] = None

		owner = char.scene.addObject(obj, char, 0)
		owner.setDamping(self.data["DAMPING"][0], self.data["DAMPING"][1])
		owner["Class"] = self

		self.objects["Root"] = owner

		self.addCollisionCallBack()
		self.setPhysicsType("RESUME")
		self.parentArmature(owner)

		return owner

	def parentArmature(self, obj, offset=False):
		char = self.objects["Character"]

		if offset == True:
			POS = self.OFFSET
		else:
			POS = (0,0,0)

		if char.parent != obj:
			char.setParent(obj)

		char.localPosition = POS
		char.localOrientation = self.createMatrix()

	def assignCamera(self):
		viewport.setCamera(self)
		viewport.setParent(self.owner)
		self.setCameraState(None)

	def switchPlayerNPC(self):
		self.resetGroundRay()
		self.resetAcceleration()
		self.doPlayerAnim("RESET")

		owner = self.addPhysicsBox()

		self.hideObject()

		self.active_state = self.ST_Idle
		self.player_id = None
		self.player_npc = True

	def switchPlayerActive(self, ID=None):
		self.resetGroundRay()
		self.resetAcceleration()
		self.doPlayerAnim("RESET")

		keymap.MOUSELOOK.center()

		owner = self.addPhysicsBox()

		self.active_state = self.ST_Walking
		self.player_npc = False

		self.assignCamera()
		self.hideObject()

		HUD.SetLayout(self)

		name = self.dict["Object"]
		if ID != None:
			self.player_id = ID
			base.WORLD["PLAYERS"][ID] = name

		base.PLAYER_CLASSES[name] = self

	def switchPlayerPassive(self):
		self.resetGroundRay()
		self.resetAcceleration()
		self.doPlayerAnim("RESET")

		self.data["HUD"]["Target"] = None
		self.data["HUD"]["Text"] = ""

		if viewport.getController() == self:
			viewport.setCamera(None)
		self.removePhysicsBox()

		self.active_state = self.ST_Walking

		name = self.dict["Object"]
		if name in base.PLAYER_CLASSES:
			del base.PLAYER_CLASSES[name]

		if self.player_id != None:
			self.player_npc = False
			self.doUpdate()

			ID = self.player_id
			base.WORLD["PLAYERS"][ID] = None
			self.player_id = None
			return ID

	def enterVehicle(self, seat):
		self.doAnim(STOP=True)

		if viewport.getController() == self:
			viewport.setCamera(None)
		self.removePhysicsBox()

		self.dict["Vehicle"] = True

		self.parentArmature(seat, True)

		if self.player_id != None:
			ID = self.player_id
			name = self.dict["Object"]
			base.WORLD["PLAYERS"][ID] = name
			base.PLAYER_CLASSES[name] = self

	def exitVehicle(self, pos, ori):
		self.objects["Character"].removeParent() 

		if self.player_npc == False:
			self.switchPlayerActive(self.player_id)
		else:
			self.switchPlayerNPC()

		self.dict["Vehicle"] = None

		self.getOwner().worldPosition = pos
		self.getOwner().worldOrientation = ori

		if viewport.getController() == self:
			self.alignPlayer()

	def alignCamera(self, factor=1.0, axis=(0,1,0), up=None, pitch=None):
		vref = self.getOwner().getAxisVect(axis)
		if up == None:
			up = self.getOwner().getAxisVect((0,0,1))
		if pitch == None:
			viewport.setDirection(vref, factor, up)
		else:
			viewport.pointCamera(vref, factor)

	def alignPlayer(self, factor=1.0, axis=None, up=None):
		if axis == None:
			axis = viewport.getDirection()
		self.objects["Root"].alignAxisToVect(axis, 1, factor)
		if up == None:
			self.alignToGravity()
		elif up != False:
			self.objects["Root"].alignAxisToVect(up, 2, 1.0)

	def setCameraState(self, state=None, load=False):
		if state == None:
			state = self.data["CAMERA"]["State"]

		pos = [0, 0, self.EYE_H-self.GND_H]

		if state == "SHOULDER":
			pos[0] = self.CAM_SHSIDE
			pos[2] = pos[2]-self.CAM_MIN
			viewport.setState("SHOULDER")
		elif state == "THIRD":
			viewport.setState("THIRD")
		else:
			viewport.setState("FIRST")

		viewport.setEyeHeight(eye=pos, set=True)

		self.data["CAMERA"]["FOV"] = self.CAM_FOV

	def getDropPoint(self):
		rotate = viewport.getObject("Rotate")
		drop = rotate.worldPosition+rotate.getAxisVect((0,self.INTERACT,0))

		if self.rayhit != None:
			if self.rayvec.length < (self.INTERACT+0.5):
				drop += (self.rayhit[2]*0.5)
			if self.rayvec.length < self.INTERACT:
				drop = self.rayhit[1]+(self.rayhit[2]*0.5)

		rayOBJ, rayPNT, rayNRM = self.owner.rayCast(drop+self.gravity, drop, 100, "GROUND", 1, 1, 0)

		if rayOBJ != None:
			drop = rayPNT+(rayNRM*0.5)

		return drop

	def getInputs(self):
		FORWARD = keymap.BINDS["PLR_FORWARD"].axis(clip=False) - keymap.BINDS["PLR_BACKWARD"].axis(clip=False)
		STRAFE = keymap.BINDS["PLR_STRAFERIGHT"].axis(clip=False) - keymap.BINDS["PLR_STRAFELEFT"].axis(clip=False)
		MOVE = keymap.input.JoinAxis(STRAFE, FORWARD)

		TURN = keymap.BINDS["PLR_TURNLEFT"].axis(clip=False) - keymap.BINDS["PLR_TURNRIGHT"].axis(clip=False)
		LOOK = keymap.BINDS["PLR_LOOKUP"].axis(clip=False) - keymap.BINDS["PLR_LOOKDOWN"].axis(clip=False)
		ROTATE = keymap.input.JoinAxis(LOOK, 0, TURN)
		ROTATE *= ROTATE.length

		CLIMB = keymap.BINDS["PLR_JUMP"].axis() - keymap.BINDS["PLR_DUCK"].axis()

		self.motion["Move"][0] = MOVE[0]
		self.motion["Move"][1] = MOVE[1]
		self.motion["Climb"] = CLIMB

		self.motion["Rotate"][0] = ROTATE[0]
		self.motion["Rotate"][1] = 0
		self.motion["Rotate"][2] = ROTATE[2]

		if keymap.BINDS["PLR_RUN"].tap() == True:
			self.data["RUN"] ^= True

		if keymap.BINDS["TOGGLECAM"].tap() == True:
			if self.data["CAMERA"]["State"] == "THIRD":
				self.setCameraState("FIRST")

			elif self.data["CAMERA"]["State"] == "FIRST":
				self.setCameraState("THIRD")

		if self.data["CAMERA"]["State"] == "THIRD":
			if keymap.BINDS["TOGGLESTRAFE"].tap() == True:
				self.data["STRAFE"] ^= True
		elif self.data["CAMERA"]["State"] == "FIRST":
			if keymap.BINDS["ZOOM_IN"].tap() == True:
				self.data["CAMERA"]["FOV"] -= 10
				if self.data["CAMERA"]["FOV"] < 50:
					self.data["CAMERA"]["FOV"] = 50
			if keymap.BINDS["ZOOM_OUT"].tap() == True:
				self.data["CAMERA"]["FOV"] += 10
				if self.data["CAMERA"]["FOV"] > self.CAM_FOV:
					self.data["CAMERA"]["FOV"] = self.CAM_FOV
			if self.motion["World"].length*(1/60) > 0.01:
				self.data["CAMERA"]["FOV"] = self.CAM_FOV

		for slot in self.data["SLOTS"]:
			key = self.data["SLOTS"][slot]
			cls = self.getSlotChild(slot)
			if cls != None and keymap.BINDS[key].tap() == True:
				if keymap.SYSTEM["ALT"].checkModifiers() == True:
					cls.dropItem(self.getDropPoint())
				else:
					cls.stateSwitch()

		if keymap.BINDS["TOGGLEHUD"].tap() == True:
			way = self.owner.worldPosition+base.ORIGIN_OFFSET
			pos = base.LEVEL["WAYPOINT"][-1]
			pos = self.createVector(vec=pos)

			if (way-pos).length > 5:
				base.LEVEL["WAYPOINT"].append(list(way))
				print("WAYPOINT ADD:", way)
			elif len(base.LEVEL["WAYPOINT"]) > 1:
				way = base.LEVEL["WAYPOINT"].pop()
				print("WAYPOINT REMOVE:", way)

		if keymap.BINDS["SUPER_DROP"].tap() == True:
			WPDROP = self.getDropPoint()
			ADD = self.gravity.normalized()*-0.5
			for cls in self.attachments.copy():
				cls.dropItem(WPDROP)
				WPDROP += ADD

		self.checkStability()

	def getGroundPoint(self, obj):
		if self.groundchk == True:
			print("WARNING: Ground reference already updated...")
			return

		newpos = obj.worldPosition.copy()
		newori = obj.worldOrientation.copy()

		self.groundpos.insert(0, newpos)
		self.groundpos.pop()
		self.groundori.insert(0, newori)
		self.groundori.pop()

		if base.ORIGIN_SHIFT != None:
			self.groundpos[1] -= base.ORIGIN_SHIFT
		self.groundchk = True

		if self.groundobj != obj or obj.get("MOVE", True) == False:
			self.groundobj = obj
			self.groundvel *= 0
			self.groundpos[1] = self.groundpos[0].copy()
			self.groundori[1] = self.groundori[0].copy()

		wp = self.groundhit[1]

		locOLD = wp - self.groundpos[1]
		posOLD = self.groundori[1].inverted()*locOLD
		posOLD += self.groundori[1].inverted()*self.gposoffset

		locNEW = wp - self.groundpos[0]
		posNEW = self.groundori[0].inverted()*locNEW
		posNEW += self.groundori[0].inverted()*self.gposoffset

		local = posOLD - posNEW
		self.groundvel = ((self.groundori[0]*local)*60)

	def checkGround(self, simple=False, ray=None):
		owner = self.objects["Root"]
		ground = None
		angle = 0
		move = self.motion["World"].copy()
		gndfrom = owner.worldPosition.copy()
		gndto = owner.getAxisVect((0,0,-self.GND_H))+gndfrom
		gndbias = 0
		tan = 0

		if ray == None:
			raylist = [gndto, gndfrom, self.gndraybias]
		else:
			raylist = list(ray)

		rayOBJ, rayPNT, rayNRM = owner.rayCast(raylist[0], raylist[1], raylist[2]+0.3, "GROUND", 1, 1, 0)

		if simple == False and rayOBJ == None and self.jump_state == "NONE":
			if self.groundfrz <= 1 and self.groundold != None:
				if base.ORIGIN_SHIFT != None:
					self.groundold[1] -= base.ORIGIN_SHIFT
				rayOBJ = self.groundold[0]
				rayPNT = gndto.copy()
				rayNRM = self.groundold[2].copy()
				self.groundfrz += 1
		else:
			self.groundfrz = 0

		if rayOBJ == None or rayOBJ.invalid == True or self.gravity.length < 0.1:
			self.groundfrz = 0
			if simple == True:
				self.gndraybias = self.GND_H+gndbias
				return None

		else:
			ground = [rayOBJ, rayPNT, rayNRM]

			angle = owner.getAxisVect((0,0,1)).angle(rayNRM, 0)
			angle = round(self.toDeg(angle), 2)
			gndbias = (angle/90)*self.SLOPE_BIAS

			if self.jump_state not in ["NONE", "CROUCH"] and ray == None:
				dist = owner.worldPosition-rayPNT
				vo = owner.localLinearVelocity[2]*(1/60)
				if vo > 0.02:
					ground = None
				if (dist.length+vo)-(self.GND_H+gndbias) > 0:
					ground = None
				if angle > self.SLOPE:
					ground = None

			if simple == True:
				self.groundfrz = 0
				self.gndraybias = self.GND_H+gndbias
				return ground

		if ground != None:
			self.groundhit = ground
			self.getGroundPoint(rayOBJ)

			dist = owner.worldPosition-rayPNT
			gnddiff = owner.worldOrientation.inverted()*(self.groundvel/60)

			self.gndraybias = dist.length+gndbias+gnddiff[2]

		else:
			self.groundvel *= 0
			self.gndraybias = self.GND_H+gndbias

		return ground, angle

	def checkWall(self, z=True, axis=None, simple=None, prop="GROUND"):
		owner = self.objects["Root"]

		if axis == None:
			axis = owner.getAxisVect((0,1,0))
		else:
			axis = self.createVector(vec=axis)
		dist = self.WALL_DIST+0.3
		angle = -1

		if simple != None:
			dist = simple

		WALLOBJ, WALLPNT, WALLNRM = owner.rayCast(owner.worldPosition+axis, None, dist, "GROUND", 1, 1, 0)

		if WALLOBJ != None:
			if simple != None:
				return WALLOBJ, WALLPNT, WALLNRM
			if z == False:
				WALLNRM = owner.localOrientation.inverted()*WALLNRM
				WALLNRM[2] = 0
				WALLNRM.normalize()
				WALLNRM = owner.localOrientation*WALLNRM
			if WALLNRM.length > 0.01:
				angle = axis.angle(WALLNRM*-1, 0)
				angle = round(self.toDeg(angle), 2)

		else:
			if simple != None:
				return None

		return angle, WALLNRM

	def doInteract(self, rayfrom=None):
		scene = base.SC_SCN
		owner = self.objects["Root"]

		dist = 100

		if self.data["CAMERA"]["State"] == "SHOULDER":
			if rayfrom == None:
				rayfrom = (0, 0, self.EYE_H-self.GND_H)
			rayfrom = self.getWorldSpace(owner, rayfrom)
		else:
			rayfrom = viewport.getObject("Rotate").worldPosition

		rayto = rayfrom+viewport.getRayVec()

		RAYHIT = owner.rayCast(rayto, rayfrom, dist, "", 1, 0, 0)

		RAYTARGPOS = [0.5, 0.5]
		RAYSEND = False
		RAYOBJ = None
		RAYCLS = None

		self.rayhit = None
		self.rayvec = None
		self.raycls = None

		k = self.getModifierValues("KEYRING")
		k.append("")

		if self.active_weapon != None:
			self.data["HUD"]["Color"] = (0,1,0,1)
			self.data["HUD"]["Text"] = self.active_weapon.NAME

		if RAYHIT[0] != None:
			self.rayhit = RAYHIT  #(RAYOBJ, RAYPNT, RAYNRM)
			self.rayvec = rayfrom-RAYHIT[1]
			self.raycls = self.rayhit[0].get("Class", None)

			screen = scene.active_camera.getScreenPosition(RAYHIT[1])
			RAYTARGPOS[0] = screen[0]
			RAYTARGPOS[1] = screen[1]

			if self.rayvec.length < self.INTERACT:
				RAYOBJ = RAYHIT[0]
				RAYCLS = self.raycls

				if "RAYCAST" in RAYOBJ:
					if self.active_weapon == None:
						self.data["HUD"]["Color"] = (0,1,0,1)
						RAYOBJ["RAYCAST"] = self
						if self.motion["Move"].length < 0.1:
							self.doMoveAlign(-self.rayvec*0.03)
						if RAYOBJ.get("RAYNAME", None) != None:
							self.data["HUD"]["Text"] = RAYOBJ["RAYNAME"]
						RAYSEND = True
					else:
						self.data["HUD"]["Color"] = (1,0,0,1)
						self.data["HUD"]["Text"] = self.active_weapon.NAME

				if self.raycls != None:
					self.sendEvent("INTERACT", self.raycls, "SEND", OBJECT=RAYOBJ, TYPE="RAY")
					#RAYSEND = True

				elif self.rayvec.dot(RAYHIT[2]) < 0 and config.DO_STABILITY == True:
					self.data["HUD"]["Color"] = (0,0,1,1)
					self.data["HUD"]["Text"] = "Press "+keymap.BINDS["ACTIVATE"].input_name+" To Ghost Jump"
					if keymap.BINDS["ACTIVATE"].tap() == True:
						owner.worldPosition = RAYHIT[1]+(RAYHIT[2]*self.WALL_DIST)

		all = self.getAllEvents("INTERACT", "RECEIVE")

		for evt in all:
			if evt != None:
				l = evt.getProp("LOCK", "")
				o = evt.getProp("OBJECT", None)
				t = evt.getProp("TYPE", None)

				if self.active_weapon == None:
					self.data["HUD"]["Color"] = (0,1,0,1)
					self.data["HUD"]["Text"] = evt.getProp("NAME", "")
					if l not in k:
						self.data["HUD"]["Locked"] = True
					elif t == "RAY":
						if self.motion["Move"].length < 0.1 and self.rayvec != None:
							self.doMoveAlign(-self.rayvec*0.03)
						RAYSEND = True

					if keymap.BINDS["ACTIVATE"].tap() == True:
						for i in k:
							self.sendEvent("INTERACT", evt.sender, "TAP", "ACTOR", OBJECT=o, LOCK=i)

					elif keymap.BINDS["ACTIVATE"].active() == True:
						for i in k:
							self.sendEvent("INTERACT", evt.sender, "ACTOR", OBJECT=o, LOCK=i)

				else:
					self.data["HUD"]["Color"] = (1,0,0,1)
					self.data["HUD"]["Text"] = self.active_weapon.NAME

		for obj in self.collisionList:
			cls = obj.get("Class", None)
			if "COLLIDE" in obj and cls != None and RAYSEND == False:
				self.sendEvent("INTERACT", cls, "SEND", OBJECT=obj, TYPE="COLLIDE")

		if self.groundhit != None:
			rayfoot = self.groundhit[0].get("RAYFOOT", [])
			if rayfoot == None:
				rayfoot = self
			elif self not in rayfoot:
				rayfoot.append(self)
			self.groundhit[0]["RAYFOOT"] = rayfoot

		self.data["HUD"]["Target"] = RAYTARGPOS

	def doJump(self, height=None, move=0.5, align=False):
		owner = self.objects["Root"]

		self.jump_state = "JUMP"
		self.jump_timer = 0

		owner.setDamping(0, 0)

		if height == None:
			height = self.JUMP

		#align = owner.localLinearVelocity.copy()*align
		#align[2] = 0
		#if align.length > 0.02 and move > 0:
		#	if self.data["STRAFE"] == False:
		#		self.alignPlayer(axis=owner.getAxisVect(align))

		owner.localLinearVelocity[0] *= move
		owner.localLinearVelocity[1] *= move

		owner.localLinearVelocity[2] = height+(self.motion["Local"][2]*0.5)

		self.doPlayerAnim("JUMP", 5)

	def doMovement(self, vec, mx=0):
		owner = self.objects["Root"]

		local = self.motion["Local"].copy()
		local[2] = 0
		world = owner.worldOrientation*local

		if self.groundhit != None:
			rayNRM = self.groundhit[2]
		else:
			rayNRM = owner.getAxisVect((0,0,1))

		angle = owner.getAxisVect((0,0,1)).angle(rayNRM, 0)
		angle = round(self.toDeg(angle), 2)

		dot = 0
		if angle > 5 and local.length > 0.01:
			slvec = owner.worldOrientation.inverted()*rayNRM
			slvec[2] = 0

			slang = slvec.normalized().angle(local.normalized(), 0)
			slang = round(self.toDeg(slang), 2)

			if angle <= self.SLOPE:
				dot = abs(1-(slang/90))*self.SLOPE_SPEED

		slope = 1-(dot*(angle/90))

		mref = (vec*mx*slope)

		if self.ACCEL < 1:
			self.accel_end = mref.copy()
			self.accel_move = mref.copy()

			move = self.accel_move.copy()

		else:
			check = self.accel_end-mref
			if check.length > 0.001:
				self.accel_start = self.accel_move.copy()
				self.accel_end = mref.copy()
				self.accel_timer = 0
				if self.accel_stand == -1 and self.accel_start.length < 0.001:
					self.accel_stand = 0
				elif self.accel_start.normalized().dot(self.accel_end.normalized()) < 0:
					self.accel_stand = 0

			rate = (1/self.ACCEL)
			if self.accel_stand == -1:
				rate = (1/self.ACCEL_FAST)
			if self.accel_end.length < 0.001:
				rate = (1/self.ACCEL_STOP)

			offset = self.accel_end-self.accel_start
			self.accel_move += offset*rate
			check = self.accel_end-self.accel_move

			greater = offset.normalized().dot(check.normalized())

			if greater < 0 or check.length < 0.001:
				self.accel_move = mref.copy()

			if self.accel_timer < self.ACCEL:
				self.accel_timer += 1
			else:
				self.accel_timer = self.ACCEL

			if self.accel_stand >= 0:
				self.accel_stand += 1
			if self.accel_stand >= self.ACCEL:
				self.accel_stand = -1

			move = self.accel_move.copy()

		if angle > 5 and angle <= self.SLOPE and world.length > 0.001:
			tango = rayNRM.angle(world.normalized(), 0)
			tango = round(self.toDeg(tango), 2)

			tan = self.getSlopeOffset(tango-90, world.length/60)
			tangle = owner.getAxisVect((0,0,1))*tan
			move += tangle
			#self.gndraybias += tan

		self.objects["Character"]["DEBUG1"] = str(round(move.length, 3))
		owner.worldLinearVelocity = move*60

	def doMoveAlign(self, axis=None, up=None, margin=0.002):
		owner = self.objects["Root"]

		align = owner.worldLinearVelocity*(1/60)
		if axis != None:
			align = self.createVector(vec=axis)
		if viewport.getController() == self:
			if self.data["STRAFE"] == True or self.data["CAMERA"]["State"] != "THIRD":
				align = viewport.getDirection((0,1,0))

		align = owner.worldOrientation.inverted()*align

		if align.xy.length >= margin:
			mref = align.xy.normalized()
			yref = self.createVector(vec=[0,1,0]).xy

			ang = self.toDeg(yref.angle_signed(mref, 0))
			dot = 1-((yref.dot(mref)*0.5)+0.5)
			dot = 0.1+((dot)*0.2)

			fac = align.length/(self.data["SPEED"]*0.5)
			if fac >= 1 or self.data["STRAFE"] == True:
				fac = 1

			rot = self.createMatrix([0,0,-ang*dot*fac])
			rot.rotate(owner.worldOrientation)
			owner.worldOrientation = rot

		if up == None:
			self.alignToGravity()
		elif up != False:
			owner.alignAxisToVect(up, 2, 1.0)

	def doPlayerOnGround(self):
		owner = self.objects["Root"]
		char = self.objects["Character"]

		move = self.motion["Move"].normalized()

		if self.data["RUN"] == False or self.motion["Move"].length <= 0.7 or self.data["SPEED"] <= 0.035:
			mx = 0.035
		else:
			mx = self.data["SPEED"]

		vref = viewport.getDirection((move[0], move[1], 0))
		self.doMovement(vref, mx)
		self.doMoveAlign(up=False)

		linLV = self.motion["Local"].copy()
		linLV[2] = 0
		action = "IDLE"

		if linLV.length > 0.01:
			action = "FORWARD"

			if linLV[1] < 0:
				action = "BACKWARD"
			if linLV[0] > 0.5 and abs(linLV[1]) < 0.5:
				action = "STRAFE_R"
			if linLV[0] < -0.5 and abs(linLV[1]) < 0.5:
				action = "STRAFE_L"

			if linLV.length <= (0.05*60):
				action = "WALK_"+action

		self.doPlayerAnim(action, blend=10)

		## Jump/Crouch ##
		if keymap.BINDS["PLR_DUCK"].active() == True:
			self.doCrouch(True)

		elif keymap.BINDS["PLR_JUMP"].tap() == True:
			if self.jump_timer == 1:
				self.jump_timer = 2
		elif keymap.BINDS["PLR_JUMP"].active() == True:
			if self.jump_timer >= 2:
				self.jump_timer += 1
			if self.jump_timer >= 7 and keymap.BINDS["PLR_JUMP"].axis() > 0.9:
				self.doJump(height=self.JUMP*0.9, move=0.8, align=True)
		elif keymap.BINDS["PLR_JUMP"].released() == True:
			if self.jump_timer >= 1:
				self.doJump(height=self.JUMP*0.6, move=0.9, align=False)
		else:
			self.jump_timer = 1

	def doPlayerAnim(self, action="IDLE", blend=10):
		if self.ANIMOBJ == None:
			return

		if action == "LAND":
			self.doAnim(STOP=True)
			return

		if action == "JUMP":
			self.doAnim(NAME=self.ANIMSET+"Jumping", FRAME=(0,20), PRIORITY=2, MODE="PLAY", BLEND=10)
			self.lastaction = [action, 0]
			return

		if action == "FALLING":
			self.doAnim(NAME=self.ANIMSET+"Jumping", FRAME=(20,20), PRIORITY=3, MODE="LOOP", BLEND=blend)
			self.lastaction = [action, 0]
			return

		if action == "DIVE":
			self.doAnim(NAME=self.ANIMSET+"Jumping", FRAME=(40,40), PRIORITY=3, MODE="LOOP", BLEND=blend)
			self.lastaction = [action, 0]
			return

		if action == "CROUCH_FALL":
			self.doAnim(NAME=self.ANIMSET+"Jumping", FRAME=(30,30), PRIORITY=3, MODE="LOOP", BLEND=blend)
			self.lastaction = [action, 0]
			return

		if action == "CROUCH_IDLE":
			self.doAnim(NAME=self.ANIMSET+"Crouching", FRAME=(0,0), PRIORITY=3, MODE="LOOP", BLEND=blend)
			self.lastaction = [action, 0]
			return

		if action in ["IDLE", "RESET"]:
			invck = 0
			if self.getSlotChild("Hip_L") != None or self.getSlotChild("Hip_R") != None: #REPLACE WITH EVENT
				invck = -5

			if action == "RESET":
				self.doAnim(STOP=True)
				self.doAnim(NAME=self.ANIMSET+"Jumping", FRAME=(0+invck,0+invck))
			else:
				self.doAnim(NAME=self.ANIMSET+"Jumping", FRAME=(0+invck,0+invck), PRIORITY=3, MODE="LOOP", BLEND=blend)
			self.lastaction = [action, 0]
			return

		if self.ACCEL > 10 and self.accel_stand >= 0:
			blend = self.ACCEL

		stair = ""
		if self.groundhit != None:
			angle = self.objects["Character"].getAxisVect((0,0,1)).angle(self.groundhit[2], 0)
			angle = round(self.toDeg(angle), 2)

			if angle > 20 and angle <= self.SLOPE:
				stair = ".Stairs"

		## ANIMATIONS ##
		if action == "FORWARD":
			self.doAnim(NAME=self.ANIMSET+"Running"+stair, FRAME=(0,39), PRIORITY=3, MODE="LOOP", BLEND=blend)
			setframe = 39

		elif action == "WALK_FORWARD":
			self.doAnim(NAME=self.ANIMSET+"Walking", FRAME=(0,59), PRIORITY=3, MODE="LOOP", BLEND=blend)
			setframe = 59

		elif action == "BACKWARD":
			self.doAnim(NAME=self.ANIMSET+"Walking", FRAME=(59,0), PRIORITY=3, MODE="LOOP", BLEND=blend)
			setframe = 59

		elif action == "WALK_BACKWARD":
			self.doAnim(NAME=self.ANIMSET+"Walking", FRAME=(59,0), PRIORITY=3, MODE="LOOP", BLEND=blend)
			setframe = 59

		elif action == "CROUCH_FORWARD":
			self.doAnim(NAME=self.ANIMSET+"Crouching", FRAME=(0,79), PRIORITY=3, MODE="LOOP", BLEND=blend)
			setframe = 79

		else:
			self.doPlayerAnim("IDLE")
			return

		if self.lastaction[0] != action:
			self.lastaction[0] = action
			self.doAnim(SET=self.lastaction[1]*setframe)

		self.lastaction[1] = self.doAnim(CHECK="FRAME")/setframe

	def doCrouch(self, state):
		self.jump_timer = 0
		if state == True or self.data["CROUCH"] != 0:
			self.replacePhysicsBox("PlayerCrouch")
			self.doPlayerAnim("CROUCH_IDLE", blend=10)
			self.jump_state = "CROUCH"
			self.active_state = self.ST_Crouch
		elif state == False:
			self.replacePhysicsBox(self.HITBOX)
			self.doAnim(STOP=True)
			self.jump_state = "NONE"
			self.active_state = self.ST_Walking

	def hideObject(self, state=None):
		char = self.objects["Character"]

		if state != None:
			state = (state==False)
			char.setVisible(state, True)
		elif viewport.getController() == self and self.data["CAMERA"]["State"] == "FIRST" and self.vis_override <= 0:
			char.setVisible(False, True)
		else:
			char.setVisible(True, True)

		if "SKT" in self.objects:
			for key in self.HAND:
				hand = self.HAND[key]
				skt = self.objects["SKT"].get(hand, None)
				if skt != None:
					skt.setVisible(True, True)

	def manageStatAttr(self):
		if self.data["HEALTH"] < 0:
			self.data["HEALTH"] = -1
		if self.data["ENERGY"] < 100:
			self.data["ENERGY"] += self.data["RECHARGE"]
			if self.data["ENERGY"] > 100:
				self.data["ENERGY"] = 100
		if self.data["HEALTH"] > 200:
			self.data["HEALTH"] = 200
		if self.data["ENERGY"] > 200:
			self.data["ENERGY"] = 200

	## INIT STATE ##
	def ST_Startup(self):
		pass

	## PRE ##
	def PR_LastVelocity(self):
		owner = self.objects["Root"]

		gndvel = self.groundvel.copy()

		if owner != None:
			linWV = owner.worldLinearVelocity-gndvel
			linLV = owner.worldOrientation.inverted()*linWV
		else:
			linWV = self.createVector()
			linLV = self.createVector()

		self.motion["Local"] = linLV
		self.motion["World"] = linWV
		#print(linWV)

	## POST ##
	def PS_GroundTrack(self):
		owner = self.objects["Root"]
		self.groundchk = False
		if owner == None:
			self.groundhit = None
			self.groundold = None
			self.groundobj = None
			return

		if self.groundhit == None or self.jump_state not in ["NONE", "CROUCH", "JUMP"]:
			drag = self.createVector(vec=self.AIR_DRAG)*self.air_drag
			LV = owner.localLinearVelocity.copy()
			LV -= (owner.worldOrientation.inverted()*self.air_linv)
			dragX = LV[0]*drag[0] #0.67
			dragY = LV[1]*drag[1] #0.67
			dragZ = LV[2]*drag[2] #0.33
			owner.applyForce((-dragX, -dragY, -dragZ), True)
			#self.groundvel = self.air_linv.copy()
			self.groundhit = None
			self.groundold = None
			self.groundobj = None
			return

		offset = self.groundvel.copy()
		if self.jump_state in ["NONE", "CROUCH"] and base.ORIGIN_SHIFT == None:
			offset -= ((owner.getAxisVect([0,0,1])*self.gndraybias)-self.gposoffset)*12

		if self.data["PHYSICS"] in ["DYNAMIC", "RIGID"]:
			owner.worldLinearVelocity += offset #+self.air_linv
		else:
			owner.worldPosition += (offset/60) #+(self.air_linv/60)

		if "ROTATE" in self.groundhit[0]:
			yvec = owner.getAxisVect((0,1,0))
			rotOLD = self.groundori[1].inverted()*yvec
			rotOLD = self.groundori[0]*rotOLD
			rotOLD = owner.worldOrientation.inverted()*rotOLD
			rotOLD[2] *= 0
			euler = self.createVector(vec=(0,1,0)).rotation_difference(rotOLD).to_euler()
			if viewport.getController() == self:
				if self.data["CAMERA"]["State"] != "THIRD" or self.data["STRAFE"] == True:
					rotZ = self.createMatrix(rot=euler, deg=False)
					rotZ.rotate(viewport.getObject("Root").worldOrientation)
					viewport.getObject("Root").worldOrientation = rotZ

			rotZ = self.createMatrix(rot=euler, deg=False)
			rotZ.rotate(owner.worldOrientation)
			owner.worldOrientation = rotZ

		owner.applyForce(-self.gravity, False)

		self.groundold = self.groundhit.copy()

		self.groundhit = None

	def PS_SetVisible(self):
		if self.vis_override <= 0:
			self.hideObject()
			self.vis_override = 0
		else:
			print("Player Vis Block", self.vis_override)
			self.vis_override -= 1

	## WALKING STATE ##
	def ST_Crouch(self):
		self.getInputs()

		scene = base.SC_SCN
		owner = self.objects["Root"]
		char = self.objects["Character"]

		## GROUND CHECK ##
		ground, angle = self.checkGround()

		owner.setDamping(0, 0)

		if ground != None:
			owner.worldLinearVelocity = (0,0,0)

			if ground[0].getPhysicsId() != 0:
				impulse = self.gravity*owner.mass*(1/60)
				ground[0].applyImpulse(ground[1], impulse, False)

			axis = owner.getAxisVect((0,0,2))
			point = ground[1]+axis
			dist = point-owner.worldPosition

			chkwall = True
			if self.data["CROUCH"] == "DONE": #-20
				if keymap.BINDS["PLR_DUCK"].released() == True:
					chkwall = False
			elif self.data["CROUCH"] < -5:
				chkwall = False

			if keymap.BINDS["PLR_JUMP"].tap() == True:
				chkwall = False
			if self.checkWall(axis=axis.normalized(), simple=dist.length) != None:
				chkwall = True

		else:
			chkwall = False

		## MOVEMENT ##
		mx = 0
		move = self.motion["Move"].normalized()
		if self.motion["Move"].length > 0.01:
			mx = 0.02

		vref = viewport.getDirection((move[0], move[1], 0))
		self.doMovement(vref, mx)
		self.doMoveAlign()

		## CROUCH STATE ##
		if self.data["CROUCH"] != "DONE":
			if self.data["CROUCH"] > -5:
				chkwall = True

		if chkwall == False:
			self.doPlayerAnim("IDLE", blend=10)

			if self.data["CROUCH"] == "DONE":
				self.data["CROUCH"] = -20
			elif self.data["CROUCH"] == -10:
				self.data["CROUCH"] = 0
				self.doCrouch(False)
			else:
				self.data["CROUCH"] += 1
				self.gndraybias += self.CROUCH_H*0.1
				#owner.worldPosition += owner.getAxisVect((0,0,1))*(self.CROUCH_H*0.1)
				owner.localLinearVelocity[2] += (self.CROUCH_H*0.1)*60

		else:
			linLV = self.motion["Local"].copy()
			linLV[2] = 0

			action = "IDLE"
			if linLV.length > 0.01:
				action = "FORWARD"

			self.doPlayerAnim("CROUCH_"+action, blend=10)

			if self.data["CROUCH"] == "DONE":
				pass
			elif self.data["CROUCH"] == 10:
				self.data["CROUCH"] = "DONE" #-20
			elif self.data["CROUCH"] >= 0:
				self.data["CROUCH"] += 1
				self.gndraybias -= self.CROUCH_H*0.1
				#owner.worldPosition -= owner.getAxisVect((0,0,1))*(self.CROUCH_H*0.1)
				owner.localLinearVelocity[2] -= (self.CROUCH_H*0.1)*60

		if self.data["CROUCH"] == "DONE":
			fac = 1
		else:
			fac = self.data["CROUCH"]*0.1
			if self.data["CROUCH"] < 0:
				fac = (self.data["CROUCH"]+10)*-0.1

		cr_fac = self.GND_H-(fac*self.CROUCH_H)

		## EYE HEIGHT ##
		self.gposoffset = owner.getAxisVect((0,0,1))*cr_fac

		char.localPosition[2] = fac*self.CROUCH_H

		eyevec = [0, 0, (self.EYE_H-self.GND_H)]

		if self.data["CAMERA"]["State"] == "SHOULDER":
			eyevec[0] = self.CAM_SHSIDE
			eyevec[2] -= self.CAM_MIN

		eyevec[2] *= (1-fac)
		eyevec[2] -= (self.CROUCH_H-0.2)*fac

		viewport.setEyeHeight(eye=eyevec, set=True)

		self.doInteract(rayfrom=eyevec)
		self.weaponManager()

		self.objects["Character"]["DEBUG2"] = self.data["CROUCH"]

	def ST_Walking(self):
		self.getInputs()

		scene = base.SC_SCN
		owner = self.objects["Root"]
		char = self.objects["Character"]

		move = self.motion["Move"].normalized()
		self.gposoffset = owner.getAxisVect((0,0,1))*self.GND_H
		ground, angle = self.checkGround()

		owner.setDamping(0, 0)

		if ground != None:
			impulse = (self.gravity*owner.mass)*(1/60)
			if self.jump_state != "NONE": # and owner.localLinearVelocity[2] < 0.1:
				vel = self.motion["World"]*(1/60)
				impulse += vel

				self.jump_timer = 0
				self.jump_state = "NONE"
				self.resetAcceleration(vel)
				self.doPlayerAnim("LAND")

			if self.jump_state == "NONE":
				owner.worldLinearVelocity = (0,0,0)

				self.doPlayerOnGround()
				impulse -= self.accel_move

			if ground[0].getPhysicsId() != 0:
				ground[0].applyImpulse(ground[1], impulse, False)

			self.objects["Character"]["DEBUG2"] = str(angle)

		elif self.gravity.length <= 0.1:
			self.doPlayerAnim("FALLING")
			self.jump_state = "FLOATED"

			#if self.rayvec != None:
			#	if self.rayvec.length < self.INTERACT:
			#		self.data["HUD"]["Color"] = (0, 1, 0, 0.5)
			#		self.data["HUD"]["Text"] = "Press "+keymap.BINDS["PLR_JUMP"].input_name+" To Wall Shove"
			#		if keymap.BINDS["PLR_JUMP"].tap() == True:
			#			owner.worldLinearVelocity = self.rayvec.normalized()*6

			vref = (self.motion["Move"][0], self.motion["Move"][1], self.motion["Climb"])
			vref = viewport.getDirection(vref)
			#vref.normalize()

			owner.applyForce(vref*5, False)
			owner.applyForce(owner.worldLinearVelocity*-1, False)

			owner.alignAxisToVect(viewport.getRayVec((0,1,0)), 1, 1.0)
			owner.alignAxisToVect(viewport.getRayVec((0,0,1)), 2, 1.0)
			self.alignCamera(0.5, pitch=True)

		elif self.jump_state == "NONE":
			self.jump_state = "FALLING"

			if keymap.BINDS["PLR_JUMP"].active() == True and self.jump_timer >= 1:
				self.doJump(move=0.8, align=True)
			elif keymap.BINDS["PLR_DUCK"].active() != True:
				self.doJump(height=1, move=0.5)

		else:
			if keymap.BINDS["PLR_DUCK"].active() == True:
				self.doPlayerAnim("CROUCH_FALL")
				self.replacePhysicsBox("PlayerSphere")
			else:
				self.doPlayerAnim("FALLING")
				self.replacePhysicsBox(self.HITBOX)

			axis = owner.worldLinearVelocity*(1/60)*0.1

			if self.WALL_ALIGN == True:
				angle, wallnrm = self.checkWall()
				if wallnrm != None and angle < 50:
					axis = wallnrm*-1
					#axis *= 0
					#self.alignPlayer(0.1, wallnrm*-1)

			if self.jump_state != "NO_AIR":
				self.doMoveAlign(axis, up=False, margin=0.001)

				vref = viewport.getDirection((move[0], move[1], 0))

				mx = abs(owner.localLinearVelocity[2])
				if owner.localLinearVelocity[2] > 0:
					mx = 1
				elif mx > 5:
					mx = 7
				else:
					mx += 2

				owner.applyForce(vref*mx, False)

			if keymap.BINDS["PLR_JUMP"].active() == True:
				if self.jump_state == "JUMP":
					self.jump_state = "A_JUMP"
				if self.jump_state == "A_JUMP":
					if owner.localLinearVelocity[2] < 0:
						self.jump_state = "FALLING"
				if self.jump_state == "FALLING":
					self.jump_state = "B_JUMP"
			else:
				self.jump_state = "FALLING"

			self.jump_timer += 1

			self.objects["Character"]["DEBUG2"] = str(round(owner.localLinearVelocity[2], 2))

		self.alignToGravity()
		self.doInteract()
		self.weaponManager()

		if self.active_weapon == None and keymap.BINDS["TOGGLEMODE"].tap() == True:
			self.ST_Advanced_Set()

	def ST_Walking_Set(self):
		self.doAnim(STOP=True)
		self.resetAcceleration()
		self.active_state = self.ST_Walking

	def ST_Idle(self):
		scene = base.SC_SCN
		owner = self.objects["Root"]
		char = self.objects["Character"]

		self.gposoffset = owner.getAxisVect((0,0,1))*self.GND_H
		ground, angle = self.checkGround()

		owner.setDamping(0, 0)

		if ground != None:
			if self.jump_state != "NONE":
				self.jump_timer = 0
				self.jump_state = "NONE"
				vel = self.motion["World"]*(1/60)
				self.resetAcceleration(vel)
				self.doPlayerAnim("LAND")

			owner.worldLinearVelocity = (0,0,0)

			self.doMoveAlign(up=False)

			linLV = self.motion["Local"].copy()
			linLV[2] = 0
			action = "IDLE"

			if linLV.length > 0.01:
				action = "FORWARD"

				if linLV[1] < 0:
					action = "BACKWARD"
				if linLV[0] > 0.5 and abs(linLV[1]) < 0.5:
					action = "STRAFE_R"
				if linLV[0] < -0.5 and abs(linLV[1]) < 0.5:
					action = "STRAFE_L"

				if linLV.length <= (0.05*60):
					action = "WALK_"+action

			self.doPlayerAnim(action, blend=10)

		elif self.jump_state == "NONE":
			self.doJump(height=1, move=0.5)

		else:
			self.doPlayerAnim("FALLING")
			self.jump_timer += 1

			axis = owner.worldLinearVelocity*(1/60)*0.1
			self.doMoveAlign(axis, up=False, margin=0.001)

		self.alignToGravity()

	## FLY STATE ##
	def ST_FlySimple(self):
		self.getInputs()

		scene = base.SC_SCN
		owner = self.objects["Root"]

		mx = 5
		if keymap.SYSTEM["SHIFT"].checkModifiers() == True:
			mx = 20
		move = self.motion["Move"]
		climb = self.motion["Climb"]

		owner.setDamping(0, 0)

		vref = viewport.getDirection((move[0], move[1], climb))
		owner.applyForce(vref*mx, False)

		owner.applyForce(-self.gravity, False)
		self.doMoveAlign(axis=viewport.getDirection((0,1,0)))

		self.jump_state = "FLYING"

		self.doPlayerAnim("FALLING")

		if keymap.BINDS["TOGGLEMODE"].tap() == True:
			self.jump_state = "FALLING"
			self.ST_Walking_Set()

	def ST_Advanced_Set(self):
		self.jump_state = "FLYING"
		self.data["HUD"]["Target"] = None
		self.active_state = self.ST_FlySimple

	def ST_Freeze(self):
		key = self.dict.get("Vehicle", None)
		cls = self.getParent()
		cam = viewport.getController()
		cdt = viewport.VIEWCLASS.camdata

		if cls != None and key not in [None, True] and cdt != None:
			cst = cdt["State"]
			veh = getattr(cls, "SEATS", {}).get(key, {})
			act = veh.get("ACTION", None)
			vis = veh.get("VISIBLE", None)

			if act != None and (cst == "THIRD" or self.player_id == None or cls != cam):
				self.hideObject(False)
				self.doAnim(NAME=self.ANIMSET+act, FRAME=(0,0))
				if vis != "FULL":
					for child in self.attachments:
						child.hideObject(True)

			else:
				self.hideObject(True)

		else:
			self.PS_SetVisible()

	## RUN ##
	def RUN(self):
		self.runModifiers()
		self.manageStatAttr()
		if self.objects["Root"] == None:
			self.ST_Freeze()
		else:
			self.runPre()
			#if self.phys_paused == False:
			self.runStates()
			self.runPost()
		self.runPhysMan()
		self.clearEvents()
		self.runChildren()



