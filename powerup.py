####
# bge_game3_core: Full python game structure for the Blender Game Engine
# Copyright (C) 2019  DaedalusMDW @github.com (Daedalus_MDW @blenderartists.org)
# https://github.com/DaedalusMDW/bge_game3_core
#
# This file is part of bge_game3_core.
#
#    bge_game3_core is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    bge_game3_core is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with bge_game3_core.  If not, see <http://www.gnu.org/licenses/>.
#
####

## POWERUPS ##


from bge import logic

from . import attachment, keymap


class CorePowerup(attachment.CoreAttachment):

	NAME = "PowerUp"
	WAIT = 3600
	DURATION = 0
	STACK_GROUP = None
	STACK_TYPE = "ADD"
	STACK_LIMIT = 1
	CAMPING = False
	COLLIDE = True
	OFFSET = (0,0,0)
	MODIFIER = {}

	BOX_MESH = "BOX_Sphere"
	BOX_NORMALIZE = False
	BOX_ALIGN = False
	BOX_GLOW = True

	def checkData(self, cls):
		return True

	def sendPowerup(self):
		dict = {}
		for key in self.MODIFIER:
			dict[key] = self.MODIFIER[key]
		return dict

	def equipItem(self, cls, load=False):
		if "MODIFIERS" not in cls.data:
			return
		if self.checkData(cls) == False:
			return

		grp = self.owner.name
		if self.STACK_GROUP != None:
			grp = self.STACK_GROUP

		dict = self.sendPowerup()
		dict["__GROUP__"] = dict.get("__GROUP__", grp)
		if self.DURATION >= 0:
			dict["__TIMER__"] = self.DURATION
			dict["__RATE__"] = 1
		else:
			dict["__TIMER__"] = 1
			dict["__RATE__"] = 0

		if self.STACK_TYPE == "NONE":
			cls.data["MODIFIERS"].append(dict)

		elif self.STACK_TYPE == "ADD":
			chk = 0
			for mod in cls.data["MODIFIERS"]:
				if mod.get("__GROUP__", None) == dict["__GROUP__"]:
					chk += 1
			if chk < self.STACK_LIMIT:
				cls.data["MODIFIERS"].append(dict)
			else:
				return

		elif self.STACK_TYPE == "REPLACE":
			chk = 0
			for i in range(len(cls.data["MODIFIERS"])):
				mod = cls.data["MODIFIERS"][i]
				if mod.get("__GROUP__", None) == dict["__GROUP__"]:
					cls.data["MODIFIERS"][i] = dict
					chk += 1
			if chk == 0:
				cls.data["MODIFIERS"].append(dict)

		self.data["COOLDOWN"] = self.WAIT

		self.active_state = self.ST_Wait

	## STATE INACTIVE ##
	def ST_Wait(self):
		if self.data["COOLDOWN"] < 0:
			self.endObject()
			return

		if self.data["COOLDOWN"] == 0:
			self.hideObject(False)
			self.data["COOLDOWN"] = 0
			self.campers = list(self.box["COLLIDE"])
			self.box["RAYNAME"] = self.NAME
			self.active_state = self.ST_Box
		else:
			self.hideObject(True)
			self.data["COOLDOWN"] -= 1
			self.box["RAYNAME"] = self.NAME+": "+str(int(self.data["COOLDOWN"]))

	## STATE BOX ##
	def ST_Box(self):
		if self.data["COOLDOWN"] != 0:
			self.active_state = self.ST_Wait
			return

		for cls in self.box["COLLIDE"]:
			if self.CAMPING == True or cls not in self.campers:
				self.equipItem(cls)

		for plr in self.campers:
			if plr not in self.box["COLLIDE"]:
				self.campers.remove(plr)


class CoreAmmo(attachment.CoreAttachment):

	NAME = "PowerUp"
	WAIT = -1
	DURATION = 0
	CAMPING = True
	LIMIT = 1
	COLLIDE = True
	OFFSET = (0,0,0)
	MODIFIER = {}

	def checkData(self, cls):
		return True

	## STATE BOX ##
	def ST_Box(self):
		count = int(self.LIMIT)
		for cls in self.box["COLLIDE"]:
			if count != 0:
				self.equipItem(cls)
				count -= 1


class CoreStats(CorePowerup):

	def checkData(self, cls):
		for key in ["HEALTH", "ENERGY"]:
			cap = self.MODIFIER.get("LIMIT", 100)
			chk = cls.data.get(key, cap)
			if chk < cap:
				return True
		return False

	def sendPowerup(self):
		dict = {"SUPER":False}
		for key in self.MODIFIER:
			dict[key] = self.MODIFIER[key]
		if dict.get("LIMIT", 100) > 100:
			dict["SUPER"] = True
		return dict


class CoreKey(CorePowerup):

	LOCK = 1
	WAIT = -1
	DURATION = -1
	STACK_GROUP = "PU_Key"
	STACK_TYPE = "NONE"
	COLLIDE = False

	BOX_MESH = "BOX_Drop"
	BOX_NORMALIZE = False
	BOX_ALIGN = False
	BOX_GLOW = False

	def defaultData(self):
		dict = super().defaultData()
		dict["LOCK"] = self.owner.get("LOCK", self.LOCK)

		return dict

	def ST_Startup(self):
		self.objects["Root"]["RAYNAME"] = "Key: "+str(self.data["LOCK"])

	def sendPowerup(self):
		return {"KEYRING": self.data["LOCK"], "__TEXT__":self.data["LOCK"]}

	## STATE BOX ##
	def ST_Box(self):
		if self.data["COOLDOWN"] != 0:
			self.active_state = self.ST_Wait
			return

		if self.box["RAYCAST"] != None and keymap.BINDS["ACTIVATE"].tap() == True:
			self.equipItem(self.box["RAYCAST"])

