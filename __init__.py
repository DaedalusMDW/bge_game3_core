####
# bge_game3_core: Full python game structure for the Blender Game Engine
# Copyright (C) 2019  DaedalusMDW @github.com (Daedalus_MDW @blenderartists.org)
# https://github.com/DaedalusMDW/bge_game3_core
#
# This file is part of bge_game3_core.
#
#    bge_game3_core is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    bge_game3_core is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with bge_game3_core.  If not, see <http://www.gnu.org/licenses/>.
#
####


import bge

import sys
import os.path as ospath

print(__file__)
GAMEPATH = ospath.normpath(__file__+"\\..\\..\\")
sys.path.append(GAMEPATH)

for i in sys.path:
	print("PATH:", i)

COREBLENDS = {
	"ASSETS": ospath.normpath(__file__+"\\..\\CoreAssets.blend"),
	"HUD": ospath.normpath(__file__+"\\..\\CoreOverlay.blend")
}
GAMEPATH += "\\"


## CHECK VERSION ##
from . import config

if hasattr(bge, "app"):
	config.UPBGE_FIX = True
	config.MOUSE_FIX = False
	config.ASYNC_FIX = False
	config.UPBGE_VER = bge.app.upbge_version
	if bge.app.upbge_version[1] >= 2:
		if bge.app.upbge_version[2] >= 2:
			config.MOUSE_FIX = True
		if bge.app.upbge_version[2] >= 4:
			config.ASYNC_FIX = True

else:
	config.UPBGE_FIX = False
	config.MOUSE_FIX = False
	config.ASYNC_FIX = False
	config.UPBGE_VER = (2,7,4)


## NO VIEWPORT PLAYER ##
try:
	import bpy
	config.EMBEDDED_FIX = True
	print("NOTICE: Embedded Player Dectected...")
except ImportError:
	config.EMBEDDED_FIX = False


## SETUP DATA ##
from . import settings

bge.logic.setMaxLogicFrame(1)
bge.logic.setMaxPhysicsFrame(1)
bge.logic.setLogicTicRate(60)
bge.logic.setPhysicsTicRate(60)

bge.logic.UPDATELIST = []
bge.logic.OLDUPDATELIST = []
bge.logic.ORPHANUPDATE = []
bge.logic.HUDCLASS = None
bge.logic.VIEWPORT = None

firstrun = settings.checkWorldData()

if config.UPBGE_FIX == False or config.EMBEDDED_FIX == True:
	pass
elif firstrun == True:
	sys.stdout = open(GAMEPATH+"stout.txt", "w")
	sys.stderr = open(GAMEPATH+"sterr.txt", "w")
else:
	sys.stdout = open(GAMEPATH+"stout.txt", "a")
	sys.stderr = open(GAMEPATH+"sterr.txt", "a")

print("UPBGE", config.UPBGE_VER)
print("\n\t...game3 core init...\n")

settings.LoadBinds()


