####
# bge_game3_core: Full python game structure for the Blender Game Engine
# Copyright (C) 2019  DaedalusMDW @github.com (Daedalus_MDW @blenderartists.org)
# https://github.com/DaedalusMDW/bge_game3_core
#
# This file is part of bge_game3_core.
#
#    bge_game3_core is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    bge_game3_core is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with bge_game3_core.  If not, see <http://www.gnu.org/licenses/>.
#
####

## DOOR OBJECTS ##


from bge import logic

from . import base, keymap


class CoreDoor(base.CoreObject):

	NAME = "Door"
	UPDATE = True
	CONTAINER = "LOCK"
	INTERACT = False
	OPEN = False
	OPENLOCK = False #event needs to have valid LOCK or KEYRING to interact with open door
	LOCK = ""
	SPRING = "SPRING"
	SPRINGLOCK = True #accept interaction while auto-closing
	TIME = -1
	ANIM = {"OPEN":(0,60), "CLOSE":(60,0)} # not customisable, subclass to change

	## GAME PROPERTIES ##
	# "LOCK": a variable which is required in the event (data LOCK or in KEYRING list) to open
	# "SPRING": the type of auto close one of "SPRING" (gradual close over TIME) or "MOTOR" (close after TIME)
	# "TIME": the duration of the auto close mechanism defined by SPRING (0 or less is none)
	# "PANEL": add this property `True` to all children that are to be animated
	# "ACTION": string specifying the action to play, otherwise use name of object
	# "BUTTON": add this property `True` to children allow this object to read interactions
	##

	def defaultData(self):
		lock = self.owner.get("LOCK", self.LOCK)
		time = self.owner.get("TIME", self.TIME)
		spring = self.owner.get("SPRING", self.SPRING)
		name = self.owner.get("RAYNAME", self.NAME)

		self.door_panels = []
		self.door_trigger = []

		for obj in list(self.owner.childrenRecursive)+[self.owner]:
			if obj.get("PANEL", False) == True:
				self.door_panels.append(obj)
			if obj.get("BUTTON", False) == True:
				obj["Class"] = self
				self.door_trigger.append(obj)

		#self.owner["OPEN"] = False
		#self.owner["FRAME"] = 0
		#self.owner.addDebugProperty("OPEN", True)
		#self.owner.addDebugProperty("FRAME", True)

		dict = super().defaultData()
		dict["FRAME"] = 0
		dict["TIME"] = 0
		dict["OPEN"] = None
		dict["LOCK"] = lock
		dict["TYPE"] = spring
		dict["SPRING"] = time
		dict["RAYNAME"] = name

		return dict

	def doLoad(self):
		super().doLoad()

		if self.data["OPEN"] == None:
			self.data["OPEN"] = self.owner.get("OPEN", self.OPEN)

			if self.data["OPEN"] == True:
				self.doPanelAction("OPEN", set=True)
				self.active_state = self.ST_Open
			else:
				self.doPanelAction("CLOSE", set=True)
				self.active_state = self.ST_Closed

		elif self.active_state == self.ST_Closed:
			self.doPanelAction("CLOSE", set=True)

		elif self.active_state == self.ST_Open:
			self.doPanelAction("OPEN", set=True)

	def doPanelAction(self, state, set=False, stop=False, fac=None):
		frame = (self.ANIM[state][0], self.ANIM[state][1])
		offset = 1

		if self.ANIM[state][0] < self.ANIM[state][1]:
			offset = 1
		elif self.ANIM[state][0] > self.ANIM[state][1]:
			offset = -1

		if fac != None:
			mix = (fac*(frame[1]-frame[0]))+frame[0]
			self.data["FRAME"] = mix
		elif set == True or stop == True:
			self.data["FRAME"] = frame[1]
		else:
			self.data["FRAME"] += offset
			if (self.data["FRAME"]*offset) >= (frame[1]*offset):
				self.data["FRAME"] = frame[1]

		for obj in self.door_panels:
			name = obj.get("ACTION", self.ANIM.get("ACTION", obj.name))

			if stop == True:
				self.doAnim(OBJECT=obj, STOP=True)
			elif set == True:
				self.doAnim(obj, name, (self.data["FRAME"], self.data["FRAME"]))
			else:
				frame = (frame[0]-(offset*5), frame[1]+(offset*5))
				self.doAnim(obj, name, frame)
				self.doAnim(OBJECT=obj, SET=self.data["FRAME"])

	def checkPanelAction(self, state):
		if self.data["FRAME"] == self.ANIM[state][1]:
			self.doPanelAction(state, stop=True)
			return True
		return False

	def clearRayProps(self):
		pass
		#self.owner["OPEN"] = self.data["OPEN"]
		#self.owner["FRAME"] = self.data["FRAME"]
		#for child in self.door_trigger:
		#	child["RAYCAST"] = None

	def checkClicked(self, obj=None):
		all = self.getAllEvents("INTERACT", "SEND")
		act = self.getAllEvents("INTERACT", "ACTOR", "TAP")
		sec = self.getAllEvents("INTERACT", "!ACTOR", "!SEND")

		for evt in all:
			t = evt.getProp("TYPE", None)
			self.sendEvent("INTERACT", evt.sender, "RECEIVE", LOCK=self.data["LOCK"], NAME=self.data["RAYNAME"], TYPE=t)

		for evt in (act+sec):
			if self.data["OPEN"] == True and self.OPENLOCK == False:
				return True

			if self.data["LOCK"] in ["", evt.getProp("LOCK")]:
				return True

		return False

	## OPEN STATE ##
	def ST_Open(self):
		self.data["FRAME"] = self.ANIM["CLOSE"][0]

		if self.data["TYPE"] == "SPRING" and self.data["SPRING"] >= 5:
			self.active_state = self.ST_Spring

		elif self.data["TYPE"] == "MOTOR" and self.data["SPRING"] >= 1:
			if self.checkClicked() == True:
				if self.SPRINGLOCK == False:
					self.data["TIME"] = self.data["SPRING"]
				else:
					self.data["TIME"] = 0

			if self.data["TIME"] >= self.data["SPRING"]:
				self.data["TIME"] = 0
				self.active_state = self.ST_Closing
			else:
				self.data["TIME"] += 1

		elif self.checkClicked() == True:
			self.active_state = self.ST_Closing

	def ST_Opening(self):
		self.doPanelAction("OPEN")

		if self.checkPanelAction("OPEN") == True:
			self.data["TIME"] = 0
			self.active_state = self.ST_Open

	def ST_Spring(self):
		self.doPanelAction("CLOSE", fac=self.data["TIME"]/self.data["SPRING"])

		self.data["TIME"] += 1

		if self.checkClicked() == True:
			self.active_state = self.ST_Opening

		elif self.checkPanelAction("CLOSE") == True:
			self.data["TIME"] = 0
			self.active_state = self.ST_Closing

	## CLOSED STATE ##
	def ST_Closed(self):
		self.data["FRAME"] = self.ANIM["OPEN"][0]

		if self.checkClicked() == True:
			self.data["OPEN"] = True
			self.active_state = self.ST_Opening

	def ST_Closing(self):
		self.doPanelAction("CLOSE")

		if self.checkPanelAction("CLOSE") == True:
			self.data["OPEN"] = False
			self.active_state = self.ST_Closed

