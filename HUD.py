####
# bge_game3_core: Full python game structure for the Blender Game Engine
# Copyright (C) 2019  DaedalusMDW @github.com (Daedalus_MDW @blenderartists.org)
# https://github.com/DaedalusMDW/bge_game3_core
#
# This file is part of bge_game3_core.
#
#    bge_game3_core is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    bge_game3_core is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with bge_game3_core.  If not, see <http://www.gnu.org/licenses/>.
#
####

## HUD ##


from bge import logic

from mathutils import Vector, Matrix

from . import keymap, base, settings, config, world, viewport, COREBLENDS, GAMEPATH


def START(cont):
	owner = cont.owner

	base.SC_HUD = owner.scene

	base.SC_HUD.post_draw.append(settings.SCREENSHOT)
	if settings.SCREENSHOT in base.SC_SCN.post_draw:
		base.SC_SCN.post_draw.remove(settings.SCREENSHOT)

	print("Loading HUD...")
	base.LEVEL["HUDData"] = {}

	logic.HUDCLASS = SceneManager()
	#logic.HUDCLASS.doBlackOut(True, que=True)


def RUN(cont):
	if logic.HUDCLASS != None and base.ORIGIN_SHIFT == None:
		try:
			logic.HUDCLASS.RUN()
		except Exception as ex:
			logic.HUDCLASS.setControl(None, None)
			logic.HUDCLASS = None
			print("FATAL RUNTIME ERROR:", cont.owner.name)
			print("\t", ex)


def SetLayout(plr=None, layout=None):
	if base.SC_HUD == None or logic.HUDCLASS == None:
		return

	logic.HUDCLASS.setControl(plr, layout)

def SetBlackScreen(mode=True):
	if base.SC_HUD == None or logic.HUDCLASS == None:
		return

	logic.HUDCLASS.doBlackOut(mode, que=True)

def GetController():
	if base.SC_HUD == None or logic.HUDCLASS == None:
		return None

	return logic.HUDCLASS.control

class CoreHUD(base.CoreObject):

	UPDATE = False
	OBJECT = "None"

	def __init__(self, prefix):
		scene = base.SC_HUD
		self.owner = scene.addObject(prefix+self.OBJECT, scene.objects["HUD.START"], 0)

		self.ANIMOBJ = None

		self.objects = {"Root":self.owner}

		self.data = self.defaultData()

		if self.OBJECT not in base.LEVEL["HUDData"]:
			base.LEVEL["HUDData"][self.OBJECT] = self.data
		else:
			self.data = base.LEVEL["HUDData"][self.OBJECT]

		self.active_pre = []
		self.active_state = self.ST_Active
		self.active_post = []

		self.findObjects()

		self.ST_Startup()

	def findObjects(self):
		obj = self.objects["Root"]
		dict = self.objects
		group = []
		list = []

		for child in obj.childrenRecursive:
			split = child.name.split(".")
			name = None

			if len(split) > 1:
				name = split[1]

			if name != None and name in list:
				if name not in group:
					group.append(name)

			if len(split) > 2:
				dict[split[1]] = {}
				if split[1] not in group:
					group.append(split[1])

			if name != None:
				list.append(name)

		for child in obj.childrenRecursive:
			split = child.name.split(".")
			if len(split) > 1:
				if split[1] in group:
					if len(split) <= 2:
						dict[split[1]][""] = child
					elif split[2] != "":
						dict[split[1]][split[2]] = child
				else:
					dict[split[1]] = child

	def destroy(self):
		self.objects["Root"].endObject()
		self.objects["Root"] = None

	def ST_Active(self, plr):
		pass

	def RUN(self):
		self.runPre()
		self.runStates()
		self.runPost()


class Compass(CoreHUD):

	OBJECT = "Compass"

	def defaultData(self):
		return {}

	def ST_Startup(self):
		oldcam = base.SC_HUD.active_camera
		base.SC_HUD.active_camera = self.objects["Cam"]
		RES = logic.globalDict["GRAPHICS"]["Resolution"]
		self.objects["Cam"].useViewport = True
		self.objects["Cam"].setViewport(0, 0, RES[0], RES[1])
		base.SC_HUD.active_camera = oldcam

	def destroy(self):
		self.objects["Cam"].useViewport = False
		self.objects["Root"].endObject()
		self.objects["Root"] = None

	def ST_Active(self, plr):
		player = plr.getOwner()
		camera = viewport.getOwner()

		cp_comp = self.objects["Direction"]
		cp_dist = self.objects["Distance"]
		cp_elev = self.objects["Elevation"]

		evt = plr.getFirstEvent("COMPASS", "NORTH")
		if evt != None:
			base.LEVEL["WAYPOINT"][0] = list(evt.getProp("POS"))
		else:
			base.LEVEL["WAYPOINT"][0] = (0,0,0)

		target = Vector(base.LEVEL["WAYPOINT"][-1])

		vg = target-(player.worldPosition+base.ORIGIN_OFFSET)

		vd = vg.length
		vc = (camera.worldOrientation.inverted()*vg)
		vl = (player.worldOrientation.inverted()*vg)

		vc[2] = 0

		rdif = vc.to_track_quat("Y", "Z").to_matrix()

		if vg.length < 5 or vd < 5:
			cp_comp.localOrientation = self.createMatrix()
		else:
			cp_comp.localOrientation = rdif

		if vd < 5:
			cp_comp.color = (0,0,0,0.5)
			cp_elev.color = (0,0,0,0.5)
		elif vl[2] < -4:
			cp_comp.color = (0,1,0,0.5)
			cp_elev.color = (0,0,0,0.5)
		elif vl[2] > 4:
			cp_comp.color = (0,0,0,0.5)
			cp_elev.color = (0,1,0,0.5)
		else:
			cp_comp.color = (0,1,0,0.5)
			cp_elev.color = (0,1,0,0.5)

		if vd < 300:
			vd = (1-(vd/300))**3
			cp_dist.color[1] = vd
		else:
			cp_dist.color[1] = 0


class Annotation(CoreHUD):

	OBJECT = "Annotation"

	def ST_Startup(self):
		self.textlist = None
		self.objlist = []
		self.timer = 0
		self.current = 0

		RES = logic.globalDict["GRAPHICS"]["Resolution"]
		self.objects["Cam"].setViewport(0, 0, RES[0], RES[1])
		self.objects["Cam"].useViewport = True

	def destroy(self):
		self.objects["Cam"].useViewport = False

		for obj in self.objlist:
			obj.endObject()

		self.objects["Root"].endObject()
		self.objects["Root"] = None

	def setSubtitle(self, subt):
		for obj in self.objlist:
			obj.endObject()

		self.textlist = subt
		self.timer = 0
		self.current = 0
		self.objlist = []

	def getChild(self, obj, name):
		child = obj.children["UI_Annotation"+name]
		return child

	def ST_Active(self, plr):
		fade = False
		if self.textlist != None:
			OBJ = self.objects["List"]
			CUR = self.textlist[self.current]

			if self.timer == 0:
				root = scene.addObject("UI_Annotation", OBJ, 0)
				self.getChild(root, "NameText").color = (CUR["COLOR"][0], CUR["COLOR"][1], CUR["COLOR"][2], 0)
				self.getChild(root, "NameText").text = CUR["NAME"]
				self.getChild(root, "NameMesh").color = (0, 0, 0, 0.5)
				self.getChild(root, "LineText").color = (0.8, 0.8, 0.8, 0)
				self.getChild(root, "LineText").text = CUR["LINE"]
				self.getChild(root, "LineMesh").localScale[1] = len(CUR["LINE"].split("\n"))

				self.objlist.append(root)

			if len(self.objlist) >= 2:
				fade = True

			self.timer += 1

			if self.timer > abs(CUR["TIME"])+30:
				self.timer = 0
				self.current += 1
				if self.current >= len(self.textlist):
					self.textlist = None
					self.current = 0

			elif self.timer <= 30:
				alpha = self.timer/30
				root = self.objlist[-1]
				self.getChild(root, "NameMesh").color[3] = alpha*0.5
				self.getChild(root, "NameText").color[3] = alpha
				self.getChild(root, "LineMesh").color[3] = alpha*0.5
				self.getChild(root, "LineText").color[3] = alpha

				for box in self.objlist:
					hgt = 1.5+(self.getChild(root, "LineMesh").localScale[1])
					box.localPosition[1] += hgt*(1/30)

		elif len(self.objlist) >= 1:
			self.timer += 1
			fade = True
			if self.timer > 30:
				self.timer = 0

		if fade == True:
			if self.timer == 30:
				self.objlist.pop(0).endObject()
			elif self.timer < 30:
				alpha = 1-(self.timer/30)
				root = self.objlist[0]
				self.getChild(root, "NameMesh").color[3] = alpha*0.5
				self.getChild(root, "NameText").color[3] = alpha
				self.getChild(root, "LineMesh").color[3] = alpha*0.5
				self.getChild(root, "LineText").color[3] = alpha


class Interact(CoreHUD):

	OBJECT = "Interact"

	def ST_Startup(self):
		self.boot_timer = 0
		self.objects["Text"] = base.SC_HUD.addObject("BlenderFont", self.owner, 0)
		self.objects["Text"].setParent(self.owner, False, False)
		self.objects["Text"].localPosition = (13.4, -17.3, 1)
		self.objects["Text"].color = (0,1,0,1)

	def ST_Active(self, plr):

		text = plr.data["HUD"]["Text"]
		color = plr.data["HUD"]["Color"]
		pos = plr.data["HUD"]["Target"]
		lock = plr.data["HUD"].get("Locked", None)

		if lock != None:
			text = "Locked!"
			color = (1,0,0,1)
			plr.data["HUD"]["Locked"] = None

		if self.boot_timer < 30:
			list = [". "]*int(self.boot_timer/3)
			text = "".join(list)
			self.boot_timer += 1

		self.objects["Text"].text = text

		plr.data["HUD"]["Text"] = ""
		plr.data["HUD"]["Color"] = (0, 0, 0, 0.5)

		if pos == None:
			self.objects["Target"].localPosition = (0, 0, 64)
		else:
			self.objects["Target"].localPosition = ((pos[0]-0.5)*64, (0.5-pos[1])*36, 0)

		self.objects["Target"].color = color


class Target(CoreHUD):

	OBJECT = "Target"

	def ST_Startup(self):
		self.last_health = None
		self.last_target = None
		self.objects["Name"] = base.SC_HUD.addObject("BlenderMono", self.objects["Hits"], 0)
		self.objects["Name"].setParent(self.objects["Hits"], False, False)
		self.objects["Name"].localPosition = (-0.29, 0.55, 0)
		self.objects["Name"].localScale = (0.5,0.5,0.5)
		self.objects["Name"].color = (0,1,0,1)

	def ST_Active(self, plr):
		cls = None
		obj = None
		val = None
		txt = "Target"

		if plr.rayhit != None:
			obj = plr.rayhit[0]
			if obj.invalid == False:
				cls = obj.get("Class", None)
				val = obj.get("HEALTH", None)
				txt = obj.get("RAYNAME", obj.name)

		if cls != None:
			val = cls.data.get("HEALTH", val)
			txt = cls.NAME

		if self.last_target != obj:
			self.last_target = obj
			self.last_health = None

		elif val != None:
			if self.last_health == None:
				self.last_health = val

			if self.last_health != val:
				self.objects["Hits"].color = (1, 0, 0, 1)
				self.last_health = val
			else:
				self.objects["Hits"].color = (0.4, 0.4, 0.4, 1)

			self.objects["Health"].setVisible(True, True)
			self.objects["Health"].color[0] = val/100
			self.objects["Name"].text = txt
			self.objects["Name"].localPosition[0] = len(txt)*-0.145

		else:
			self.objects["Health"].setVisible(False, True)
			self.last_health = None


class MousePos(CoreHUD):

	OBJECT = "MousePos"

	def ST_Startup(self):
		self.boot_timer = 0

	def ST_Active(self, plr):

		pos = plr.data["HUD"]["Target"]

		if pos == None:
			self.objects["Root"].setVisible(False, True)
		else:
			self.objects["Root"].setVisible(True, True)

			pos = self.createVector(vec=(pos[0], pos[1], 0))*12

			self.objects["Cursor"].localPosition = pos.copy()
			self.objects["Line"].localScale = (1,pos.length,1)

			if pos.length < 0.01:
				self.objects["Line"].color = (0,1,0,0)
			else:
				self.objects["Line"].alignAxisToVect(pos, 1, 1.0)
				self.objects["Line"].alignAxisToVect((0,0,1), 2, 1.0)

				if pos.length < 1:
					A = (pos.length*1.01)-0.01
					self.objects["Line"].color = (0,1,0,A)
				else:
					self.objects["Line"].color = (0,1,0,1)


class Stats(CoreHUD):

	OBJECT = "Stats"

	def ST_Startup(self):
		self.oldval = {}

	def ST_Active(self, plr):
		#self.objects["Name"].text = plr.NAME
		self.setStat("Health", plr.data["HEALTH"], 15)
		self.setStat("Energy", plr.data["ENERGY"], 14.1)

	def setStat(self, key, value, offset):
		bar = self.objects[key]
		#txt = self.objects[key+"Text"]

		stat = value/100
		xpos = offset*stat

		#if stat < 0.1:
		#	txt.color = (1,0,0,1)
		#elif stat > 1:
		#	xpos = offset
		#	txt.color = (1,0,1,1)
		#else:
		#	txt.color = (0,1,0,0.5)

		if key not in self.oldval:
			self.oldval[key] = {"TIMER":0, "LAST":stat, "CUR":stat, "DIR":0}

		old = self.oldval[key]

		if abs(old["LAST"]-stat) >= 0.001:
			old["TIMER"] = 30
			old["LAST"] = stat
			old["DIR"] = 1-(2*(old["CUR"]>stat))

		if old["TIMER"] <= 0:
			old["CUR"] += old["DIR"]*0.01

			if abs(old["CUR"]-stat) <= 0.02:
				old["CUR"] = stat
				old["DIR"] = 0

		else:
			old["TIMER"] -= 1

		bar.color[0] = stat
		bar.color[1] = old["CUR"]

		#txt.localPosition[0] = xpos
		#txt.text = str(int(round(value, 0)))


class Weapons(CoreHUD):

	OBJECT = "Weapons"

	def ST_Startup(self):
		self.weaptype = ""
		self.objects["Text"] = base.SC_HUD.addObject("BlenderMono", self.owner, 0)
		self.objects["Text"].setParent(self.owner, False, False)
		self.objects["Text"].localPosition = (-33.7, 16.1, 0)
		self.objects["Text"].color = (0,1,0,0.5)

		self.objects["ID"] = base.SC_HUD.addObject("BlenderMono", self.owner, 0)
		self.objects["ID"].setParent(self.owner, False, False)
		self.objects["ID"].localPosition = (2, 16.1, 0)
		self.objects["ID"].color = (0,1,0,0.5)

	def ST_Active(self, plr):
		weap = plr.data["WPDATA"]

		root = self.objects["Root"]
		mode = self.objects["Mode"]
		wpid = self.objects["ID"]
		stat = self.objects["Text"]

		if weap["CURRENT"] == "NONE":
			root.setVisible(False, True)
		else:
			root.setVisible(True, True)

		if weap["CURRENT"] == "MELEE":
			mode.worldOrientation = self.createMatrix(mirror="XZ")
		else:
			mode.worldOrientation = self.createMatrix()

		if weap["ACTIVE"] == "NONE":
			mode.color = (0.4, 0.4, 0.4, 0.5)
			stat.color = (0.4, 0.4, 0.4, 0.5)
			wpid.color = (0.4, 0.4, 0.4, 0.5)
		else:
			mode.color = (0, 0, 0, 0.5)
			stat.color = (0, 1, 0, 0.5)
			wpid.color = (0, 1, 0, 0.5)

		try:
			wheel = weap["WHEEL"][weap["CURRENT"]]
			ID = wheel["ID"]
			SLOT = wheel["LIST"][ID]

			cls = plr.getSlotChild(SLOT)
			wpid.text = str(ID) #+": "+cls.NAME
			stat.text = str(cls.data["HUD"]["Text"])

		except:
			wpid.text = "000"
			stat.text = "000"

		stat.worldPosition[0] = (len(stat.text)*-0.59)-2


class Inventory(CoreHUD):

	OBJECT = "Inventory"

	def defaultData(self):
		self.itemdict = {}
		self.slotlist = []
		self.powerups = []
		return {}

	def destroy(self):
		for slot in self.itemdict:
			obj = self.itemdict[slot]

			obj["Icon"].endObject()
			if obj["Slot"] != None:
				obj["Slot"].endObject()

		self.itemdict = {}

		self.objects["Root"].endObject()
		self.objects["Root"] = None

	def ST_Active(self, plr):
		pos = 0

		for cls in plr.attachments.copy():
			dict = cls.dict
			slot = dict["Parent"]

			if self.itemdict.get(slot, None) == None:
				name = dict["Object"]
				self.itemdict[slot] = self.newIcon(name)
				self.slotlist.append(slot)

				if slot in plr.data["SLOTS"]:
					key = plr.data["SLOTS"][slot]

					ic, bd = self.newSlotIcon(name, key)
					self.itemdict[slot]["Slot"] = ic
					self.itemdict[slot]["Border"] = bd

				self.itemdict[slot]["DICT"] = dict

		for slot in self.slotlist.copy():
			obj = self.itemdict[slot]
			dict = obj["DICT"]
			data = dict["Data"]

			if dict["Equiped"] in ["DROP", None, False]:
				obj["Icon"].endObject()
				if obj["Slot"] != None:
					obj["Slot"].endObject()
				del self.itemdict[slot]
				self.slotlist.remove(slot)
				continue

			obj["Icon"].localPosition[1] = pos*-2
			obj["Stat"].localScale[0] = 1-(data["HUD"]["Stat"]/100)
			obj["Text"].text = str(data["HUD"]["Text"])

			#obj["Icon"].worldOrientation = obj["Icon"].scene.active_camera.worldOrientation

			if data["ENABLE"] == False:
				obj["Icon"].color = (0.5, 0.5, 0.5, 1)
				obj["Text"].color[3] = 0.5
				bdcol = (0.25, 0.25, 0.25, 1)
			else:
				obj["Icon"].color = (1, 1, 1, 1)
				obj["Text"].color[3] = 1
				bdcol = (0, 0.8, 0, 1)

			if obj["Slot"] != None:
				key = plr.data["SLOTS"].get(slot, None)

				if key == None:
					obj["Slot"].endObject()
					obj["Slot"] = None
					obj["Border"] = None
				else:
					X = self.objects["INV"][key].worldPosition[0]
					obj["Slot"].localPosition[0] = X
					obj["Border"].color = bdcol
			pos += 1

		for dict in plr.data["MODIFIERS"].copy():
			chk = False
			for icon in self.powerups:
				if icon["DICT"] == dict:
					chk = True
			if dict.get("__DURATION__", 0) >= 1 and chk == False:
				icon = self.newIcon(dict.get("__GROUP__", ""))
				icon["DICT"] = dict
				self.powerups.append(icon)

		for icon in self.powerups.copy():
			dict = icon["DICT"]

			if dict not in plr.data["MODIFIERS"]:
				icon["Icon"].endObject()
				self.powerups.remove(icon)
				continue

			fac = dict["__TIMER__"]/dict["__DURATION__"]
			icon["Icon"].localPosition[1] = (pos*-2)-0.25
			icon["Icon"].color = (1,1,1,1)
			icon["Stat"].localScale[0] = 1-fac
			icon["Text"].text = str(dict.get("__TEXT__", round(fac*100)))
			icon["Text"].color[3] = 1
			pos += 1

	def newIcon(self, name):
		scene = base.SC_HUD

		stack = self.objects["Items"]

		if "HUD.Icons."+name not in scene.objectsInactive:
			name = "None"
		icon = scene.objectsInactive["HUD.Icons."+name]
		stat = scene.objectsInactive["HUD.IconStat"]
		text = scene.objectsInactive["BlenderFont"]

		iconobj = base.SC_HUD.addObject(icon, stack, 0)
		statobj = base.SC_HUD.addObject(stat, stack, 0)
		textobj = base.SC_HUD.addObject(text, stack, 0)

		iconobj.setParent(stack)
		statobj.setParent(iconobj)
		textobj.setParent(iconobj)

		statobj.localPosition = (1,0,1)
		statobj.localScale = (0,1,1)
		statobj.color = (0, 0, 0, 0.5)

		textobj.localPosition = (1.1, -0.2, 0)
		textobj.localScale = (0.5, 0.5, 0.5)
		textobj.color = (0, 1, 0, 0.5)

		return {"Icon":iconobj, "Stat":statobj, "Text":textobj, "Slot":None, "Border":None}

	def newSlotIcon(self, name, key):
		scene = base.SC_HUD

		place = self.objects["INV"][key]

		if "HUD.Icons."+name not in scene.objectsInactive:
			name = "None"
		icon = scene.objectsInactive["HUD.Icons."+name]
		border = scene.objectsInactive["HUD.IconBorder"]

		iconobj = base.SC_HUD.addObject(icon, place, 0)
		iconobj.worldPosition[2] += 2
		iconobj.color = (1,1,1,1)

		borderobj = base.SC_HUD.addObject(border, iconobj, 0)
		borderobj.setParent(iconobj)
		borderobj.localPosition = (0, 0, 1)
		borderobj.color = (0.25, 0.25, 0.25, 1)

		return iconobj, borderobj


class Cinema(CoreHUD):

	OBJECT = "Cinema"
	COLORSHEET = {}

	def ST_Startup(self):
		self.objects["Name"] = base.SC_HUD.addObject("BlenderMono", self.owner, 0)
		self.objects["Name"].setParent(self.owner, False, False)
		self.objects["Name"].localPosition = (-15, -17.15, 1)
		self.objects["Name"].text = ""

		self.objects["Line"] = base.SC_HUD.addObject("BlenderMono", self.objects["Name"], 0)
		self.objects["Line"].setParent(self.objects["Name"], False, False)
		self.objects["Line"].localPosition = (0, 0, 0)
		self.objects["Line"].text = ""

	def ST_Active(self, plr):
		subt = plr.data["HUD"].get("Subtitles", None)

		if subt == None:
			return

		self.objects["Name"].text = subt["NAME"]+":"
		col = self.COLORSHEET.get(subt["NAME"], (1,1,1))
		self.objects["Name"].color = (col[0], col[1], col[2], 1)
		self.objects["Line"].text = subt["LINE"]
		self.objects["Line"].localPosition[0] = (len(subt["NAME"])+2)*0.6


class Speedometer(CoreHUD):

	OBJECT = "Speedometer"

	def ST_Active(self, plr):
		root = plr.objects["Root"]
		refY = plr.data["HUD"].get("Forward", (0,1,0))
		vec = root.localLinearVelocity*self.createVector(vec=refY)
		speed = abs(vec)*2.237
		#self.objects["Text"].text = str(round(speed))
		#self.objects["Units"].text = "Mph"
		if speed > 130:
			speed = 130
		self.objects["Speed"].localOrientation = self.createMatrix(rot=[0,0,-speed], deg=True)


class Aircraft(CoreHUD):

	OBJECT = "Aircraft"

	def defaultData(self):
		self.old_power = 0
		self.old_lift = 0
		return {}

	def ST_Active(self, plr):
		root = plr.getOwner()

		glbZ = -plr.gravity
		angX = 90
		angY = 180

		if glbZ.length >= 0.1:

			## Roll ##
			refX = plr.data["HUD"].get("Side", (1,0,0))
			angX = root.getAxisVect(refX).angle(glbZ)
			angX = (self.toDeg(angX)-90)*0.167

			## Pitch ##
			refY = plr.data["HUD"].get("Forward", (0,1,0))
			angY = root.getAxisVect(refY).angle(glbZ)
			angY = (self.toDeg(angY)-90)*0.5

		self.objects["Roll"].localOrientation = self.createMatrix(rot=[0,0,angX], deg=True)
		self.objects["PitchL"].localOrientation = self.createMatrix(rot=[0,0,-angY], deg=True)
		self.objects["PitchR"].localOrientation = self.createMatrix(rot=[0,0,angY], deg=True)

		## Power ##
		power = plr.data["HUD"].get("Power", 0)*0.2
		self.old_power += (power-self.old_power)*0.5
		self.objects["Power"].localOrientation = self.createMatrix(rot=[0,0,self.old_power], deg=True)

		## Lift ##
		lift = plr.data["HUD"].get("Lift", 0)
		self.old_lift += (lift-self.old_lift)*0.5
		self.objects["Lift"].color[0] = ((self.old_lift/100)*0.75)

		## Landing Gear ##
		land = plr.data.get("LANDSTATE", "FLY")
		time = plr.data.get("LANDFRAME", 100)

		if hasattr(plr, "LANDFRAMES") == False:
			self.objects["Land"].color = (0,0,0,0)

		elif land == "FLY":
			start, end = plr.LANDFRAMES

			if time == abs(end-start):
				self.objects["Land"].color = (0,0,0,0)
			else:
				self.objects["Land"].color = (0.4,0.4,0.4,0.5)

		elif land == "LAND":
			if time == 0:
				self.objects["Land"].color = (0,1,0,1)
			else:
				self.objects["Land"].color = (0,1,0,0.5)

		else:
			self.objects["Land"].color = (0,0,0,0)

		## Compass ##
		player = plr.getOwner()
		camera = base.SC_SCN.active_camera
		position = (player.worldPosition+base.ORIGIN_OFFSET)

		refY = plr.data["HUD"].get("Forward", (0,1,0))
		ng = self.createVector(vec=refY)
		nn = self.createVector(vec=(0,1,0))
		ro = nn.rotation_difference(ng).to_matrix()

		evt = plr.getFirstEvent("COMPASS", "NORTH")
		if evt != None:
			base.LEVEL["WAYPOINT"][0] = list(evt.getProp("POS"))

			fw = (evt.getProp("POS")-position).normalized()
			fw = player.worldOrientation.inverted()*fw*ro
			fw[2] = 0

			rdif = fw.to_track_quat("Y", "Z").to_matrix()

		else:
			base.LEVEL["WAYPOINT"][0] = (0,0,0)

			fw = player.worldOrientation*ng
			fw[2] = 0
			#nr = fw.rotation_difference(nn).to_euler()

			rdif = fw.to_track_quat("Y", "Z").to_matrix().inverted()

		self.objects["Compass"].localOrientation = rdif #self.createMatrix(rot=(0,0,nr[2]), deg=False)

		target = Vector(base.LEVEL["WAYPOINT"][-1])

		vg = target-position
		vd = vg.length

		vc = vg.normalized()
		vc = player.worldOrientation.inverted()*vc*ro
		vc[2] = 0
		#vr = fw.rotation_difference(vc).to_euler()

		rdif = vc.to_track_quat("Y", "Z").to_matrix()

		if vd < 1:
			self.objects["CompassTarg"].visible = False
		else:
			self.objects["CompassTarg"].visible = True
			self.objects["CompassTarg"].localOrientation = rdif #self.createMatrix(rot=(0,0,vr[2]), deg=False)

		s = 180*(self.objects["CompassMark"].worldPosition[2]>0)
		self.objects["CompassMark"].worldOrientation = self.createMatrix(rot=(-90,s,0), deg=True)

		if vd < 1000:
			vd = (1-(vd/1000))**3
			self.objects["CompassDist"].color[1] = vd
		else:
			self.objects["CompassDist"].color[1] = 0


class Loading(CoreHUD):

	OBJECT = "Loading"

	def ST_Startup(self):
		self.objects["BG"] = base.SC_HUD.addObject("HUD.Black", self.objects["Root"], 0)
		#self.objects["BG"].applyMovement((0,0,-4), False)
		self.objects["BG"].setParent(self.objects["Root"], False, False)


class HUDLayout:

	GROUP = "Core"
	MODULES = []

	def __init__(self):
		self.modlist = []
		for cls in self.MODULES:
			self.modlist.append(cls(self.GROUP))

	def destroy(self):
		for cls in self.modlist:
			cls.destroy()

	def RUN(self, plr):
		for cls in self.modlist:
			cls.ST_Active(plr)


class LayoutCinema(HUDLayout):

	MODULES = [Cinema]

class LayoutLoading(HUDLayout):

	MODULES = [Loading]


class SceneManager:

	LIBRARY = ["Game Assets HUD.blend"]

	def __init__(self):
		self.active_state = self.ST_Wait
		self.active_layout = None
		self.custom_layout = None
		self.control = None
		self.firstrun = True
		self.blackobj = None
		self.MENU = None

		for blend in self.LIBRARY:
			libblend = settings.ospath.normpath(GAMEPATH+config.LIBRARY_PATH+"\\"+blend)
			logic.LibLoad(libblend, "Scene", load_actions=True, verbose=False, load_scripts=True)
			print(libblend)

	def setControl(self, plr, layout):
		self.control = plr
		self.custom_layout = layout
		self.active_state = self.ST_Wait

	def doBlackOut(self, add=True, que=False):
		state = (self.blackobj not in [None, "QUE"])

		if add == True:
			if state == False:
				if que == True:
					self.blackobj = "QUE"
				else:
					print("HUD: LoadScreen")
					self.blackobj = base.SC_HUD.addObject("HUD.Black", base.SC_HUD.active_camera, 0)
					self.blackobj.applyMovement((0,0,-4), False)

		elif state == True:
			self.blackobj.endObject()
			self.blackobj = None

	def doSceneSuspend(self):
		self.MENU = MenuPause()
		keymap.MOUSELOOK.center()
		base.SC_SCN.suspend()
		self.active_state = self.ST_Paused

	def doSceneResume(self):
		self.MENU.destroy()
		self.MENU = None
		keymap.MOUSELOOK.center()
		base.SC_SCN.resume()
		self.active_state = self.ST_HUD

	def ST_Wait(self):
		if self.active_layout != None:
			self.active_layout.destroy()
			self.active_layout = None

		#if self.control == None:
		#	self.active_state = self.ST_HUD
		#	return

		plr = self.control
		if self.custom_layout == None and plr != None:
			if plr.HUDLAYOUT != None:
				self.custom_layout = plr.HUDLAYOUT

		if self.custom_layout != None:
			self.active_layout = self.custom_layout()
			self.custom_layout = None

			if self.firstrun == True:
				self.firstrun = False
			else:
				self.active_layout.RUN(plr)

		self.active_state = self.ST_HUD

	def ST_HUD(self):

		if self.active_layout != None:
			self.doBlackOut(False)
			self.active_layout.RUN(self.control)

		if keymap.SYSTEM["ESCAPE"].tap() == True:
			self.doSceneSuspend()

	def ST_Paused(self):
		status = self.MENU.RUN()

		if status == "Quit":
			logic.endGame()

		if status == "Keymap":
			self.doBlackOut()
			self.MENU.destroy()
			self.active_state = None
			world.openBlend("KEYMAP")

		if status == "Launcher":
			self.doBlackOut()
			self.MENU.destroy()
			self.active_state = None
			world.openBlend("LAUNCHER")

		if status == "Resume" or keymap.SYSTEM["ESCAPE"].tap() == True:
			self.doSceneResume()

	def RUN(self):
		if self.blackobj == "QUE":
			self.doBlackOut()
		if self.active_state != None:
			self.active_state()

		if logic.globalDict["SCREENSHOT"]["Trigger"] == True:
			frameobj = base.SC_HUD.addObject("HUD.FreezeFrame", base.SC_HUD.active_camera, 0)
			frameobj.applyMovement((0,0,-8), True)
			if keymap.SYSTEM["SHIFT"].checkModifiers() != True and "HUD.Watermark" in base.SC_HUD.objectsInactive:
				frameobj = base.SC_HUD.addObject("HUD.Watermark", base.SC_HUD.active_camera, 5)
				frameobj.applyMovement((0,0,-8), True)


class MenuPause:

	ITEMS = ["Resume", "Keymap", "Launcher", "Quit"]
	OBJECT = "UI_Menu_Item"
	OFFSET = 2.5
	FADE = 10
	GREEN = base.mathutils.Vector([0.0, 0.8, 0.0, 1])
	GRAY = base.mathutils.Vector([0.4, 0.4, 0.4, 1])
	BLACK = base.mathutils.Vector([0.1, 0.1, 0.1, 1])

	def __init__(self):
		scene = base.SC_HUD
		owner = scene.objects["HUD.Menu"]

		self.items = []
		self.objects = {"Root":owner}

		for obj in owner.childrenRecursive:
			if "HUD.Menu." in obj.name:
				key = obj.name.replace("HUD.Menu.", "")
				self.objects[key] = obj

		offset = (self.OFFSET*(len(self.ITEMS)/2))-(self.OFFSET/2)

		self.objects["Items"].localPosition = (-5, offset, 0)
		self.objects["Ray"].localPosition = (0, 0, 0)

		self.objects["Cursor"].color = self.BLACK

		RES = logic.globalDict["GRAPHICS"]["Resolution"]
		self.objects["Cam"].setViewport(0, 0, RES[0], RES[1])
		self.objects["Cam"].useViewport = True

		for item in self.ITEMS:
			dict = self.addItem(self.OBJECT)
			dict["Root"]["ITEM"] = item
			dict["Root"]["TIMER"] = 0
			dict["Root"].color = self.GRAY
			dict["Text"].text = item
			dict["Text"].color = self.BLACK

	def destroy(self):
		self.objects["Cam"].useViewport = False
		for dict in self.items:
			dict["Root"].endObject()
		self.items = []

	def addItem(self, name):
		scene = base.SC_HUD
		owner = self.objects["Root"]
		point = self.objects["Items"]

		obj = scene.addObject(name, point, 0)
		obj["RAYCAST"] = False

		dict = {"Root":obj}

		for child in obj.childrenRecursive:
			key = child.name.replace(name+".", "")
			dict[key] = child

		txt = scene.addObject("BlenderFont", point, 0)
		txt.setParent(obj, False, False)
		txt.localPosition = (0, -0.5, 0.2)
		txt.localScale = (1.3, 1.3, 1.3)
		txt.color = (0.1, 0.1, 0.1, 1)
		dict["Text"] = txt

		obj.setParent(owner)
		self.items.append(dict)
		point.localPosition[1] -= self.OFFSET

		return dict

	def RUN(self):
		click = None

		X, Y = keymap.MOUSELOOK.axis(ui=True)
		X *= 64
		Y *= 36

		for JOYID in keymap.events.JOYBUTTONS:
			AXREF = keymap.events.JOYBUTTONS[JOYID]["Axis"]

			VALX = AXREF.get(0, {"VALUE":0})["VALUE"]
			VALY = AXREF.get(1, {"VALUE":0})["VALUE"]

			NORM = keymap.input.JoinAxis(VALX, VALY)

			X += NORM[0]*0.5
			Y -= NORM[1]*0.5

		ray = self.objects["Ray"]

		rlp = ray.localPosition.copy()
		rlp[0] += X
		rlp[1] += Y
		if rlp[0] > 32:
			rlp[0] = 32
		if rlp[0] < -32:
			rlp[0] = -32
		if rlp[1] > 18:
			rlp[1] = 18
		if rlp[1] < -18:
			rlp[1] = -18

		ray.localPosition[0] = rlp[0]
		ray.localPosition[1] = rlp[1]

		rayOBJ = ray.rayCastTo(self.objects["Cursor"], 100, "RAYCAST")

		if rayOBJ != None:
			rayOBJ["RAYCAST"] = True

		for dict in self.items:
			obj = dict["Root"]
			txt = dict["Text"]

			if obj["RAYCAST"] == True:
				if obj["TIMER"] < self.FADE:
					obj["TIMER"] += 1

				if keymap.SYSTEM["LEFTCLICK"].tap() == True:
					click = obj["ITEM"]

			elif obj["TIMER"] > 0:
				obj["TIMER"] -= 1

			obj.color = self.GRAY.lerp(self.GREEN, obj["TIMER"]/self.FADE)

			obj["RAYCAST"] = False

		return click






